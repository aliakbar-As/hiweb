import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';


import ServiceReport from '../../Components/Screens/MainScreens/ServiceReport/ServiceReport';
import Reports from '../../Components/Screens/MainScreens/ServiceReport/Reports';
import ReportDetails from '../../Components/Screens/MainScreens/ServiceReport/ReportDetails';
import ReportsList from '../../Components/Screens/MainScreens/ServiceReport/ReportsList';
import BundleScreen from '../../Components/Screens/MainScreens/Counter/BundleScreen';


const HomeStack = createStackNavigator();
const Stack = createStackNavigator();


export default ServiceReportScreens = ({ navigation }) => {
    return (
        <HomeStack.Navigator
            initialRouteName="reportsList"
            headerMode="none"
            mode={'card'}
        >

            <Stack.Screen
                name="reportsList"
                component={ReportsList}
            />


            <Stack.Screen
                name="serviceReport"
                component={ServiceReport}
            />


            <Stack.Screen
                name="reports"
                component={Reports}
            />

            <Stack.Screen
                name="reportDetails"
                component={ReportDetails}
            />

            <Stack.Screen
                name="bundle"
                component={BundleScreen}
            />
        </HomeStack.Navigator>
    );
};