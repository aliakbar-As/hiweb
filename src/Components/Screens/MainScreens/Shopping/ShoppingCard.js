import React, { useState, useContext, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    Dimensions, TouchableHighlight,
    TouchableOpacity,
    Modal, TouchableWithoutFeedback, FlatList
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Swiper from 'react-native-swiper';
import { Button, Header, List, Loading, SafeView } from '../../../Commons';
import { ServiceShoppingCard, SlideCard } from '../../../Elements';
import ProgressCircle from 'react-native-progress-circle'
import StoreContext from '../../../../Stores';
import { useObserver } from 'mobx-react-lite';
import moment from 'moment-jalaali';

const { width, height } = Dimensions.get('window');


export default ShoppingCard = ({ navigation }) => {
    const { AuthStore, CounterStore, CounterProfileStore, ShoppingStore } = useContext(StoreContext);

    const [loading, setLoading] = useState(false);
    const [loadingList, setLoadingList] = useState(false);
    const [services, setServices] = useState([]);

    useEffect(() => {
        requestData();
    }, []);

    const requestData = () => {
        setLoadingList(true);
        CounterStore.fetchData(true).then(res => {
            setServices(res);
            setLoadingList(false);
        });
    };

    const getProductList = (id) => {
        setLoading(true);
        ShoppingStore.setCustomerServiceId(id);
        ShoppingStore.fetchData(true).then(res => {
            navigation.navigate('productList');
            setLoading(false);
        });
    };

    const getShoppingList = () => {
        setLoading(true);
        ShoppingStore.getShoppingCart(true).then(() => {
            navigation.navigate('cart');
            setLoading(false);
        });
    };

    return useObserver(() => (
        <SafeView>
            <Header
                title="خرید"
                hasCart
                badgeNumber={0}
                cartOnPress={() => getShoppingList()}
                hasUserIcon
                userOnclick={() => navigation.navigate('profile')}
                noBack />

            <List
                data={services}
                onRefresh={() => requestData()}
                showLoading={loadingList}
                renderItem={(item) => {
                    return (
                        <ServiceShoppingCard
                            onPress={() => getProductList(item.customerServiceId)}
                            title={item.productServiceInfraName}
                            phone={item.phoneNumber}
                            statusTitle={item.sitName}
                            statusCode={item.sitValue}
                        />
                    )
                }}
            />

            {loading ? <Loading /> : null}
        </SafeView>
    ));
};


const styles = EStyleSheet.create({
    activeContainer: {
        width: 10,
        marginLeft: 10,
        height: 10,
        borderRadius: 15,
        backgroundColor: '#6CD7B0'
    },
    statusContainer: {
        alignItems: 'center',
        backgroundColor: '#EDF1FD',
        flexDirection: 'row',
        alignSelf: 'flex-end',
        paddingHorizontal: 7,
        borderRadius: 16,
        paddingVertical: 1,
    },
    primaryTitle: {
        fontSize: 13,
        color: '#505050',
        textAlign: 'right',
        fontFamily: '$REGULAR_FONT'
    },
    hdrTitle: {
        fontSize: 13,
        color: '#2D2D2D',
        textAlign: 'right',
        fontFamily: '$BOLD_FONT',
    },
    cardContainer: {
        height: height / 8,
        width: width - 16,
        alignSelf: 'center',
        borderWidth: 1,
        borderColor: '#fff',
        marginTop: 10,
        borderRadius: 5,
        backgroundColor: '#fff',
        padding: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,

        elevation: 3,
        shadowRadius: 10,
    },
});