import React, { useState, useContext, useEffect } from 'react';
import {
    Dimensions, TouchableHighlight,
    ScrollView, View, Image, Text, TouchableWithoutFeedback, ToastAndroid
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Button, Header, Input, List, Loading, SafeView } from '../../../Commons';
import StoreContext from '../../../../Stores';
import { useObserver } from 'mobx-react-lite';

const { width, height } = Dimensions.get('window');


export default FeedbackScreen = ({ navigation, route }) => {
    const { AuthStore, TicketStore, CounterStore } = useContext(StoreContext);

    const [loadingList, setLoadingList] = useState(false);
    const [loading, setLoading] = useState(false);

    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');

    const postFeedback = () => {
        setLoading(true);
        TicketStore.postFeedback(title, description).then(res => {
            if (typeof res !== 'string') {
                ToastAndroid.show("درخواست شما با موفقیت ثبت شد !", ToastAndroid.SHORT);
                navigation.pop();
                setLoading(false);
            } else {
                Alert.alert("error", res);
                setLoading(false);
            };
        });
    };

    return useObserver(() => (
        <SafeView>
            <Header
                title={"بازخورد نرم‌افزار"}
                backOnClick={() => navigation.pop()}
            />

            <ScrollView>
                <Input
                    value={title}
                    onChangeText={title => setTitle(title)}
                    extraStyles={{ marginTop: 16, width: '90%', }}
                    title={'عنوان :'}
                />

                <Input
                    value={description}
                    onChangeText={description => setDescription(description)}
                    extraStyles={{ marginTop: 16, width: '90%', height: height / 2, textAlignVertical: 'top', }}
                    extraInpuStyles={{ height: height / 2, textAlignVertical: 'top', }}
                    pExtraStyles={{ height: height / 3, }}
                    multiline
                    title={'توضیحات تکمیلی :'}
                />

                <Button
                    onPress={() => postFeedback()}
                    title={'ارسال'}
                    showLoading={loading}
                    disabled={loading}
                    extraStyles={{ marginTop: 16, width: '90%', }}
                />
            </ScrollView>
        </SafeView>
    ));
};


const styles = EStyleSheet.create({
});