import React, { useContext, useState, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    Dimensions,
    ScrollView,
    BackHandler,
    Platform, TouchableHighlight,
    ToastAndroid,
    Modal
} from 'react-native';
import { SafeView, Header, Button, ModalComponent } from '../../../Commons';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Logger, Storage, htmlStyles, CleanHtml } from '../../../../Utils';
import StoreContext from '../../../../Stores';
import HTMLView from 'react-native-htmlview';


const { width, height } = Dimensions.get('window');
const heightScreen = Dimensions.get('window').height;
const checked = require('../../../../assets/images/checked.png');


export default VideoState = ({ route, navigation }) => {
    const { ChallengeStore, ChallengeProfileStore } = useContext(StoreContext);

    const { templateData } = route.params;

    const [modalVisible, setModalVisible] = useState(false);
    const [loading, setLoading] = useState(false);


    return (
        <SafeView>
            <Header
                title={"ارسال ویدیو"}
                backOnClick={() => navigation.pop()}
            />



            <ScrollView>

                <View style={styles.desContainer}>
                    <HTMLView
                        value={CleanHtml(templateData.description === null ? 'بدون توضیحات' : templateData.description)}
                        stylesheet={{ ...htmlStyles }}
                    />
                </View>

                <View style={styles.desContainer}>
                    <HTMLView
                        value={CleanHtml(templateData.data === null ? 'بدون توضیحات' : templateData.data)}
                        stylesheet={{ ...htmlStyles }}
                    />
                </View>


            </ScrollView>

            <Button
                extraStyles={styles.btnstyles}
                onPress={() => navigation.navigate('uploadVideo', { uploadSize: templateData.uploadSize })}
                title={'ارسال فایل'}
            />
            {/* 
            <Modal
                animationType="fade"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => setModalVisible(false)}
            >
                <View style={styles.modalContainer}>
                <Icon
                    name={'close'}
                    size={25}
                    onPress={() => setModalVisible(false)}
                    color={'#fff'}
                    style={styles.closeIconStyle}
                />
                    <View style={styles.modaInnerContainer}>
                        <ImageZoom
                            cropWidth={width - 32}
                            cropHeight={height - 100}
                            imageWidth={width - 32}
                            imageHeight={height / 2}>
                            <Image
                                source={{ uri: templateData.file }}
                                resizeMode={'contain'}
                                style={{ width: '90%', height: '90%', alignSelf: 'center' }}
                            />
                        </ImageZoom>
                    </View>
                </View>
            </Modal> */}

        </SafeView>
    );
};
const styles = EStyleSheet.create({
    closeIconStyle: {
        alignSelf: 'flex-end',
        margin: 16,
        marginTop: 0
    },


    modalContainer: {
        backgroundColor: 'rgba(0,0,0,0.6)',
        flex: 1,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    modaInnerContainer: {
        backgroundColor: '#fff',
        padding: 10,
        paddingBottom: 30,
        // borderRadius: 10,
        alignItems: 'center',
        width: width - 32,
        height: height - 100,
        borderRadius: 10,
    },

    /////////////////////////////////////////////////////
    fileStyle: {
        width: '85%',
        height: height / 2,
        alignSelf: 'center',
        borderRadius: 10,
        marginTop: 16,
    },
    line: {
        width: '65%',
        alignSelf: 'center',
        height: 2,
        backgroundColor: '#12141D',
        marginBottom: 32
    },
    progressTitle: {
        fontSize: 13,
        color: '#fff',
        fontFamily: '$BOLD_FONT',
        textAlign: 'center',
    },
    nationalCodeContainer: {
        flexDirection: 'row-reverse',
        alignItems: 'center',
        marginTop: 32,
        justifyContent: 'center',
    },
    nationalCode: {
        fontFamily: '$BOLD_FONT',
        textAlign: 'left',
        color: '#12141D',
        fontSize: 23,
    },
    nationalCodeTitle: {
        fontFamily: '$REGULAR_FONT',
        textAlign: 'center',
        color: '#12141D',
        fontSize: 18,
        marginLeft: 10
    },
    statusTitleStyle: {
        color: '#12141D',
        textAlign: 'center',
        fontSize: 18,
        margin: 16,
        marginBottom: 24,
        fontFamily: '$BOLD_FONT'
    },
    closeIcon: {
        position: 'absolute',
        right: 10,
        top: 10,
        zIndex: 1,
    },
    uploadVideoContainer: {
        width: '90%',
        height: width / 2,
        backgroundColor: 'transparent',
        borderWidth: 1,
        borderColor: 'grey',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        alignSelf: 'center',
        marginBottom: 32,
        marginTop: 16,
    },
    avatarImage: {
        width: '100%',
        height: '100%',
        alignSelf: 'center',
        borderRadius: 5,
    },
    checkedIcon: {
        width: 50,
        height: 50,
        alignSelf: 'center',
        marginVertical: 10,
    },
    checkedcontainer: {
        width: width - 85,
        alignSelf: 'center',
        borderRadius: 30,
        borderColor: '#12141D',
        alignItems: 'center',
        borderWidth: 1,
        height: 250,
        marginTop: 32,
    },
    desContainer: {
        borderWidth: 1,
        borderColor: 'grey',
        alignItems: 'center',
        marginHorizontal: 16,
        padding: 32,
        borderRadius: 10,
        width: '85%',
        alignSelf: 'center',
        marginTop: 10,
    },
    btnstyles: {
        width: '85%',
        borderRadius: 100,
        height: 45,
    },
    desTitle: {
        fontFamily: '$REGULAR_FONT',
        textAlign: 'right',
        color: '#12141D',
        fontSize: 14,
    },
    btn: {
        backgroundColor: 'transparent',
        borderWidth: 1,
        width: '85%',
        height: 70,
        borderRadius: 50,
        borderColor: '#12141D'
    },
})
