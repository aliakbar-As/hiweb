import { types, applySnapshot } from 'mobx-state-tree';
import { UserModel } from './models';
import { request, Storage, Base64, Logger, getString } from '../Utils';
import { SingleChallenge } from './models/SingleModels';


const challengeStore = types.model('challengeStore', {
    data: types.optional(types.array(SingleChallenge), []),

    limit: types.optional(types.number, 10),
    page: types.optional(types.number, 1),
    isLastPage: types.optional(types.boolean, false),

    loading: types.optional(types.boolean, false),
    errMessage: types.maybeNull(types.string),

}).actions((self) => {
    return {
        async fetchData(clear = false) {
            return new Promise(async (resolve, reject) => {
                if (clear) this.resetList();

                request.get('/v1/User/Challenge/List', {
                    params: {
                        Size: self.limit,
                        Page: self.page,
                        Sort: 1,
                    }
                }).then(({ data }) => {
                    Logger(data, 'Challenge List');
                    if (data.isSuccess) {
                        this.fillData(data.data);
                        resolve(data.isSuccess);
                        return;
                    };
                    resolve(data.isSuccess);
<<<<<<< HEAD
=======
                    this.resetList();

>>>>>>> f4d81353b7a9c0d70ec16fd7a5857df2f5c9617b
                }).catch(err => {
                    resolve(false);
                    this.resetList();
                    Logger(err);
                });
            }).catch(err => {
                resolve(false);
                this.resetList();
                Logger(err);
            });
        },



        fillData(data) {
            if (data.length === 0) {
                self.isLastPage = true;
                return false;
            }
            Array.prototype.push.apply(self.data, data.map(item => item));
        },
        setErrorMessage(message) {
            self.errMessage = message;
        },
        resetList() {
            self.page = 1;
            self.data = [];
            self.isLastPage = false;
        },
        onEndReached() {
            if (self.isLastPage || self.loading) {
                return false;
            };
            self.page += 1;
            this.fetchData();
        },
        changeLoading(data) {
            self.loading = data;
        },
    };
});
export const ChallengeStore = challengeStore.create();


const challengeProfileStore = types.model('challengeProfileStore', {
    loading: types.optional(types.boolean, false),
    errMessage: types.maybeNull(types.string),

    challengeDetails: SingleChallenge,
    challengeId: types.maybeNull(types.string),

    text_TemplateId: types.maybeNull(types.string),
    image_TemplateId: types.maybeNull(types.string),
    postFileId: types.maybeNull(types.string),

    userHasLogged: types.optional(types.boolean, false),

}).actions((self) => {
    return {
        async getChallengeDetails() {
            return new Promise(async (resolve, reject) => {
                request.get('/v1/User/Challenge/Single', {
                    params: {
                        Id: self.challengeId,
                    }
                }).then(({ data }) => {
                    Logger(data.data, 'ChallengeSingle');
                    if (data.isSuccess) {
                        this.fillProductInfo(data.data);
                        resolve(data.data);
                    };
                }).catch(err => Logger(err));
            }).catch(err => Logger(err));
        },
        setUserHasLogged(data) {
            self.userHasLogged = data;
        },
        async postContributer() {
            return new Promise(async (resolve, reject) => {
                request.post('/v1/User/ChallengeContributor/PostContributor', {
                    challengeId: self.challengeId,
                }).then(({ data }) => {
                    Logger(data, 'PostContributor');
                    if (data.isSuccess) {
                        this.fillContributorData(data.data.id, data.data.text_TemplateId, data.data.image_TemplateId);
                        resolve(data);
                    } else {
                        resolve(data.message.toString());
                    };
                }).catch(err => Logger(err));
            }).catch(err => Logger(err));
        },


        async getTemplateSingle(type) {
            return new Promise(async (resolve, reject) => {
                request.get('/v1/User/ChallengeTemplate/Single', {
                    params: {
                        id: type === 'video' ? self.text_TemplateId : self.image_TemplateId,
                    }
                }).then(({ data }) => {
                    Logger(data, `ChallengeTemplate ${type === 'video' ? 'video' : 'image'} Single`);
                    if (data.isSuccess) {
                        if (type === 'video') {
                            resolve(data.data[0]);
                        } else {
                            resolve(data.data);
                        };
                    };
                }).catch(err => Logger(err));
            }).catch(err => Logger(err));
        },

        async postVideo(videoStatus, type) {
            return new Promise(async (resolve, reject) => {

                console.log('videoStatus', videoStatus);
                let items = {
                    filePath: videoStatus.data[0]
                }
                if (videoStatus.isSuccess) {
                    request.post('/v1/User/ChallengeContributor/PostFile', {
                        contributorId: self.postFileId,
                        type: type,
                        files: [
                            {
                                filePath: items.filePath.file
                            }
                        ]
                    }, {}, false)
                        .then(res => {
                            this.changeLoading(false);
                            Logger(res, 'postVideo');
                            if (res.data.isSuccess) {
                                resolve(true)
                            };
                        });
                } else {
                    this.changeLoading(false);
                    resolve(videoStatus.message);
                }
            }).catch((err) => reject(err));
        },


        async postImages(videoStatus, type) {

            let fileItem = videoStatus.data.map(item => {
                return { filePath: item.file };
            });

            return new Promise(async (resolve, reject) => {
                if (videoStatus.isSuccess) {
                    request.post('/v1/User/ChallengeContributor/PostFile', {
                        contributorId: self.postFileId,
                        type: type,
                        files: fileItem,
                    }, {}, false)
                        .then(res => {
                            this.changeLoading(false);
                            Logger(res, 'postVideo');
                            if (res.data.isSuccess) {
                                resolve(true)
                            };
                        });
                } else {
                    this.changeLoading(false);
                    resolve(videoStatus.message);
                }
            }).catch((err) => reject(err));
        },

        setChallengeId(id) {
            self.challengeId = id;
        },

        fillContributorData(id, text_TemplateId, image_TemplateId) {
            self.postFileId = id;
            self.text_TemplateId = text_TemplateId;
            self.image_TemplateId = image_TemplateId;
        },

        fillProductInfo(data) {
            self.challengeDetails = data;
        },

        setErrorMessage(message) {
            self.errMessage = message;
        },
        changeLoading(value) {
            return self.loading = value;
        },
    };
});

export const ChallengeProfileStore = challengeProfileStore.create({
    challengeDetails: SingleChallenge.create(),
});
