import { createContext } from 'react';
import { AuthStore } from './AuthStore';
import { CounterStore, CounterProfileStore, BundleStore } from './CounterStore';
import { ShoppingStore, ShoppingProfileStore } from './ShoppingStore';
import { TicketStore } from './TicketStore';
import { ServiceReportStore } from './ServiceReportStore';
import { ChallengeStore, ChallengeProfileStore } from './ChallengeStore';


export const stores = {
    AuthStore,
    CounterStore, CounterProfileStore, BundleStore,
    ShoppingStore, ShoppingProfileStore,
    TicketStore,
    ServiceReportStore,
    ChallengeStore, ChallengeProfileStore,
};

export default StoreContext = createContext(stores);
