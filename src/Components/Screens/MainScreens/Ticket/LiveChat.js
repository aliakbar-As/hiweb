import React, { useState, useContext, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    Dimensions, TouchableHighlight,
    TouchableOpacity,
    Modal, TouchableWithoutFeedback, FlatList,
    ToastAndroid,
    Alert,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Swiper from 'react-native-swiper';
import { Button, Header, Input, List, Loading, SafeView } from '../../../Commons';
import { SlideCard } from '../../../Elements';
import ProgressCircle from 'react-native-progress-circle'
import StoreContext from '../../../../Stores';
import { useObserver } from 'mobx-react-lite';
import moment from 'moment-jalaali';
import { Numbers, Storage } from '../../../../Utils';
import { CommonActions } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/AntDesign'
import { WebView } from 'react-native-webview';

const { width, height } = Dimensions.get('window');


export default LiveChat = ({ navigation, route }) => {
    const { AuthStore, TicketStore, CounterStore } = useContext(StoreContext);

    const [loadingList, setLoadingList] = useState(false);
    const [loading, setLoading] = useState(false);


    return useObserver(() => (
        <SafeView>
            <Header
                title={"چت با پشتیبانی"}
                backOnClick={() => navigation.pop()}
            />

            <WebView
                source={{ uri: 'http://chat.hiweb.ir/chat.php?v=2&linkid=MDRlYWVlNzhlODJhZDUyNDdkNzhhZGYxNDEzNzNjM2M_' }}
            />
        </SafeView>
    ));
};


const styles = EStyleSheet.create({
});