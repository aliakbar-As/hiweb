import { types, applySnapshot } from 'mobx-state-tree';
import { request, Storage, Base64, Logger, getString } from '../Utils';
import { SingleReport, SingleSession } from './models/SingleModels';
import moment from 'moment-jalaali';

const serviceReportStore = types.model('serviceReportStore', {
    loading: types.optional(types.boolean, false),
    errMessage: types.maybeNull(types.string),
    data: types.optional(types.array(SingleReport), []),

    limit: types.optional(types.number, 10),
    page: types.optional(types.number, 1),
    isLastPage: types.optional(types.boolean, false),

    customerServiceId: types.maybeNull(types.string),
    startDate: types.maybeNull(types.string),
    endDate: types.maybeNull(types.string),
    sessionSummary: types.optional(types.array(SingleSession), []),
}).actions((self) => {
    return {
        async fetchData(clear = false) {
            this.setErrorMessage(null);

            let serviceId = await Storage.GetItem('@serviceId');

            return new Promise(async (resolve, reject) => {
                if (clear) this.resetList();

                request.get('/v1/User/Service/Report', {
                    params: {
                        CustomerServiceId: serviceId,
                        StartDate: self.startDate,
                        EndDate: self.endDate,
                        ConnectionTimeouts: true,
                        Size: self.limit,
                        Page: self.page,
                        Sort: 1,
                    }
                })
                    .then(({ data }) => {
                        Logger(data, 'service Report');
                        if (data.isSuccess) {
                            this.fillData(data.data.result.detailGroup);
                            this.fillSession(data.data.result.sessionSummary.items);
                            resolve(data.data.result.sessionSummary);
                        } else {
                            this.setErrorMessage(data.message);
                            resolve(data.isSuccess);
                        };
                    }).catch(err => Logger(err));
            }).catch(err => Logger(err));
        },

        setCustomerServiceId(id) {
            self.customerServiceId = id;
        },
        setStartDate(startDate) {
            let mainTypeDate = moment(startDate).format('YYYY-MM-DDTHH:mm:ss');
            self.startDate = mainTypeDate;
        },
        setEndDate(endDate) {
            let mainTypeDate = moment(endDate).format('YYYY-MM-DDTHH:mm:ss');
            self.endDate = mainTypeDate;
        },

        fillCartData(data) {
            if (data.length === 0) {
                return false;
            }
            Array.prototype.push.apply(self.cartData, data.map(item => item));
        },

        fillData(data) {
            if (data.length === 0) {
                self.isLastPage = true;
                return false;
            }
            Array.prototype.push.apply(self.data, data.map(item => item));
        },
        fillSession(data) {
            self.sessionSummary = data;
        },
        setErrorMessage(message) {
            self.errMessage = message;
        },
        resetList() {
            self.page = 1;
            self.data = [];
            self.isLastPage = false;
        },
        onEndReached() {
            if (self.isLastPage || self.loading) {
                return false;
            };
            self.page += 1;
            this.fetchData();
        },
        changeLoading(data) {
            self.loading = data;
        },
    };
});
export const ServiceReportStore = serviceReportStore.create();

