import { types, applySnapshot } from 'mobx-state-tree';
import { UserModel } from './models';
import { request, Storage, Base64, Logger, getString } from '../Utils';
import { SingleTicket } from './models/SingleModels';


const ticketStore = types.model('ticketStore', {
    data: types.optional(types.array(SingleTicket), []),

    loading: types.optional(types.boolean, false),
    errMessage: types.maybeNull(types.string),

}).actions((self) => {
    return {
        async fetchData(clear = false) {
            return new Promise(async (resolve, reject) => {
                if (clear) this.resetList();

                request.get('/v1/User/Ticket/Get')
                    .then(({ data }) => {
                        Logger(data, 'Ticket List');
                        if (data.isSuccess) {
                            this.fillData(data.data);
                            resolve(data.isSuccess);
                        } else {
                            this.setErrorMessage(data.message);
                            resolve(data.isSuccess);
                        };
                    }).catch(err => {
                        Logger(err);
                        resolve(false);
                    });
            }).catch(err => {
                Logger(err);
                resolve(false);
            });
        },

        async deleteRequest(Id) {
            return new Promise(async (resolve, reject) => {

                request.post(`/v1/User/Ticket/Delete?Id=${Id}`).then(({ data }) => {
                    Logger(data, 'Delete Ticket');

                    resolve(data.isSuccess);
                }).catch(err => {
                    Logger(err);
                    resolve(false);
                });
            }).catch(err => {
                Logger(err);
                resolve(false);
            });
        },

        async getSubjectList() {
            return new Promise(async (resolve, reject) => {

                request.get('/v2/User/Ticket/SubjectConfig')
                    .then(({ data }) => {
                        Logger(data, 'subjects list');

                        resolve(data.data);
                    }).catch(err => Logger(err));
            }).catch(err => Logger(err));
        },
        async postTicket(subjectTitle, subjectId, serviceId, des) {
            this.setErrorMessage(null);

            return new Promise(async (resolve, reject) => {

                request.post('/v1/User/Ticket/Post', {
                    title: subjectTitle,
                    subjectId: subjectId,
                    customerServiceId: serviceId,
                    description: des
                })
                    .then(({ data }) => {
                        Logger(data, 'ticket Post');
                        if (data.isSuccess) {
                            resolve(data.data.id);
                        } else {
                            resolve(false);
                            this.setErrorMessage(data.message);
                        };
                    }).catch(err => Logger(err));
            }).catch(err => Logger(err));

        },
        async uploadFile(fileData, ticketId) {

            const formData = new FormData();
            formData.append('file', fileData);

            return new Promise(async (resolve, reject) => {

                request.post('/v1/User/Ticket/Upload', formData, {
                    params: {
                        TicketId: ticketId,
                    }
                }).then(data => {
                    Logger(data.data, 'uploadFile');
                    if (data.data.isSuccess) {
                        resolve(true)
                    } else {
                        resolve(false);
                    };

                }).catch(err => {
                    Logger(err);
                    resolve(false);
                });
            }).catch(err => {
                Logger(err);
                resolve(false);
            });

        },

        async getLink(type) {
            return new Promise(async (resolve, reject) => {

                request.get('/v1/Global/Link/Get', {
                    params: {
                        Type: type
                    }
                })
                    .then(({ data }) => {
                        Logger(data, 'Link Get');

                        resolve(data.data.link);
                    }).catch(err => Logger(err));
            }).catch(err => Logger(err));
        },

        async postFeedback(title, description) {
            return new Promise(async (resolve, reject) => {

                request.post('/v1/User/Feedback/Post', {
                    title: title,
                    description: description,
                })
                    .then(({ data }) => {
                        Logger(data, 'Feedback');
                        if (data.isSuccess) {
                            resolve(data.isSuccess);
                        } else {
                            resolve(data.message);
                        };
                    }).catch(err => Logger(err));
            }).catch(err => Logger(err));
        },

        fillCartData(data) {
            if (data.length === 0) {
                return false;
            }
            Array.prototype.push.apply(self.cartData, data.map(item => item));
        },

        fillData(data) {
            if (data.length === 0) {
                self.isLastPage = true;
                return false;
            }
            Array.prototype.push.apply(self.data, data.map(item => item));
        },
        setErrorMessage(message) {
            self.errMessage = message;
        },
        resetList() {
            self.page = 1;
            self.data = [];
            self.isLastPage = false;
        },
        onEndReached() {
            if (self.isLastPage || self.loading) {
                return false;
            };
            self.page += 1;
            this.fetchData();
        },
        changeLoading(data) {
            self.loading = data;
        },
    };
});
export const TicketStore = ticketStore.create();

