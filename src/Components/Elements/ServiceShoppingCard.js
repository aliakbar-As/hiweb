import React, { useState, useContext, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    Dimensions, TouchableHighlight,
    TouchableOpacity,
    Modal, TouchableWithoutFeedback, FlatList
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

const { width, height } = Dimensions.get('window');


export const ServiceShoppingCard = ({ title, phone, statusTitle, onPress, statusCode, extraStyles, postalCode }) => {
    return (
        <TouchableHighlight
            onPress={onPress}
            underlayColor={'transparent'}>
            <View style={[styles.cardContainer, extraStyles]}>
                <View style={{ alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between' }}>
                    <View style={styles.statusContainer}>
                        <Text style={styles.primaryTitle}>{statusTitle}</Text>

                        <View style={[styles.activeContainer, {
                            backgroundColor: statusCode === 13 ? '#FF9800' : statusCode === 12 ? '#6CD7B0' : 'red',
                        }]} />
                    </View>

                    <Text style={styles.hdrTitle}>{title}</Text>
                </View>


                <View style={{ alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 10, }}>
                    <Text style={styles.primaryTitle}>{postalCode}</Text>

                    <Text style={styles.primaryTitle}>{phone}</Text>
                </View>

            </View>
        </TouchableHighlight>
    );
};



const styles = EStyleSheet.create({
    activeContainer: {
        width: 10,
        marginLeft: 10,
        height: 10,
        borderRadius: 15,
        backgroundColor: '#6CD7B0'
    },
    statusContainer: {
        alignItems: 'center',
        backgroundColor: '#EDF1FD',
        flexDirection: 'row',
        alignSelf: 'flex-end',
        paddingHorizontal: 7,
        borderRadius: 16,
        paddingVertical: 1,
    },
    primaryTitle: {
        fontSize: 13,
        color: '#505050',
        textAlign: 'right',
        fontFamily: '$REGULAR_FONT'
    },
    hdrTitle: {
        fontSize: 13,
        color: '#2D2D2D',
        textAlign: 'right',
        fontFamily: '$BOLD_FONT',
    },
    cardContainer: {
        height: height / 8,
        width: width - 16,
        alignSelf: 'center',
        borderWidth: 1,
        borderColor: '#fff',
        marginTop: 10,
        borderRadius: 5,
        backgroundColor: '#fff',
        padding: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,

        elevation: 3,
        shadowRadius: 10,
    },
});