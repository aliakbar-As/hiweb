import React, { useState, useContext, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    Dimensions, TouchableHighlight,
    TouchableOpacity,
    Modal, TouchableWithoutFeedback, FlatList,
    ToastAndroid,
    Alert
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Swiper from 'react-native-swiper';
import { Button, Header, Input, List, Loading, ModalComponent, SafeView } from '../../../Commons';
import { SlideCard } from '../../../Elements';
import ProgressCircle from 'react-native-progress-circle'
import StoreContext from '../../../../Stores';
import { useObserver } from 'mobx-react-lite';
import moment from 'moment-jalaali';
import { Numbers, Storage } from '../../../../Utils';
import { CommonActions } from '@react-navigation/native';

const { width, height } = Dimensions.get('window');


export default EditProfile = ({ navigation, route }) => {
    const { AuthStore, CounterStore } = useContext(StoreContext);

    const [loading, setLoading] = useState(false);
    const [serviceActivated, setServiceActivated] = useState(true);

    useEffect(() => {
        setServiceActivated(AuthStore.userModel.cancelPreviousService);
    }, []);

    const confirmInfo = () => {
        setLoading(true);
        CounterStore.cancelService(serviceActivated).then(() => {
            AuthStore.getUserInfo().then(() => {
                navigation.pop();
                setLoading(false);
            })
        });
    };

    return useObserver(() => (
        <SafeView>
            <Header
                title={"ویرایش فعالسازی خودکار"}
                backOnClick={() => navigation.pop()}
            />

            <ScrollView>
                <View style={styles.warningView}>
                    <Text style={styles.titles}>
                        مشترک گرامی
                        {'\n'}
                        با فعال سازی این گزینه موقع خرید بسته در صورتیکه سرویس فعال داشته باشید پایان می یابد و سرویس جدید جایگزین آن می شود.
                    </Text>
                </View>

                <View style={{ margin: 10 }}>
                    <View style={{ paddingHorizontal: 16, paddingBottom: 10 }}>
                        <TouchableOpacity onPress={() => setServiceActivated(!serviceActivated)}>
                            <View style={styles.primaryView}>

                                <Text style={styles.hdrTitle}>
                                    فعال سازی خودکار سرویس
                                </Text>
<<<<<<< HEAD
                                <View style={[styles.radioView, { backgroundColor: serviceActivated ? '#0F61AB' : '#fff' }]} />
=======
                                <View style={[styles.radioView, { backgroundColor: serviceActivated ? '#DE0021' : '#fff' }]} />
>>>>>>> f4d81353b7a9c0d70ec16fd7a5857df2f5c9617b
                            </View>
                        </TouchableOpacity>

                    </View>

                </View>

            </ScrollView>

            <Button
                disabled={loading}
                showLoading={loading}
                onPress={() => confirmInfo()}
                title={'تایید و بازگشت'}
                extraStyles={{ margin: 10, width: '95%' }}
            />

        </SafeView>
    ));
};


const styles = EStyleSheet.create({
    hdrTitle: {
        fontSize: 13,
        color: '#2D2D2D',
        textAlign: 'right',
        fontFamily: '$BOLD_FONT',
    },
    primaryView: { alignItems: 'center', justifyContent: 'flex-end', flexDirection: 'row' },
    radioView: {
        borderWidth: 1,
<<<<<<< HEAD
        borderColor: '#0F61AB',
=======
        borderColor: '#DE0021',
>>>>>>> f4d81353b7a9c0d70ec16fd7a5857df2f5c9617b
        width: 15,
        height: 15,
        borderRadius: 2,
        marginLeft: 10,
    },

    warningView: {
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#5778AA',
        backgroundColor: '#D9EDF7',
        padding: 10,
        margin: 10
    },
    titles: {
        fontSize: 14,
        color: '#5778AA',
        textAlign: 'right',
        fontFamily: '$REGULAR_FONT',
    },
});