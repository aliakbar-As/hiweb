import React, { useContext, useState, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    Dimensions,
    ScrollView,
    BackHandler,
    Platform, TouchableHighlight,
    ToastAndroid,
    Modal
} from 'react-native';
import { SafeView, Header, Button, ModalComponent } from '../../../Commons';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Logger, Storage, htmlStyles, CleanHtml } from '../../../../Utils';
import StoreContext from '../../../../Stores';
import HTMLView from 'react-native-htmlview';
import Icon from 'react-native-vector-icons/AntDesign';
import ImageZoom from 'react-native-image-pan-zoom';
import Swiper from 'react-native-swiper';

const { width, height } = Dimensions.get('window');


export default PicturesState = ({ route, navigation }) => {
    const { ChallengeStore, ChallengeProfileStore } = useContext(StoreContext);

    const { templateData } = route.params;

    const [modalVisible, setModalVisible] = useState(false);
    const [loading, setLoading] = useState(false);

    const [albums, setAlbums] = useState([]);


    useEffect(() => {
        console.log('templateData', templateData);
        setAlbums(templateData);
    }, []);

    return (
        <SafeView>
            <Header
                title={"ارسال عکس"}
                backOnClick={() => navigation.pop()}
            />



            <ScrollView>

                <View style={styles.desContainer}>
                    <HTMLView
                        value={CleanHtml(templateData[0].description === null ? 'بدون توضیحات' : templateData[0].description)}
                        stylesheet={{ ...htmlStyles }}
                    />
                </View>


                <ScrollView horizontal style={{ paddingBottom: 16 }}>
                    {templateData.map((item, i) => {
                        return (
                            <TouchableHighlight
                                key={i}
                                underlayColor={'transparent'}
                                onPress={() => setModalVisible(true)}>
                                <Image
                                    source={{ uri: item.data }}
                                    style={{ width: width / 2 + 50, height: 150, borderRadius: 10, marginLeft: 10, }}
                                />
                            </TouchableHighlight>
                        )
                    })}
                </ScrollView>

            </ScrollView>

            <Button
                extraStyles={styles.btnstyles}
                onPress={() => navigation.navigate('uploadImages')}
                title={'ارسال فایل'}
            />

            <Modal
                animationType="fade"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => setModalVisible(false)}
            >
                <View style={styles.modalContainer}>
                    <Icon
                        name={'close'}
                        size={25}
                        onPress={() => setModalVisible(false)}
                        color={'#fff'}
                        style={styles.closeIconStyle}
                    />
                    <View style={styles.modaInnerContainer}>
                        <Swiper
                            key={albums.length}
                            dot={<View style={styles.dotContainer} />}
                            activeDot={<View style={styles.activeDotContainer} />}
                            height={height - 100}
                            showsPagination
                            loop>
                            {albums.map((album, i) => (
                                <ImageZoom
                                    key={i}
                                    cropWidth={width - 32}
                                    cropHeight={height - 100}
                                    imageWidth={width - 32}
                                    imageHeight={height / 2}>
                                    <Image
                                        source={{ uri: album.data }}
                                        resizeMode={'contain'}
                                        style={styles.swiperImg} />
                                </ImageZoom>
                            ))}
                        </Swiper>
                    </View>
                </View>
            </Modal>

        </SafeView>
    );
};
const styles = EStyleSheet.create({
    swiperImg: {
        width: width - 32,
        height: height / 2,
        borderRadius: 10
    },
    dotContainer: {
        backgroundColor: 'rgba(0,0,0,0.5)',
        width: 8,
        height: 8,
        borderRadius: 4,
        marginLeft: 3,
        marginRight: 3,
        marginTop: 3,
        marginBottom: 3,
    },
    activeDotContainer: {
        backgroundColor: '#FF005A',
        width: 8,
        height: 8,
        borderRadius: 4,
        marginLeft: 3,
        marginRight: 3,
        marginTop: 3,
        marginBottom: 3,
    },
    closeIconStyle: {
        alignSelf: 'flex-end',
        margin: 16,
        marginTop: 0
    },
    modalContainer: {
        backgroundColor: 'rgba(0,0,0,0.8)',
        height: height,
        alignItems: 'center',
        justifyContent: 'center',
        width: width,
    },
    modaInnerContainer: {
        backgroundColor: '#fff',
        padding: 6,
        paddingBottom: 30,
        borderRadius: 10,
        alignItems: 'center',
        width: width - 32,
        height: height - 100,
        justifyContent: 'center',
    },

    /////////////////////////////////////////////////////
    fileStyle: {
        width: '85%',
        height: height / 2,
        alignSelf: 'center',
        borderRadius: 10,
        marginTop: 16,
    },
    line: {
        width: '65%',
        alignSelf: 'center',
        height: 2,
        backgroundColor: '#12141D',
        marginBottom: 32
    },
    progressTitle: {
        fontSize: 13,
        color: '#fff',
        fontFamily: '$BOLD_FONT',
        textAlign: 'center',
    },
    nationalCodeContainer: {
        flexDirection: 'row-reverse',
        alignItems: 'center',
        marginTop: 32,
        justifyContent: 'center',
    },
    nationalCode: {
        fontFamily: '$BOLD_FONT',
        textAlign: 'left',
        color: '#12141D',
        fontSize: 23,
    },
    nationalCodeTitle: {
        fontFamily: '$REGULAR_FONT',
        textAlign: 'center',
        color: '#12141D',
        fontSize: 18,
        marginLeft: 10
    },
    statusTitleStyle: {
        color: '#12141D',
        textAlign: 'center',
        fontSize: 18,
        margin: 16,
        marginBottom: 24,
        fontFamily: '$BOLD_FONT'
    },
    closeIcon: {
        position: 'absolute',
        right: 10,
        top: 10,
        zIndex: 1,
    },
    uploadVideoContainer: {
        width: '90%',
        height: width / 2,
        backgroundColor: 'transparent',
        borderWidth: 1,
        borderColor: 'grey',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        alignSelf: 'center',
        marginBottom: 32,
        marginTop: 16,
    },
    avatarImage: {
        width: '100%',
        height: '100%',
        alignSelf: 'center',
        borderRadius: 5,
    },
    checkedIcon: {
        width: 50,
        height: 50,
        alignSelf: 'center',
        marginVertical: 10,
    },
    checkedcontainer: {
        width: width - 85,
        alignSelf: 'center',
        borderRadius: 30,
        borderColor: '#12141D',
        alignItems: 'center',
        borderWidth: 1,
        height: 250,
        marginTop: 32,
    },
    desContainer: {
        borderWidth: 1,
        borderColor: 'grey',
        alignItems: 'center',
        marginHorizontal: 16,
        padding: 32,
        borderRadius: 10,
        width: '85%',
        alignSelf: 'center',
        marginTop: 10,
        marginBottom: 32,
    },
    btnstyles: {
        width: '85%',
        borderRadius: 100,
        height: 45,
    },
    desTitle: {
        fontFamily: '$REGULAR_FONT',
        textAlign: 'right',
        color: '#12141D',
        fontSize: 14,
    },
    btn: {
        backgroundColor: 'transparent',
        borderWidth: 1,
        width: '85%',
        height: 70,
        borderRadius: 50,
        borderColor: '#12141D'
    },
})
