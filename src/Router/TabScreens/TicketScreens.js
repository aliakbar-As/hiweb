import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';


import Tickets from '../../Components/Screens/MainScreens/Ticket/Tickets';
import TicketRequest from '../../Components/Screens/MainScreens/Ticket/TicketRequest';
import Profile from '../../Components/Screens/MainScreens/Profile/Profile';
import LiveChat from '../../Components/Screens/MainScreens/Ticket/LiveChat';
import FaqScreen from '../../Components/Screens/MainScreens/Ticket/FaqScreen';
import FeedbackScreen from '../../Components/Screens/MainScreens/Ticket/FeedbackScreen';
import ChangePassword from '../../Components/Screens/MainScreens/Profile/ChangePassword';
import EditProfile from '../../Components/Screens/MainScreens/Profile/EditProfile';


const HomeStack = createStackNavigator();
const Stack = createStackNavigator();

export default TicketScreens = ({ navigation }) => {
    return (
        <HomeStack.Navigator
            initialRouteName="tickets"
            headerMode="none"
            mode={'card'}
        >

            <Stack.Screen
                name="tickets"
                component={Tickets}
            />

            <Stack.Screen
                name="ticketRequest"
                component={TicketRequest}
            />

            <Stack.Screen
                name="profile"
                component={Profile}
            />

            <Stack.Screen
                name="changePassword"
                component={ChangePassword}
            />

            <Stack.Screen
                name="editProfile"
                component={EditProfile}
            />


            <Stack.Screen
                name="liveChat"
                component={LiveChat}
            />

            <Stack.Screen
                name="faqScreen"
                component={FaqScreen}
            />

            <Stack.Screen
                name="feedback"
                component={FeedbackScreen}
            />
        </HomeStack.Navigator>
    );
};