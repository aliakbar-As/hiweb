import React, { useState, useContext, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    Dimensions, TouchableHighlight,
    TouchableOpacity,
    Modal, TouchableWithoutFeedback, FlatList,
    ToastAndroid,
    Alert,
    Linking,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Swiper from 'react-native-swiper';
import { Button, Header, Input, List, Loading, SafeView } from '../../../Commons';
import { SlideCard } from '../../../Elements';
import ProgressCircle from 'react-native-progress-circle'
import StoreContext from '../../../../Stores';
import { useObserver } from 'mobx-react-lite';
import moment from 'moment-jalaali';
import { Numbers, Storage } from '../../../../Utils';
import { CommonActions } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/AntDesign'

const { width, height } = Dimensions.get('window');


export default Tickets = ({ navigation, route }) => {
    const { AuthStore, TicketStore, CounterStore } = useContext(StoreContext);

    const [loadingList, setLoadingList] = useState(false);
    const [loading, setLoading] = useState(false);
    const [serviceId, setServiceId] = useState('');

    useEffect(() => {
        setMainServiceId();
        getTicketsData();
    }, []);

    const setMainServiceId = async () => {
        let serviceId = await Storage.GetItem('@serviceId');

        setServiceId(serviceId);
    };

    const getTicketsData = () => {
        setLoadingList(true);
        TicketStore.fetchData(true).then(() => {
            setLoadingList(false);
        });
    };

    const deleteRequest = (id) => {
        setLoadingList(true);
        TicketStore.deleteRequest(id).then(res => {
            if (res) {
                setLoadingList(false);
                getTicketsData();
                ToastAndroid.show("درخواست شما با موفقیت حذف شد !", ToastAndroid.SHORT);
            } else {
                Alert.alert('err', "اجرای درخواست با مشکل مواجه شد");
                setLoadingList(false);
            }
        })
    };

    const getSubjectData = () => {
        setLoading(true);
        TicketStore.getSubjectList().then(data => {
            CounterStore.fetchData(true).then(res => {
                navigation.navigate('ticketRequest', { subjectList: data, services: res });
                setLoading(false);
            });

        });
    };


    const getLink = () => {
        setLoading(true);
        TicketStore.getLink('LiveChat').then(res => {
            Linking.openURL(res);
            // navigation.navigate('liveChat')
            setLoading(false);
        });
    };

    const getFAQLink = () => {
        setLoading(true);
        TicketStore.getLink('FAQ').then(data => {
            navigation.navigate('faqScreen', { link: data })
            setLoading(false);
        });
    };
    
    return useObserver(() => (
        <SafeView>
            <Header
                title={"درخواست پشتیبانی"}
                backOnClick={() => navigation.pop()}
                plusOnclick={() => getSubjectData()}
                hasPlus noBack
                hasMessageIcon
                messageOnclick={() => navigation.navigate('feedback')}
            />

            <List
                data={TicketStore.data.filter(item => item.customerServiceId === serviceId)}
                onRefresh={() => getTicketsData()}
                showLoading={loadingList}
                ListHeader={() => (
                    <TouchableHighlight
                        underlayColor={'transparent'}
                        onPress={() => getFAQLink()}>
                        <View style={[styles.cardView, { justifyContent: 'center', alignItems: 'center', }]}>
                            <Text style={styles.faqTitle}>پرسش‌های متداول</Text>
                        </View>
                    </TouchableHighlight>
                )}
                ListEmptyComponent={() => {
                    if (!loadingList) {
                        return (
                            <Text style={styles.emptyDataTitle}>
                                موردی یافت نشد!
                            </Text>
                        )
                    } else return null;
                }}
                renderItem={(item) => (
                    <View style={styles.cardView}>
                        <View style={styles.innerCardView}>
                            <Text style={styles.hdrTitle}>{item.customerServiceName}</Text>
                            <Text style={styles.primaryTitle}>
                                سرویس:
                            </Text>
                        </View>

                        <View style={styles.innerCardView}>
                            <Text style={styles.hdrTitle}>
                                {item.subjectName}
                            </Text>

                            <Text style={styles.primaryTitle}>
                                عنوان :
                            </Text>
                        </View>

                        <View style={styles.innerCardView}>
                            <Text style={styles.hdrTitle}>{item.description}</Text>
                            <Text style={styles.primaryTitle}>توضیحات:</Text>
                        </View>

                        <View style={styles.innerCardView}>
                            <Text style={styles.hdrTitle}>{moment(item.createdOn).format('jYYYY/jMM/jDD   HH:mm')}</Text>
                            <Text style={styles.primaryTitle}>تاریخ ایجاد	:</Text>
                        </View>

                        <View style={styles.innerCardView}>
                            <View style={[styles.entityStatusView, { backgroundColor: item.entityStatus === 3 ? '#0F61AB' : item.entityStatus === 1 ? '#FF9800' : 'green', }]}>
                                <Text style={styles.entityStatusTitle}>{item.entityStatus === 3 ? 'لغو شده' : item.entityStatus === 1 ? 'در انتظار' : 'پاسخ داده شده'}</Text>
                            </View>
                            <Text style={styles.primaryTitle}>وضعیت: </Text>
                        </View>


                        {item.entityStatus === 1 ?
                            <Button
                                title={'لغو درخواست'}
                                showLoading={loadingList}
                                onPress={() => deleteRequest(item.id)}
                                extraStyles={styles.btn}
                                extraTitleStyle={{ color: '#2d2d2d' }}
                            /> : null}

                    </View>
                )}
            />

            {loading ? <Loading /> : null}

            <TouchableWithoutFeedback
                onPress={() => getLink()}
                underlayColor={'transparent'}>
                <View style={styles.liveChatContainer}>
                    <Icon
                        name={'wechat'}
                        size={20}
                        color={'#fff'}
                    />
                </View>
            </TouchableWithoutFeedback>
        </SafeView>
    ));
};


const styles = EStyleSheet.create({
    liveChatContainer: {
        padding: 10,
        backgroundColor: '#0F61AB',
        borderRadius: 100,
        margin: 10,
        position: 'absolute',
        right: 5,
        bottom: 5,
    },
    emptyDataTitle: {
        color: '#2d2d2d',
        textAlign: 'center',
        fontSize: 16,
        fontFamily: '$BOLD_FONT',
        marginTop: '50%',
        marginHorizontal: 32,
    },
    btn: {
        backgroundColor: 'transparent',
        borderWidth: 1,
        borderColor: '#ccc',
    },
    entityStatusView: {
        paddingVertical: 1,
        paddingHorizontal: 7,
        borderRadius: 10,
    },
    entityStatusTitle: {
        color: '#fff',
        fontSize: 12,
        fontFamily: '$REGULAR_FONT',
        textAlign: 'center'
    },
    titles: {
        fontSize: 13,
        color: '#505050',
        textAlign: 'right',
        fontFamily: '$REGULAR_FONT'
    },

    innerCardView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    faqTitle: {
        fontSize: 18,
        color: '#28BAD0',
        textAlign: 'center',
        fontFamily: '$BOLD_FONT',
    },
    hdrTitle: {
        fontSize: 16,
        color: '#505050',
        textAlign: 'right',
        fontFamily: '$BOLD_FONT',
        width: '70%'
    },
    cardView: {
        backgroundColor: '#fff',
        marginHorizontal: 16,
        marginTop: 10,
        borderRadius: 10,
        padding: 10,
        alignItems: 'flex-end',
        justifyContent: 'space-around',

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,

        elevation: 3,
        shadowRadius: 10,
    },
    primaryTitle: {
        fontSize: 16,
        color: '#505050',
        textAlign: 'right',
        fontFamily: '$REGULAR_FONT',
        width: '30%',
    },
    emptyDataTitle: {
        color: '#2d2d2d',
        textAlign: 'center',
        fontSize: 16,
        fontFamily: '$BOLD_FONT',
        marginTop: '50%',
        marginHorizontal: 32,
    },
});