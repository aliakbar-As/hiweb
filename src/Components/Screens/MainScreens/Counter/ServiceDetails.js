import React, { useState, useContext, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    Dimensions, TouchableHighlight,
    TouchableOpacity,
    Modal, TouchableWithoutFeedback, FlatList
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Swiper from 'react-native-swiper';
import { Button, Header, List, Loading, SafeView } from '../../../Commons';
import { SlideCard } from '../../../Elements';
import ProgressCircle from 'react-native-progress-circle'
import StoreContext from '../../../../Stores';
import { useObserver } from 'mobx-react-lite';
import moment from 'moment-jalaali';
import { Numbers } from '../../../../Utils';
import Icon from 'react-native-vector-icons/Feather';

const { width, height } = Dimensions.get('window');


export default ServiceDetails = ({ navigation, route }) => {
    const { AuthStore, ShoppingStore, BundleStore } = useContext(StoreContext);

    const [loading, setLoading] = useState(false);

    const { items } = route.params;

    // useEffect(() => {
    //     console.log('items', items);
    // }, []);

    const getProductList = () => {
        setLoading(true);
        ShoppingStore.setCustomerServiceId(items.customerServiceId);
        ShoppingStore.fetchData(true).then(res => {
            navigation.navigate('extraTraffic');
            setLoading(false);
        });
    };


    const getBundleList = () => {
        setLoading(true);
        BundleStore.setCustomerServiceId(items.customerServiceId);
        BundleStore.fetchData(true).then((data) => {
            navigation.navigate('bundle');
            setLoading(false);
        });

    };


    return useObserver(() => (
        <SafeView>
            <Header
                title={"اطلاعات سرویس"}
                backOnClick={() => navigation.pop()}
            />


            <ScrollView>

                <View style={styles.tableView}>


                    <View style={styles.innerTableView}>
                        <View style={{ width: '70%' }}>
                            <Text style={styles.hdrTitle}>{items.productServiceInfraName}</Text>
                        </View>

                        <View style={{ width: '30%' }}>
                            <Text style={styles.primaryTitle}>نام سرویس</Text>
                        </View>
                    </View>


                    <View style={styles.subTableView}>
                        <View style={{ width: '70%' }}>
                            <Text style={styles.hdrTitle}>{items.trafficInfo.trafficTitle}</Text>
                        </View>

                        <View style={{ width: '30%' }}>
                            <Text style={styles.primaryTitle}>حجم خریداری شده</Text>
                        </View>
                    </View>


                    <View style={styles.innerTableView}>
                        <View style={{ width: '70%' }}>
                            <View style={styles.subTableView}>
                                <Text style={[styles.hdrTitle, { marginRight: 10 }]}>
                                    (بین الملل)
                                </Text>
                                <Text style={[styles.hdrTitle]}>
                                    {items.trafficInfo.traffic_RemainedTitle}
                                </Text>
                            </View>

                            <View style={styles.subTableView}>
                                <Text numberOfLines={2} style={[styles.hdrTitle, { width: '80%', marginRight: 10 }]}>
                                    (در صورت استفاده از محتوای داخلی)
                                </Text>
                                <Text numberOfLines={10} style={[styles.hdrTitle,]}>
                                    {items.trafficInfo.traffic_Remained_InternalTitle}
                                </Text>
                            </View>
                        </View>

                        <View style={{ width: '30%' }}>
                            <Text style={styles.primaryTitle}>حجم باقیمانده</Text>
                        </View>
                    </View>


                    <View style={styles.innerTableView}>
                        <View style={{ width: '70%' }}>
                            <Text style={styles.hdrTitle}>{items.uplink} Kbps</Text>
                        </View>

                        <View style={{ width: '30%' }}>
                            <Text style={styles.primaryTitle}>سرعت آپلود</Text>
                        </View>
                    </View>

                    <View style={styles.innerTableView}>
                        <View style={{ width: '70%' }}>
                            <Text style={styles.hdrTitle}>{items.downlink} Kbps</Text>
                        </View>

                        <View style={{ width: '30%' }}>
                            <Text style={styles.primaryTitle}>سرعت دانلود</Text>
                        </View>
                    </View>


                    {items.trafficInfo.serviceNextStepDate !== null ?
                        <View style={styles.innerTableView}>
                            <View style={{ width: '70%' }}>
                                <Text style={styles.hdrTitle}>{moment(items.trafficInfo.serviceNextStepDate).format('jYYYY/jMM/jDD')}</Text>
                            </View>

                            <View style={{ width: '30%' }}>
                                <Text style={styles.primaryTitle}>تاریخ ترافیک ماهیانه بعدی</Text>
                            </View>
                        </View> : null}


                    <View style={styles.innerTableView}>
                        <View style={{ width: '70%' }}>
                            <Text style={styles.hdrTitle}>{items.trafficInfo.serviceNextStepTrafficTitle}</Text>
                        </View>

                        <View style={{ width: '30%' }}>
                            <Text style={styles.primaryTitle}>حجم ترافیک ماهیانه بعدی</Text>
                        </View>
                    </View>


                    {items.trafficInfo.servicePurchaseDate !== null ?
                        <View style={styles.innerTableView}>
                            <View style={{ width: '70%' }}>
                                <Text style={styles.hdrTitle}>{moment(items.trafficInfo.servicePurchaseDate).format('jYYYY/jMM/jDD   HH:mm')}</Text>
                            </View>

                            <View style={{ width: '30%' }}>
                                <Text style={styles.primaryTitle}>تاریخ خرید</Text>
                            </View>
                        </View> : null}


                    {items.trafficInfo.servieActivationDate !== null ?
                        <View style={styles.innerTableView}>
                            <View style={{ width: '70%' }}>
                                <Text style={styles.hdrTitle}>{moment(items.trafficInfo.servieActivationDate).format('jYYYY/jMM/jDD   HH:mm')}</Text>
                            </View>

                            <View style={{ width: '30%' }}>
                                <Text style={styles.primaryTitle}>تاریخ فعال سازی</Text>
                            </View>
                        </View> : null}


                    {items.trafficInfo.servieExpireDate !== null ?
                        <View style={styles.innerTableView}>
                            <View style={{ width: '70%' }}>
                                <Text style={styles.hdrTitle}>{moment(items.trafficInfo.servieExpireDate).format('jYYYY/jMM/jDD   HH:mm')}</Text>
                            </View>

                            <View style={{ width: '30%' }}>
                                <Text style={styles.primaryTitle}>تاریخ پایان</Text>
                            </View>
                        </View> : null}


                    <View style={styles.innerTableView}>
                        <Text style={[styles.primaryTitle, { color: '#31708F' }]}>
                            نکته: حجم باقیمانده شامل حجم از ابتدای فعال سازی سرویس تا این لحظه می باشد.(مجموع حجم دوره + ترافیک اضافه + حجم منتقل شده از دوره قبل)
                        </Text>
                    </View>
                </View>


            </ScrollView>

            <View>
            </View>
            {loading ? <Loading /> : null}
        </SafeView>
    ));
};


const styles = EStyleSheet.create({
    btnStyles: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    reportTitle: {
        fontSize: 16,
        color: '$LINK_COLOR',
        textAlign: 'center',
        fontFamily: '$REGULAR_FONT',
        marginVertical: 5,
    },
    subTableView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    innerTableView: {
        borderBottomWidth: 1,
        borderColor: '#eee',
        padding: 10,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center'
    },
    tableView: {
        justifyContent: 'flex-end',
        borderWidth: 1,
        borderColor: '#ccc',
        margin: 10,
        borderRadius: 10,
        backgroundColor: '#fff',
    },
    hdrContainer: {
        backgroundColor: '#CFD8DC',
        marginTop: 16,
        padding: 10,
    },
    productCardContainer: {
        width: width - 16,
        height: height / 6,
        backgroundColor: '#fff',
        alignSelf: 'center',
        marginTop: 10,
        borderRadius: 10,
        padding: 10,


        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,

        elevation: 5
        ,
        shadowRadius: 10,
    },
    amountTitle: {
        color: '$MAIN_THEME',
        textAlign: 'right',
        fontSize: 14,
        position: 'absolute',
        left: 10,
        fontFamily: '$BOLD_FONT',
        bottom: 5,
    },
    primaryTitle: {
        fontSize: 14,
        color: '#505050',
        textAlign: 'right',
        fontFamily: '$REGULAR_FONT'
    },
    hdrTitle: {
        fontSize: 15,
        color: '#2D2D2D',
        textAlign: 'right',
        fontFamily: '$BOLD_FONT',
    },
});