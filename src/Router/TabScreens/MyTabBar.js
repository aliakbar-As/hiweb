import React from 'react';
import {
    View, Text,
    Dimensions,
    TouchableWithoutFeedback
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Icon from 'react-native-vector-icons/AntDesign';
import { ChallengeStore } from '../../Stores/ChallengeStore';

const widthScreen = Dimensions.get('window').width;

export default MyTabBar = ({ state, descriptors, navigation }) => {
    const focusedOptions = descriptors[state.routes[state.index].key].options;

    if (focusedOptions.tabBarVisible === false) {
        return null;
    }

    return (
        <View style={{ flexDirection: 'row' }}>
            {state.routes.map((route, index) => {
                const { options } = descriptors[route.key];
                const label =
                    options.tabBarLabel !== undefined ? options.tabBarLabel : route.name;
                // const icon =
                //     options.iconName !== undefined ? options.iconName : options.title !== undefined ? options.title : route.name;

                const isFocused = state.index === index;

                const onPress = () => {
                    const event = navigation.emit({
                        type: 'tabPress',
                        target: route.key,
                        canPreventDefault: true,
                    });

                    if (!isFocused && !event.defaultPrevented) {
                        navigation.navigate(route.name);
                    }
                };

                const onLongPress = () => {
                    navigation.emit({
                        type: 'tabLongPress',
                        target: route.key,
                    });
                };
                if (ChallengeStore.data.length === 0 && label === 'چالش') return null;
<<<<<<< HEAD
=======

>>>>>>> f4d81353b7a9c0d70ec16fd7a5857df2f5c9617b
                return (
                    
                    <TouchableWithoutFeedback
                        key={index}
                        accessibilityRole="button"
                        accessibilityState={isFocused ? { selected: true } : {}}
                        accessibilityLabel={options.tabBarAccessibilityLabel}
                        testID={options.tabBarTestID}
                        onPress={onPress}
                        onLongPress={onLongPress}
                    // style={styles.tabContainer}
                    >
                        <View style={[styles.tabContainer, { width: ChallengeStore.data.length !== 0 ? widthScreen / 5 : widthScreen / 4, }]}>
                            <Icon
                                name={options.iconName}
                                size={20}
                                style={{ alignSelf: 'center' }}
                                color={isFocused ? EStyleSheet.value('$MAIN_THEME') : 'gray'}
                            />
                            <Text style={[styles.tabTitles, { color: isFocused ? EStyleSheet.value('$MAIN_THEME') : 'gray' }]}>
                                {label}
                            </Text>
                        </View>
                    </TouchableWithoutFeedback>
                );
            })}
        </View>
    );
};

const styles = EStyleSheet.create({
    tabTitles: {
        color: 'gray',
        fontSize: 10,
        textAlign: 'center',
        fontFamily: '$REGULAR_FONT'
    },
    tabContainer: {
        // height: 40,
        paddingVertical: 3,
        width: widthScreen / 5,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fafafa',
        flexDirection: 'column',
        borderTopWidth: 1,
        borderColor: '#ccc'
    }
});