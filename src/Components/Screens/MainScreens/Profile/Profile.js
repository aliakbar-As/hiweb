import React, { useState, useContext, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    Dimensions, TouchableHighlight,
    TouchableOpacity,
    Modal, TouchableWithoutFeedback, FlatList,
    ToastAndroid,
    Alert
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Swiper from 'react-native-swiper';
import { Button, Header, Input, List, Loading, SafeView } from '../../../Commons';
import { SlideCard } from '../../../Elements';
import ProgressCircle from 'react-native-progress-circle'
import StoreContext from '../../../../Stores';
import { useObserver } from 'mobx-react-lite';
import moment from 'moment-jalaali';
import { Numbers, Storage } from '../../../../Utils';
import { CommonActions } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Fontisto';

const { width, height } = Dimensions.get('window');


export default Profile = ({ navigation, route }) => {
    const { AuthStore, ShoppingStore, BundleStore } = useContext(StoreContext);

    const [loading, setLoading] = useState(false);


    const userSignout = () => {
        ShoppingStore.resetCartList();
        Storage.Clear();
        Storage.RemoveItem('@password');
        navigation.dispatch(CommonActions.reset({
            routes: [{ name: 'login' }],
        }));
    };


    return useObserver(() => (
        <SafeView>
            <Header
                title={AuthStore.userModel.isContact ? 'کاربر حقیقی' : 'کاربر حقوقی'}
                backOnClick={() => navigation.pop()}
            // hasEdit
            />

            <ScrollView>


                {AuthStore.userModel.isContact ?
                    <View style={{ padding: 10, }}>
                        <View style={styles.iconView}>
                            <Icon
                                name={AuthStore.userModel.gender === 0 ? 'male' : 'female'}
                                size={width / 8}
                                color={'#2d2d2d'}
                            />
                        </View>


                        <View style={styles.line} />

                        <View style={{ alignItems: 'flex-end', paddingRight: 10 }}>
                            <TouchableOpacity onPress={() => navigation.navigate('changePassword')}>
                                <Text style={[styles.titles, { textAlign: 'center', color: '#007ACC' }]}>تغییر رمز عبور</Text>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => navigation.navigate('editProfile')}>
                                <Text style={[styles.titles, { textAlign: 'center', color: '#007ACC' }]}>ویرایش فعال سازی خودکار</Text>
                            </TouchableOpacity>
                        </View>


                        <View style={{ marginVertical: 16, }}>
                            <Text style={styles.innerTitles}>نام و نام‌خانوادگی</Text>
                            <View style={styles.cardView}>
                                <Text style={styles.titles}>{AuthStore.userModel.fullName}</Text>
                            </View>
                        </View>


                        <View style={{ marginBottom: 16, }}>
                            <Text style={styles.innerTitles}>نام پدر</Text>
                            <View style={styles.cardView}>
                                <Text style={styles.titles}>{AuthStore.userModel.fatherName}</Text>
                            </View>
                        </View>

                        <View style={{ marginBottom: 16, }}>
                            <Text style={styles.innerTitles}>کد ملی</Text>
                            <View style={styles.cardView}>
                                <Text style={styles.titles}>{AuthStore.userModel.nationalId} </Text>
                            </View>
                        </View>

                        <View style={{ marginBottom: 16, }}>
                            <Text style={styles.innerTitles}>ایمیل</Text>
                            <View style={styles.cardView}>
                                <Text style={styles.titles}>
                                    {!AuthStore.userModel.IsEmailVerified ? AuthStore.userModel.email : 'ایمیل شما هنوز تایید نشده است'}
                                </Text>
                            </View>
                        </View>

                        <View style={{ marginBottom: 16, }}>
                            <Text style={styles.innerTitles}>شماره شناسنامه</Text>
                            <View style={styles.cardView}>
                                <Text style={styles.titles}>{AuthStore.userModel.certificateNo}</Text>
                            </View>
                        </View>

                        <View style={{ marginBottom: 16, }}>
                            <Text style={styles.innerTitles}>کد پستی</Text>
                            <View style={styles.cardView}>
                                <Text style={styles.titles}>{AuthStore.userModel.postalCode}</Text>
                            </View>
                        </View>

                        <View style={{ marginBottom: 16, }}>
                            <Text style={styles.innerTitles}>موبایل</Text>
                            <View style={styles.cardView}>
                                <Text style={styles.titles}>
                                    {!AuthStore.userModel.IsMobileVerified ? AuthStore.userModel.mobile : 'شماره موبایل شما هنوز تایید نشده است'}
                                </Text>
                            </View>
                        </View>

                    </View>
                    :
                    <View style={{ padding: 10, }}>
                        <View style={{ alignItems: 'flex-end', paddingRight: 10 }}>
                            <TouchableOpacity onPress={() => navigation.navigate('changePassword')}>
                                <Text style={[styles.titles, { textAlign: 'center', color: '#007ACC' }]}>تغییر رمز عبور</Text>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => navigation.navigate('editProfile')}>
                                <Text style={[styles.titles, { textAlign: 'center', color: '#007ACC' }]}>ویرایش فعال سازی خودکار</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{ marginBottom: 16, }}>
                            <Text style={styles.innerTitles}>نام شرکت</Text>
                            <View style={styles.cardView}>
                                <Text style={styles.titles}>{AuthStore.userModel.organizationName}</Text>
                            </View>
                        </View>


                        <View style={{ marginBottom: 16, }}>
                            <Text style={styles.innerTitles}>تلفن</Text>
                            <View style={styles.cardView}>
                                <Text style={styles.titles}>{AuthStore.userModel.organizationPhone}</Text>
                            </View>
                        </View>

                        <View style={{ marginBottom: 16, }}>
                            <Text style={styles.innerTitles}>کد اقتصادی</Text>
                            <View style={styles.cardView}>
                                <Text style={styles.titles}>{AuthStore.userModel.organizationEconomicCode} </Text>
                            </View>
                        </View>

                        <View style={{ marginBottom: 16, }}>
                            <Text style={styles.innerTitles}>ایمیل</Text>
                            <View style={styles.cardView}>
                                <Text style={styles.titles}>
                                    {!AuthStore.userModel.IsEmailVerified ? AuthStore.userModel.email : 'ایمیل شما هنوز تایید نشده است'}
                                </Text>
                            </View>
                        </View>

                        <View style={{ marginBottom: 16, }}>
                            <Text style={styles.innerTitles}>شماره ثبت</Text>
                            <View style={styles.cardView}>
                                <Text style={styles.titles}>{AuthStore.userModel.organizationRegisterId}</Text>
                            </View>
                        </View>

                        <View style={{ marginBottom: 16, }}>
                            <Text style={styles.innerTitles}>کد پستی</Text>
                            <View style={styles.cardView}>
                                <Text style={styles.titles}>{AuthStore.userModel.postalCode}</Text>
                            </View>
                        </View>

                    </View>
                }


                <Button
                    onPress={() => userSignout()}
                    title={'خروج از حساب کاربری'}
                    extraStyles={{ width: '95%' }}
                />
            </ScrollView>

            {loading ? <Loading /> : null}
        </SafeView>
    ));
};


const styles = EStyleSheet.create({
    cardView: {
        borderWidth: 1,
        borderColor: '#2d2d2d',
        alignSelf: 'center',
        borderRadius: 10,
        width: width - 32,
        padding: 10,
        alignItems: 'center',
        marginTop: 5,
    },
    line: {
        width: '30%',
        height: 5,
        alignSelf: 'center',
        marginVertical: 16,
        borderRadius: 30,
        backgroundColor: '#44D69B'
    },
    iconView: {
        width: width / 5,
        height: width / 5,
        borderRadius: 50,
        borderRadius: 10,
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
    },
    warningView: {
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#5778AA',
        backgroundColor: '#D9EDF7',
        padding: 10,
        margin: 10
    },
    titles: {
        fontSize: 16,
        color: '#2d2d2d',
        textAlign: 'right',
        fontFamily: '$REGULAR_FONT',
    },
    innerTitles: {
        fontSize: 15,
        color: '#2d2d2d',
        textAlign: 'right',
        fontFamily: '$BOLD_FONT',
        marginRight: 10,
    },
});