import React from 'react';
import { View, Text, } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { CommonActions } from '@react-navigation/native';
import { Storage } from '../../Utils';

import MyTabBar from './MyTabBar';
import CounterScreens from './CounterScreens';
import { Button } from '../../Components/Commons';
import ShoppingScreens from './ShoppingScreens';
import TicketScreens from './TicketScreens';
import ServiceReportScreens from './ServiceReportScreens';
import ChallengeScreens from './ChallengeScreens';

const Tab = createBottomTabNavigator();


export default StudentTabScreens = () => {
    return (
        <Tab.Navigator
            initialRouteName={'counterScreens'}
            tabBar={props => <MyTabBar {...props} />}>


            <Tab.Screen
                name="serviceReport"
                component={ServiceReportScreens}
                options={{
                    tabBarLabel: 'گزارش‌ها',
                    iconName: 'notification'
                }}
            />

            <Tab.Screen
                name="ticket"
                component={TicketScreens}
                options={{
                    tabBarLabel: 'پشتیبانی',
                    iconName: 'message1'
                }}
            />

            <Tab.Screen
                name="shopping"
                component={ShoppingScreens}
                options={{
                    tabBarLabel: 'خرید',
                    iconName: 'shoppingcart'
                }}
            />

            <Tab.Screen
                name="challengeScreens"
                component={ChallengeScreens}
                options={({ route }) => {
                    return ({
                        tabBarLabel: 'چالش',
                        iconName: 'Trophy',
                    })
                }}
            />

            <Tab.Screen
                name="counterScreens"
                component={CounterScreens}
                options={({ route }) => {
                    // console.log('route', route.params)
                    return ({
                        tabBarLabel: 'پیشخوان',
                        iconName: 'earth',
                        tabBarVisible: route.name === 'play' ? false : true
                    })
                }}
            />
        </Tab.Navigator>
    );
};