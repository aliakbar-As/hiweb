import React from 'react';
import { SafeAreaView, StatusBar } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';



export const SafeView = ({ children, style }) => {
    return (
        <SafeAreaView style={[styles.container, style]}>
            <StatusBar 
            backgroundColor={'#0F61AB'}
            // backgroundColor={EStyleSheet.value('$MAIN_THEME')}
            barStyle="light-content" 
            translucent={false} />
            {children}
        </SafeAreaView>
    );
};

const styles = {
    container: {
        flex: 1,
        backgroundColor: '#eee'
    },
    pageWrapper: {
        flex: 1,
        backgroundColor: '#eee'
    }
};
