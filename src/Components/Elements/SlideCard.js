import React from 'react';
import {
    Image,
    View,
    Text,
    Dimensions,
    TouchableHighlight
} from 'react-native';


const { width, height } = Dimensions.get('window');

export const SlideCard = ({ source, onPress, disabled }) => {
    return (
        <TouchableHighlight
            onPress={onPress}
            disabled={disabled}
            underlayColor={'transparent'}>
            <Image
                source={source}
                style={styles.slideCardImageStyle}
            />
        </TouchableHighlight>
    );
};
const styles = {
    slideCardImageStyle: {
        width: width - 32,
        height: width / 2,
        alignSelf: 'center',
        borderRadius: 10,
        marginHorizontal: 10,
    },
};