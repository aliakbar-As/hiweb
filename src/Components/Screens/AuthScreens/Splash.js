import React, { useEffect, useContext, useState } from 'react';
import { View, Image, sl, Text, ImageBackground, StatusBar, Dimensions } from 'react-native';
import { CommonActions } from '@react-navigation/native';
import EStyleSheet from 'react-native-extended-stylesheet';
import AsyncStorage from '@react-native-community/async-storage';
import StoreContext from '../../../Stores';

import { Button, SafeView } from '../../Commons';
import { Logger, Storage } from '../../../Utils';

const { width, height } = Dimensions.get('window');

const splash = require('../../../assets/images/parsSplash.jpg');

export default Splash = ({ navigation }) => {
    const { AuthStore, ChallengeStore } = useContext(StoreContext);

    useEffect(() => {
        // Storage.Clear();
        checkUserToken();
    }, []);

    const checkUserToken = () => {

        AsyncStorage.getItem('@token', (error, result) => {
            Logger(result, 'token');

            if (result !== null) {
                AuthStore.getUserInfo().then(res => {
                    console.log('res', res)
                    ChallengeStore.fetchData(true).then(() => {
                        navigation.dispatch(CommonActions.reset({
                            routes: [{ name: 'tabScreens' }],
                        }));
                    });
<<<<<<< HEAD
=======

>>>>>>> f4d81353b7a9c0d70ec16fd7a5857df2f5c9617b
                });
            } else {
                navigation.dispatch(CommonActions.reset({
                    routes: [{ name: 'login' }],
                }));
            };
        });
    };

    return (
        <SafeView>
            <StatusBar translucent backgroundColor={EStyleSheet.value('$MAIN_THEME')} />

            <View style={styles.innerContainer}>
                <Image
                    source={splash}
                    resizeMode={'contain'}
                    style={styles.splashImg}
                />

                <Text style={styles.bottomTitle}>
<<<<<<< HEAD
                    {/* شرکت داده گستر عصر نوین */}
                </Text>
            </View>
=======
                    شرکت داده گستر عصر نوین
                </Text>
            </View>
            {/* 
            <Button 
            title={'clear btn'}
            onPress={() => Storage.Clear()}
            /> */}
>>>>>>> f4d81353b7a9c0d70ec16fd7a5857df2f5c9617b
        </SafeView>
    );
};

const styles = EStyleSheet.create({
    bottomTitle: {
        textAlign: 'center',
        color: '#fff',
        fontSize: 16,
        position: 'absolute',
        bottom: 10,
        right: 0,
        left: 0,
        fontFamily: '$BOLD_FONT'
    },
    splashImg: {
        width: width,
        height: height,
        alignSelf: 'center',
    },
    innerContainer: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#36618F',
        justifyContent: 'center',
    },
    title: {
        color: '#fff',
        textAlign: 'center',
        fontSize: 32,
        fontWeight: 'bold',
        marginRight: 32,
    },
    titleContainer: {
        alignSelf: 'stretch',
        borderBottomWidth: 5,
        borderTopWidth: 5,
        borderColor: '#fff',
        padding: 5,
        marginTop: 50,
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

});