import React, { useState, useContext, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    Dimensions, TouchableHighlight,
    TouchableOpacity,
    Modal, TouchableWithoutFeedback,

} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Button, Header, List, Loading, SafeView } from '../../../Commons';
import StoreContext from '../../../../Stores';
import { useObserver } from 'mobx-react-lite';

const { width, height } = Dimensions.get('window');

export default ServiceReport = ({ navigation, route }) => {
    const { AuthStore, CounterStore, ServiceReportStore, ShoppingStore } = useContext(StoreContext);

    const { data } = route.params;

    const [loading, setLoading] = useState(false);
    const [loadingList, setLoadingList] = useState(false);
    const [services, setServices] = useState([]);


    return useObserver(() => (
        <SafeView>
            <Header
                title="جزئیات"
                backOnClick={() => navigation.pop()}
            />

            <ScrollView>
                <List
                    contentContainerStyle={{ alignItems: 'flex-end', justifyContent: 'space-around' }}
                    style={styles.cardView}
                    disableRefresh
                    ListHeader={() => (

                        <View>

                            <View style={styles.innerCardView}>
                                <Text style={styles.hdrTitle}>{data.startDateTimeString}</Text>
                                <Text style={styles.primaryTitle}>شروع : </Text>
                            </View>

                            <View style={styles.innerCardView}>
                                <Text style={styles.hdrTitle}>{data.lastUpdateDateTimeString}</Text>
                                <Text style={styles.primaryTitle}>پایان : </Text>
                            </View>

                            <View style={styles.innerCardView}>
                                <Text style={styles.hdrTitle}>{data.connectionTimeString}</Text>
                                <Text style={styles.primaryTitle}>مدت : </Text>
                            </View>


                            <View style={styles.innerCardView}>
                                <Text style={styles.hdrTitle}>{data.disconnect}</Text>
                                <Text style={styles.primaryTitle}>وضعیت / دلیل قطع:</Text>
                            </View>

                            <View style={styles.innerCardView}>
                                <Text style={styles.hdrTitle}>
                                    {data.ipAddress}
                                    {'\n'}
                                    {data.callerId}
                                </Text>
                                <Text style={styles.primaryTitle}>
                                    : IP
                                    {'\n'}
                                    : MAC
                                </Text>
                            </View>


                            <View style={styles.viewStyle}>

                                <Text style={styles.tableTitles}>
                                    دانلود / آپلود MB
                                {'\n'}
                                    اعمال ضریب
                                </Text>

                                <Text style={styles.tableTitles}>
                                    دانلود / آپلود
                                </Text>

                                <Text style={styles.tableTitles}>
                                    نام
                                </Text>
                            </View>
                        </View>
                    )}
                    data={data.subSessions}
                    renderItem={(item) => {
                        return (
                            <View style={styles.tableVeiw}>
                                <Text style={[styles.tableTitles, { color: '#2d2d2d', }]}>
                                    {item.downloadWithCoefficientMBString}
                                    {'\n'}
                                    (ضریب {item.coefficientDownload})
                                    {'\n'}
                                    {item.uploadWithCoefficientMBString}
                                    {'\n'}
                                    (ضریب {item.coefficientUpload})
                                </Text>

                                <Text style={[styles.tableTitles, { color: '#2d2d2d', }]}>
                                    {item.downloadMBString}
                                    {'\n'}
                                    {item.uploadMBString}
                                </Text>

                                <Text style={[styles.tableTitles, { color: '#2d2d2d', }]}>
                                    {item.serviceInfo}
                                </Text>
                            </View>
                        )
                    }}
                />


                <View style={styles.footerViewStyle}>
                    <View style={{ width: '35%' }}>
                        <Text style={styles.titles}>{data.totalCoefficientDownloadString}</Text>
                        <Text style={styles.titles}>{data.totalCoefficientUploadString}</Text>
                    </View>

                    <View style={{ width: '35%' }}>
                        <Text style={styles.titles}>{data.totalDownloadString}</Text>
                        <Text style={styles.titles}>{data.totalUploadString}</Text>
                    </View>

                    <View style={{ width: '30%' }}>
                        <Text style={styles.titles}>جمع:</Text>
                    </View>
                </View>
            </ScrollView>
        </SafeView>
    ));
};


const styles = EStyleSheet.create({
    footerViewStyle: {
        borderTopWidth: 1,
        flexDirection: 'row',
        borderColor: '#2d2d2d',
        alignItems: 'center',
        justifyContent: 'space-around',
        paddingHorizontal: 10,
        backgroundColor: '#fff',
        marginTop: -16,
        width: width - 16,
        alignSelf: 'center',
        paddingTop: 5,
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
        marginBottom: 16,
        paddingBottom: 10,
    },
    viewStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        width: width - 32,
        backgroundColor: '#36618F',
        borderRadius: 10,
        justifyContent: 'space-between',
        paddingVertical: 10,
        marginTop: 32,
    },
    tableVeiw: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        width: width - 32,
        borderRadius: 10,
        justifyContent: 'space-between',
        padding: 10,
    },
    tableTitles: {
        fontSize: 14,
        color: '#fff',
        textAlign: 'center',
        fontFamily: '$BOLD_FONT',
        width: '34%'
    },
    titles: {
        fontSize: 16,
        color: '#2d2d2d',
        textAlign: 'center',
        fontFamily: '$BOLD_FONT'
    },
    activeContainer: {
        width: 10,
        marginLeft: 10,
        height: 10,
        borderRadius: 15,
        backgroundColor: '#6CD7B0'
    },
    statusContainer: {
        alignItems: 'center',
        backgroundColor: '#EDF1FD',
        flexDirection: 'row',
        paddingHorizontal: 5,
        borderRadius: 16,
        paddingVertical: 1,
        position: 'absolute',
        left: 0,
    },
    innerCardView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingHorizontal: 10,
    },
    hdrTitle: {
        fontSize: 16,
        color: '#505050',
        textAlign: 'right',
        fontFamily: '$BOLD_FONT',
        width: '70%'
    },
    cardView: {
        backgroundColor: '#fff',
        marginTop: 10,
        borderRadius: 10,
        width: width - 16,
        alignSelf: 'center',
    },
    primaryTitle: {
        fontSize: 16,
        color: '#505050',
        textAlign: 'right',
        fontFamily: '$REGULAR_FONT',
        width: '30%',
    },
    emptyDataTitle: {
        color: '#2d2d2d',
        textAlign: 'center',
        fontSize: 16,
        fontFamily: '$BOLD_FONT',
        marginTop: '50%',
        marginHorizontal: 32,
    },
    tabMainView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    filterView: {
        width: width / 3,
        alignSelf: 'center',
        justifyContent: 'center',
        height: width / 9,
        overflow: 'hidden',
        borderBottomWidth: 1,
        backgroundColor: '#fff'
    },
    filterTitle: {
        color: '#12141D',
        textAlign: 'center',
        fontFamily: '$BOLD_FONT',
        fontSize: 16
    },
});