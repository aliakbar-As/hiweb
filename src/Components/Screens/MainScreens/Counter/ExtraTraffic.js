import React, { useState, useContext, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    Dimensions, TouchableHighlight,
    TouchableOpacity,
    Modal, TouchableWithoutFeedback, FlatList
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Swiper from 'react-native-swiper';
import { Button, Header, List, Loading, SafeView } from '../../../Commons';
import { ServiceProductCard, SlideCard } from '../../../Elements';
import ProgressCircle from 'react-native-progress-circle'
import StoreContext from '../../../../Stores';
import { useObserver } from 'mobx-react-lite';
import moment from 'moment-jalaali';
import { Numbers } from '../../../../Utils';

const { width, height } = Dimensions.get('window');


export default ExtraTraffic = ({ navigation }) => {
    const { AuthStore, ShoppingStore, ShoppingProfileStore } = useContext(StoreContext);

    const [loading, setLoading] = useState(false);
    const [loadingList, setLoadingList] = useState(false);

    const getProductDetails = (item) => {
        setLoading(true);
        ShoppingProfileStore.getProductDetail(item.promotionProductId).then(res => {
            navigation.navigate('productDetails', {
                trafficInternalWithUnit: item.trafficInternalWithUnit,
                trafficWithUnit: item.trafficWithUnit,
                amount: item.amount,

            });
            setLoading(false);
        });
    };

    return useObserver(() => (
        <SafeView>
            <Header
                title="ترافیک اضافه"
                backOnClick={() => navigation.pop()}
            />

            <List
                data={ShoppingStore.data}
                ListEmptyComponent={() => {
                    if (!loadingList) {
                        return (
                            <Text style={styles.emptyDataTitle}>
                                موردی یافت نشد!
                            </Text>
                        )
                    } else return null;
                }}
                showLoading={loadingList}
                onRefresh={() => {
                    setLoadingList(true);
                    ShoppingStore.fetchData(true).then(() => setLoadingList(false));
                }}
                renderItem={(item) => {
                    if (item.id === 'c7f8d2e6-f25d-ea11-a94d-005056818579') {
                        return (
                            <View>
                                <View style={styles.hdrContainer}>
                                    <Text style={styles.hdrTitle}>
                                        {item.name}
                                    </Text>
                                </View>

                                <List
                                    data={item.poducts}
                                    horizontal
                                    inverted
                                    showsHorizontalScrollIndicator={false}
                                    ListEmptyComponent={() => {
                                        if (!loadingList) {
                                            return (
                                                <Text style={styles.emptyDataTitle}>
                                                    موردی یافت نشد!
                                                </Text>
                                            )
                                        } else return null;
                                    }}
                                    renderItem={(item) => {
                                        return (
                                            <ServiceProductCard
                                                // onPress={() => getProductDetails(item)}
                                                // categoryPriority={item.categoryPriority}
                                                // trafficInternalWithUnit={item.trafficInternalWithUnit}
                                                // trafficWithUnit={item.trafficWithUnit}
                                                // productName={item.productName}
                                                // amount={item.amount}
                                                // time={null}
                                                // buyOnclick={() => getProductDetails(item)}

                                                showInternalTraffic={ShoppingStore.productServiceInfraName !== 'LTE'}
                                                onPress={() => getProductDetails(item)}
                                                categoryPriority={item.categoryPriority}
                                                time={item.categoryId === 'c7f8d2e6-f25d-ea11-a94d-005056818579' || item.categoryId === 'd7f8d2e6-f25d-ea11-a94d-005056818579' ? null : item.time}
                                                trafficInternal={item.trafficInternal / 1024 === null ? item.trafficInternal : item.trafficInternal / 1024}
                                                trafficInternalWithUnit={item.trafficInternalWithUnit}
                                                trafficWithUnit={item.categoryId === 'd7f8d2e6-f25d-ea11-a94d-005056818579' ? null : item.trafficWithUnit}
                                                productName={item.productName}
                                                amount={item.amount}
                                                buyOnclick={() => getProductDetails(item)}
                                                catId={item.categoryId}
                                            />
                                        )
                                    }}
                                />
                            </View>
                        );
                    };
                }}
            />

            {loading ? <Loading /> : null}
        </SafeView>
    ));
};


const styles = EStyleSheet.create({
    emptyDataTitle: {
        color: '#2d2d2d',
        textAlign: 'center',
        fontSize: 16,
        fontFamily: '$BOLD_FONT',
        marginTop: '45%',
        marginHorizontal: 32,
    },
    hdrView: {
        borderWidth: 1,
        borderTopWidth: 0,
        borderBottomRightRadius: 50,
        borderBottomLeftRadius: 50,
        borderColor: '#eee',
        paddingHorizontal: 32,
        // backgroundColor: 'red',
        alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,

        elevation: 1,
        shadowRadius: 10,
    },
    hdrContainer: {
        backgroundColor: '#CFD8DC',
        marginTop: 16,
        padding: 10,
        width: '100%',
    },
    productCardContainer: {
        width: width / 2 - 10,
        // height: height / 2 - 50,
        paddingBottom: 20,
        backgroundColor: '#fff',
        alignSelf: 'center',
        marginTop: 10,
        borderRadius: 20,
        marginHorizontal: 5,

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,

        elevation: 5,
        shadowRadius: 10,
    },
    amountTitle: {
        color: '$MAIN_THEME',
        textAlign: 'right',
        fontSize: 14,
        position: 'absolute',
        left: 10,
        fontFamily: '$BOLD_FONT',
        bottom: 5,
    },
    primaryTitle: {
        fontSize: 13,
        color: '#505050',
        textAlign: 'center',
        fontFamily: '$REGULAR_FONT'
    },
    hdrTitle: {
        fontSize: 16,
        color: '#2D2D2D',
        textAlign: 'right',
        fontFamily: '$BOLD_FONT',
    },
});