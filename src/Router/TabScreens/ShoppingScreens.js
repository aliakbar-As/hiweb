import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import ShoppingCard from '../../Components/Screens/MainScreens/Shopping/ShoppingCard';
import ProductList from '../../Components/Screens/MainScreens/Shopping/ProductList';
import ProductDetails from '../../Components/Screens/MainScreens/Shopping/ProductDetails';
import Cart from '../../Components/Screens/MainScreens/Shopping/Cart';
import Checkout from '../../Components/Screens/MainScreens/Shopping/Checkout';
import Profile from '../../Components/Screens/MainScreens/Profile/Profile';
import ChangePassword from '../../Components/Screens/MainScreens/Profile/ChangePassword';
import EditProfile from '../../Components/Screens/MainScreens/Profile/EditProfile';


const HomeStack = createStackNavigator();
const Stack = createStackNavigator();

export default ShoppingScreens = ({ navigation }) => {
    return (
        <HomeStack.Navigator
            initialRouteName="productList"
            headerMode="none"
            mode={'card'}
        >

            {/* <Stack.Screen
                name="shoppingCard"
                component={ShoppingCard}
            /> */}

            <Stack.Screen
                name="productList"
                component={ProductList}
            />

            <Stack.Screen
                name="cart"
                component={Cart}
            />

            <Stack.Screen
                name="checkout"
                component={Checkout}
            />

            <Stack.Screen
                name="profile"
                component={Profile}
            />

            <Stack.Screen
                name="changePassword"
                component={ChangePassword}
            />

            <Stack.Screen
                name="editProfile"
                component={EditProfile}
            />

        </HomeStack.Navigator>
    );
};