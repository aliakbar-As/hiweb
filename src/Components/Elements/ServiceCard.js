import React, { useState, useContext, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    Dimensions, TouchableHighlight,
    TouchableOpacity,
    Modal, TouchableWithoutFeedback, FlatList
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Button, Header, List, Loading, SafeView } from '../Commons';
import ProgressCircle from 'react-native-progress-circle'

const { width, height } = Dimensions.get('window');

export const ServiceCard = ({
    serviceName,
    statusTitle,
    expireDate,
    remindTrafficPercentage,
    remindTrafficTitle,
    trafficTitle,
    timeRemindPercentage,
    timeRemindTitle,
    serviceTime,
    onPress,
    statusCode,
    mainOnPress,
    showTrafficButton,
    phoneNumber,
    postalCode,
    stopDate,
    onGetService
}) => {
    return (
        <TouchableWithoutFeedback onPress={mainOnPress}>
            <View style={styles.counterContainer}>
                <Text style={styles.pHdrTitle}>{serviceName} {phoneNumber !== null ? ' | ' + phoneNumber : ''} {postalCode !== null ? ' | ' + postalCode : ''}</Text>
                <View style={styles.counterHdrView}>
                    <View style={styles.statusContainer}>
                        <Text style={styles.primaryTitle}>{statusTitle}</Text>
                        <View style={[styles.activeContainer, {
                            backgroundColor: statusCode === 13 ? '#FF9800' : statusCode === 12 ? '#6CD7B0' : 'red',
                        }]} />
                    </View>
                    {expireDate !== null ?
                        <Text style={styles.primaryTitle}>پایان سرویس {expireDate}</Text>
                        : null}

                </View>


                <View style={styles.progressContainers}>
                    <View style={{ alignItems: 'center', justifyContent: 'space-around', }}>
                        <Text style={styles.hdrTitle}>حجم باقیمانده</Text>
                        <ProgressCircle
                            percent={remindTrafficPercentage}
                            radius={50}
                            borderWidth={8}
                            color="#3399FF"
                            shadowColor="#CCDBF7"
                            bgColor="#fff"
                            outerCircleStyle={{ marginVertical: 5 }}
                        >
                            <Text style={styles.hdrTitle}>
                                {remindTrafficTitle}
                            </Text>
                        </ProgressCircle>

                        <Text style={styles.primaryTitle}>حجم خریداری شده: {trafficTitle}</Text>
                    </View>

                    <View style={{ alignItems: 'center', justifyContent: 'space-around', }}>
                        <Text style={styles.hdrTitle}>روز باقیمانده</Text>

                        <ProgressCircle
                            percent={timeRemindPercentage}
                            radius={50}
                            borderWidth={8}
                            color={'#E3000F'}
                            shadowColor="#CCDBF7"
                            outerCircleStyle={{ marginVertical: 5 }}
                            bgColor="#fff"
                        >
                            <Text style={styles.hdrTitle}>
                                {timeRemindTitle} مانده
                            </Text>
                        </ProgressCircle>

                        <Text style={styles.primaryTitle}>زمان کل: {serviceTime}</Text>
                    </View>

                </View>

                {showTrafficButton ?
                    <Button
                        title={'خرید ترافیک'}
                        onPress={onPress}
                        extraStyles={styles.btnStyle}
                    /> : <View style={{ margin: 3 }} />}


                {!showTrafficButton && stopDate !== null ?
                    <Button
                        title={'خرید سرویس'}
                        onPress={onGetService}
                        extraStyles={styles.btnStyle}
                    /> : <View style={{ margin: 3 }} />}

            </View>
        </TouchableWithoutFeedback>
    );
};

const styles = EStyleSheet.create({
    pHdrTitle: {
        fontSize: 16,
        color: '#2D2D2D',
        textAlign: 'center',
        fontFamily: '$BOLD_FONT',
    },
    btnStyle: {
        width: '95%',
    },
    progressContainers: {
        alignItems: 'center',
        justifyContent: 'space-around',
        alignItems: 'center',
        flexDirection: 'row',
    },
    hdrTitle: {
        fontSize: 13,
        color: '#2D2D2D',
        textAlign: 'right',
        fontFamily: '$BOLD_FONT',
    },
    activeContainer: {
        width: 10,
        marginLeft: 10,
        height: 10,
        borderRadius: 15,
        backgroundColor: '#6CD7B0'
    },
    statusContainer: {
        alignItems: 'center',
        backgroundColor: '#EDF1FD',
        flexDirection: 'row',
        alignSelf: 'flex-end',
        paddingHorizontal: 7,
        borderRadius: 16,
        paddingVertical: 1,
    },
    counterHdrView: {
        padding: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    primaryTitle: {
        fontSize: 13,
        color: '#505050',
        textAlign: 'right',
        fontFamily: '$REGULAR_FONT'
    },
    counterContainer: {
        width: width - 32,
        backgroundColor: '#fff',
        borderRadius: 10,
        alignSelf: 'center',
        borderWidth: 1,
        borderColor: '#eee',
        marginBottom: 10,
        zIndex: -1,

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,

        elevation: 3,
        shadowRadius: 10,
    },
    dotContainer: {
        backgroundColor: 'transparent',
        width: 8,
        height: 8,
        borderRadius: 4,
        marginLeft: 3,
        marginRight: 3,
        marginTop: 3,
        marginBottom: 3,
        borderColor: '#fff',
        borderWidth: 1,
    },
    activeDotContainer: {
        backgroundColor: '#FF005A',
        width: 8,
        height: 8,
        borderRadius: 4,
        marginLeft: 3,
        marginRight: 3,
        marginTop: 3,
        marginBottom: 3,
        borderColor: '#fff',
        borderWidth: 1,
    },
});