import React, { useState, useContext, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    Dimensions, TouchableHighlight,
    TouchableOpacity,
    Modal, TouchableWithoutFeedback, FlatList
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Swiper from 'react-native-swiper';
import { Button, Header, List, Loading, SafeView } from '../../../Commons';
import { SlideCard } from '../../../Elements';
import ProgressCircle from 'react-native-progress-circle'
import StoreContext from '../../../../Stores';
import { useObserver } from 'mobx-react-lite';
import moment from 'moment-jalaali';
import { Numbers } from '../../../../Utils';
import Icon from 'react-native-vector-icons/Entypo';

const { width, height } = Dimensions.get('window');

const melat = require('../../../../assets/images/gateway/melat.png');
const saman = require('../../../../assets/images/gateway/saman.png');

export default Checkout = ({ navigation, route }) => {
    const { AuthStore, ShoppingStore, ShoppingProfileStore } = useContext(StoreContext);

    const [loading, setLoading] = useState(false);
    const [gatewaySelectedId, setGatewaySelectedId] = useState(1);
    const [ruleConfirmation, setRuleConfirmation] = useState(false);
    const [modalVisible, setModalVisible] = useState(false);

    const { items } = route.params;
    const { gateway } = route.params;

    return useObserver(() => (
        <SafeView>
            <Header
                title={"فاکتور"}
                backOnClick={() => navigation.pop()}
            />

            <ScrollView>
                <View style={styles.mainContainer}>
                    <View style={styles.innerView}>
                        <Text style={styles.hdrTitle}>{Numbers.putCommas(items.invoiceAmount)} ریال</Text>
                        <Text style={styles.desTitle}>مبلغ فاکتور: </Text>
                    </View>

                    <View style={styles.innerView}>
                        <Text style={styles.hdrTitle}>{Numbers.putCommas(items.ranjeAmount)} ریال</Text>
                        <Text style={styles.desTitle}>اعتبار فعلی: </Text>
                    </View>




                    <View style={{ justifyContent: 'center', alignItems: 'center', marginVertical: 16 }}>
                        <Text style={styles.costTitle}>مبلغ قابل پرداخت </Text>
                        <Text style={styles.costTitle}>{Numbers.putCommas(items.totalAmount)} ریال</Text>
                    </View>

                    <Text style={[styles.desTitle, { marginHorizontal: 10 }]}>به مبلغ فوق 9% مالیات بر ارزش افزوده اضافه شده است.</Text>


                    <List
                        data={gateway}
                        onRefresh={() => console.log('refresh')}
                        renderItem={(item) => (
                            <TouchableWithoutFeedback onPress={() => setGatewaySelectedId(item.code)}>
                                <View style={[styles.gatewayContainer, { backgroundColor: item.code === gatewaySelectedId ? '#0F61AB' : '#FFF' }]}>

                                    <View style={{ width: '80%', marginRight: 10, alignItems: 'flex-end' }}>
                                        <Text style={[styles.hdrTitle, { textAlign: 'right', color: item.code === gatewaySelectedId ? '#fff' : '#2D2D2D' }]}>درگاه {item.name}</Text>
                                        <Text style={[styles.desTitle, { textAlign: 'right', color: item.code === gatewaySelectedId ? '#fff' : '#2D2D2D' }]}>پرداخت آنلاین از طریق درگاه به پرداخت{item.name}</Text>
                                    </View>

                                    <Image
                                        source={item.code === 1 ? melat : saman}
                                        style={{ width: 50, height: 50 }}
                                        resizeMode={'contain'}
                                    />
                                </View>
                            </TouchableWithoutFeedback>
                        )}
                    />


                    <View style={styles.warningView}>
                        <Text style={[styles.desTitle, { color: '#8A6D3B', fontSize: 12, width: '85%' }]}>
                            درصورتی که به دلیل مشکلات احتمالی در پرداخت های آنلاین، خرید شما ثبت نگردد مبلغ کسر شده حداکثر تا 72 ساعت بعد به حساب شما عودت داده خواهد شد.
                        </Text>

                        <Icon
                            name={'warning'}
                            color={'#8A6D3B'}
                            size={20}
                            style={{ marginLeft: 10 }}
                        />
                    </View>


                    <View style={[styles.genderInnerContainer, { marginTop: 16 }]}>
                        <TouchableOpacity onPress={() => setRuleConfirmation(!ruleConfirmation)}>
                            <View style={[styles.radioContainer, { backgroundColor: ruleConfirmation ? '#0F61AB' : '#fff' }]} />
                        </TouchableOpacity>


                        <TouchableHighlight
                            onPress={() => setModalVisible(true)}
                            underlayColor={'transparent'}>
                            <Text style={[styles.radioTitles, { marginLeft: 16 }]}>
                                تمام <Text style={{ textDecorationLine: 'underline' }}>قوانین سرویس‌ها</Text> را میپذیرم
                            </Text>
                        </TouchableHighlight>

                    </View>

                    <Button
                        extraStyles={{ width: '90%', opacity: ruleConfirmation ? 1 : 0.3 }}
                        disabled={!ruleConfirmation}
                        onPress={() => console.log('checkout')}
                        title={'پرداخت فاکتور'}
                    />
                </View>
            </ScrollView>


            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => setModalVisible(false)}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <TouchableHighlight
                            style={styles.onclicks}
                            onPress={() => setModalVisible(false)}
                            underlayColor={'transparent'}>
                            <Icon
                                name={'closecircleo'}
                                size={35}
                                color={'#EB0454'}
                                style={{ alignSelf: 'flex-start', margin: 32 }}
                            />
                        </TouchableHighlight>

                        <ScrollView style={{ marginBottom: 32 }}>
                            <Text style={styles.infoTitle}>
                            </Text>
                        </ScrollView>
                    </View>
                </View>
            </Modal>
        </SafeView>
    ));
};


const styles = EStyleSheet.create({
    onclicks: {
        padding: 16,
        margin: -16,
        alignSelf: 'flex-end'
    },
    infoTitle: {
        fontSize: 15,
        color: '#fff',
        textAlign: 'center',
        margin: 16,
        fontFamily: '$REGULAR_FONT'
    },
    centeredView: {
        backgroundColor: 'rgba(0,0,0,0.94)',
        flex: 1,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    modalView: {
        backgroundColor: "transparent",
        width: width - 64,
        height: width - 150,
        borderRadius: 15,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        borderColor: '#EB0454',
        borderWidth: 1,
    },
    radioTitles: {
        color: '#2d2d2d',
        marginLeft: 5,
        textAlign: 'left',
        fontFamily: '$REGULAR_FONT'
    },
    radioContainer: {
        height: 15,
        width: 20,
        borderRadius: 5,
        borderColor: '#FF005A',
        borderWidth: 1,
        marginLeft: 5
    },
    genderTitle: {
        color: '#2d2d2d',
        marginRight: 5,
        fontFamily: '$BOLD_FONT'
    },
    genderInnerContainer: {
        flexDirection: 'row-reverse',
        alignItems: 'center',
        justifyContent: 'center'
    },
    warningView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FCF8E3',
        padding: 10,
        width: '90%',
        alignSelf: 'center',
        borderWidth: 1,
        borderColor: '#8A6D3B',
        borderRadius: 10,
    },
    gatewayContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '90%',
        alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,

        elevation: 1,
        shadowRadius: 10,
        borderWidth: 1,
        borderColor: '#eee',
        marginTop: 10,
        borderRadius: 10,
        padding: 10,
    },
    costTitle: {
        textAlign: 'center',
        fontSize: 20,
        color: '#3C763D',
        fontFamily: '$BOLD_FONT',
    },
    innerView: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 10,
        justifyContent: 'center'
    },
    desTitle: {
        fontSize: 16,
        color: '#2D2D2D',
        textAlign: 'center',
        fontFamily: '$REGULAR_FONT',
    },
    hdrTitle: {
        fontSize: 16,
        color: '#2D2D2D',
        textAlign: 'center',
        fontFamily: '$BOLD_FONT',
        width: '55%'
    },

    mainContainer: {
        width: '95%',
        backgroundColor: '#fff',
        alignSelf: 'center',
        marginVertical: 16,
        paddingVertical: 10,
        borderRadius: 10,


        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,

        elevation: 5,
        shadowRadius: 10,
    }
});