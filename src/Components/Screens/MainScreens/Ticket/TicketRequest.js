import React, { useState, useContext, useEffect, useCallback } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    Dimensions, TouchableHighlight,
    TouchableOpacity,
    Modal, TouchableWithoutFeedback, FlatList,
    ToastAndroid,
    Alert,
    ImageBackground,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Button, CollapseBox, Header, Input, List, Loading, ModalComponent, SafeView } from '../../../Commons';
import StoreContext from '../../../../Stores';
import { useObserver } from 'mobx-react-lite';
import { Logger, Numbers, request, Storage, useLayoutAnimation } from '../../../../Utils';
import Icon from 'react-native-vector-icons/Entypo';
import ImagePicker from 'react-native-image-crop-picker';
import Axios from 'axios';
import DocumentPicker from 'react-native-document-picker';

const { width, height } = Dimensions.get('window');


export default ChangePassword = ({ navigation, route }) => {
    const { AuthStore, TicketStore, CounterStore } = useContext(StoreContext);

    const { subjectList } = route.params;
    const { services } = route.params;

    const [loadingList, setLoadingList] = useState(false);

    const [subjectTitle, setSubjectTitle] = useState('عنوان');
    const [subjectId, setSubjectId] = useState('عنوان');

    const [serviceId, setServiceId] = useState('نام سرویس');


    const [description, setDescription] = useState('');

    const [open, setOpen] = useState(false);
    const [modalVisible, setModalVisible] = useState(false);
    const [errMessage, setErrorMessage] = useState('');

    const [avatarSource, setAvatarSource] = useState('');
    const [videoUri, setVideoUri] = useState('');
    const [videoName, setVideoName] = useState('');
    const [fileSize, setFileSize] = useState(0);
    const [fileType, setFileType] = useState('');


    const [errModal, setErrModal] = useState(0);
    const [progressbar, setProgressbar] = useState(0);

    useEffect(() => {
        setMainServiceId()
    }, []);

    const setMainServiceId = async () => {
        let serviceId = await Storage.GetItem('@serviceId');

        setServiceId(serviceId);
    };

    const openWithAnimation = useCallback(() => {
        useLayoutAnimation();
        setOpen(!open);
    }, [open]);


    const sendTicketRequest = () => {
        setLoadingList(true);
        TicketStore.postTicket(subjectTitle, subjectId, serviceId, description).then(res => {
            if (avatarSource !== '') {
                console.log('ticket id =', res);

                uploadFile(res);
            } else if (res === false) {
                setModalVisible(true);
                setErrorMessage(TicketStore.errMessage);
                setLoadingList(false);
                return;
            } else {
                ToastAndroid.show("درخواست شما با موفقیت ثبت شد !", ToastAndroid.SHORT);
                setAvatarSource('');
                setDescription('');
                setSubjectTitle('عنوان');
                setSubjectId('عنوان');

                navigation.push('tickets');
                setLoadingList(false);
            };
        });
    };

    const subjectOnclick = (subject) => {
        setSubjectTitle(subject.name);
        setSubjectId(subject.id);
        useLayoutAnimation();
        setOpen(false);
    };


    const handleSelectFiles = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.images],
            });
            console.log(
                res.uri,
                res.type, // mime type
                res.name,
                res.size
            );
            const path = res.type.split('/');
            const type = path[path.length - 1];
            setFileType(type);

            const source = { uri: res.uri };
            setAvatarSource(source);
            setVideoName(res.name);
            setVideoUri(res.uri);

            niceBytes(res.size);
            setLoadingList(false);
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }



    };

    const niceBytes = (bytes) => {
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes == 0) return '0 Byte';
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        let convertedSize = (bytes / Math.pow(1024, i)).toFixed(2) + ' ' + sizes[i];

        setFileSize((bytes / Math.pow(1024, i)).toFixed(2));
        console.log('size', convertedSize);
        return convertedSize;
    };


    const uploadFile = async (ticketId) => {
        setLoadingList(true);
        const fileData = {
            uri: videoUri,
            name: videoName,
            type: `video/${videoName.split('.')[1]}`,
        };


        TicketStore.uploadFile(fileData, ticketId).then(res => {
            if (res) {
                ToastAndroid.show("درخواست شما با موفقیت ثبت شد !", ToastAndroid.SHORT);
                setAvatarSource('');
                setDescription('');
                setSubjectTitle('عنوان');
                setSubjectId('عنوان');
                navigation.push('tickets');
                setLoadingList(false);
            } else {
                setAvatarSource('');
                setLoadingList(false);
            };
        });
        // request.post('/v1/User/Ticket/Upload', formData, {
        //     params: {
        //         TicketId: ticketId,
        //     }
        // }).then(data => {
        //     Logger(data, 'uploadFile');
        //     ToastAndroid.show("درخواست شما با موفقیت ثبت شد !", ToastAndroid.SHORT);
        //     setAvatarSource('');
        //     setDescription('');
        //     setSubjectTitle('عنوان');
        //     setSubjectId('عنوان');
        //     navigation.push('tickets');
        //     setLoadingList(false);

        // }).catch(err => {
        //     setAvatarSource('');
        //     setLoadingList(false);
        //     Logger(err, 's err');
        // });
    };



    return useObserver(() => (
        <SafeView>
            <Header
                title={"ایجاد درخواست پشتیبانی"}
                backOnClick={() => navigation.pop()}
            />

            <ScrollView>
                <TouchableWithoutFeedback onPress={() => openWithAnimation()}>
                    <View style={[styles.container, { height: open ? 'auto' : 60 }]}>
                        <View style={styles.header}>
                            <Icon
                                color={'#2d2d2d'}
                                size={25}
                                name={open ? 'chevron-small-up' : 'chevron-small-down'} />
                            <Text style={styles.title}>{subjectTitle}</Text>
                        </View>
                        <View style={styles.contentWrapper}>
                            {subjectList.map((subject, i) => {
                                if (subject.subjectId !== '5bdecafe-8d64-eb11-9c5a-00505681e6ea') {
                                    return (
                                        <AnswerItem
                                            key={i}
<<<<<<< HEAD
                                            flagExtraStyle={{ backgroundColor: subject.id === subjectId ? '#0F61AB' : '#44D69B' }}
=======
                                            flagExtraStyle={{ backgroundColor: subject.subjectId === subjectId ? '#DE0021' : '#44D69B' }}
>>>>>>> f4d81353b7a9c0d70ec16fd7a5857df2f5c9617b
                                            onPress={() => subjectOnclick(subject)}>
                                            {subject.name}
                                        </AnswerItem>
                                    )
                                }
                            })}
                        </View>
                    </View>
                </TouchableWithoutFeedback>

                {services.filter(item => item.customerServiceId === serviceId).map((subject, i) => (
                    <View key={i} style={styles.collapseView}>
                        <Text style={styles.title}>
                            {subject.productServiceInfraName}
                        </Text>

                        <Text style={styles.title}>
                            {subject.phoneNumber}
                        </Text>
                    </View>
                )
                )}

                <Input
                    placeholder={'توضیحاتت رو بنویس...'}
                    value={description}
                    onChangeText={description => setDescription(description)}
                    pExtraStyles={styles.inputStyle}
                    extraInpuStyles={styles.extraInpuStyles}
                    multiline
                />



                <TouchableWithoutFeedback onPress={() => handleSelectFiles()}>
                    <View style={styles.uploadContainer}>
                        <Text style={styles.title}>برای انتخاب فایل کلیک کنید</Text>
                        <Icon
                            color={'#2d2d2d'}
                            size={25}
                            name={'upload'} />
                    </View>
                </TouchableWithoutFeedback>

                {avatarSource !== '' ?
                    <View>
                        {fileType !== 'pdf' ?
                            <ImageBackground
                                source={avatarSource}
                                style={styles.avatarImg}
                            >
                                <Icon
                                    name={'circle-with-cross'}
                                    size={25}
                                    color={'#eee'}
                                    onPress={() => setAvatarSource('')}
                                    style={{ alignSelf: 'flex-end', backgroundColor: 'rgba(0,0,0,0.8)', borderRadius: 100, }}
                                />
                            </ImageBackground>
                            :
                            <View style={styles.pdfView}>
                                <Text style={[styles.title, { width: '85%' }]}>{videoName}</Text>
                                <Icon
                                    color={'#2d2d2d'}
                                    size={25}
                                    name={'text-document'}
                                />
                            </View>
                        }
                    </View>
                    : null}

                <Text style={styles.hdrTitle}>
                    توجه:
                    {'\n'}
                    * تصویر ارسالی باید کاملا خوانا باشد.
                </Text>

                <Button
                    title={'ارسال درخواست'}
                    onPress={() => sendTicketRequest()}
                    disabled={description.length === 0 || subjectId === 'عنوان'}
                    extraStyles={{ width: '95%', opacity: description.length === 0 ? 0.4 : 1 }}
                />
            </ScrollView>

            {loadingList ? <Loading /> : null}


            <ModalComponent
                modalVisible={modalVisible}
                okModal
                title={'خطا!'}
                description={errMessage}
                okText={'باشه'}
                onPress={() => setModalVisible(false)}
                onRequestClose={() => setModalVisible(false)}
            />

        </SafeView>
    ));
};

const AnswerItem = ({ children, onPress, flagExtraStyle }) => {
    return (
        <TouchableWithoutFeedback onPress={onPress}>
            <View style={styles.answerItemBox}>
                <Text style={[styles.textStyle, { width: '90%' }]}>{children}</Text>
                <View style={[styles.answerItemFlag, flagExtraStyle]} />
            </View>
        </TouchableWithoutFeedback>
    );
};
const styles = EStyleSheet.create({
    hdrTitle: {
        fontFamily: '$BOLD_FONT',
        color: '#2d2d2d',
        fontSize: 16,
        textAlign: 'right',
        marginRight: 10,
    },
    pdfView: {
        flexDirection: 'row', alignItems: 'center',
        borderRadius: 10,
        backgroundColor: '#fff',
        padding: 10,
        margin: 10,
        justifyContent: 'flex-end'
    },
    avatarImg: {
        width: 150,
        height: 70,
        borderRadius: 10,
        marginHorizontal: 10,
        marginTop: 10,
        alignSelf: 'flex-end'
    },
    uploadContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        padding: 16,
        backgroundColor: '#fff',
        width: '95%',
        marginTop: 16,
        alignSelf: 'center',
        borderRadius: 10,
    },
    container: {
        width: '95%',
        borderColor: '#eee',
        borderWidth: 1,
        backgroundColor: '#fff',
        borderRadius: 10,
        overflow: 'hidden',
        marginBottom: 15,
        alignSelf: 'center',
        marginTop: 10,
    },
    header: {
        width: '100%',
        height: 'auto',
        minHeight: 40,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 10,
        paddingVertical: 10,
        borderBottomColor: '#252832',
        // borderBottomWidth: 1,
    },
    title: {
        fontFamily: '$REGULAR_FONT',
        color: '#2d2d2d',
        fontSize: 14,
        paddingHorizontal: 8
    },
    contentWrapper: {
        width: '100%',
        height: 'auto',
        minHeight: 50,
        paddingHorizontal: 20,
        paddingVertical: 10,
    },

    collapseView: {
        width: '95%',
        borderColor: '#eee',
        borderWidth: 1,
        backgroundColor: '#fff',
        borderRadius: 10,
        overflow: 'hidden',
        marginBottom: 15,
        height: 60,
        alignSelf: 'center',
        padding: 10,
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    extraInpuStyles: {
        color: '#2d2d2d',
        height: height / 4,
        textAlignVertical: 'top',
    },
    btn: {
        width: '90%',
        borderRadius: 5,
        marginTop: -16,
    },
    inputStyle: {
        height: height / 4,
        alignSelf: 'center',
        marginTop: 10,
        backgroundColor: '#fff',
        color: '#FFF',
        width: '95%',
    },
    ///////////////////////
    textStyle: {
        fontFamily: '$REGULAR_FONT',
        fontSize: 14,
        color: '#2d2d2d',
        textAlign: 'right'
    },
    answerItemBox: {
        width: '100%',
        height: 'auto',
        minHeight: 30,
        marginBottom: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    answerItemFlag: {
        width: 15,
        height: 15,
        borderRadius: 10,
        backgroundColor: '#44D69B'
    }
});