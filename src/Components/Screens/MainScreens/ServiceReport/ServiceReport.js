import React, { useState, useContext, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    Dimensions, TouchableHighlight,
    TouchableOpacity,
    Modal, TouchableWithoutFeedback, FlatList
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Button, Header, List, Loading, SafeView } from '../../../Commons';
import StoreContext from '../../../../Stores';
import { useObserver } from 'mobx-react-lite';

const { width, height } = Dimensions.get('window');

export default ServiceReport = ({ navigation }) => {
    const { AuthStore, CounterStore, ServiceReportStore, ShoppingStore } = useContext(StoreContext);

    const [loading, setLoading] = useState(false);
    const [loadingList, setLoadingList] = useState(false);
    const [services, setServices] = useState([]);

    useEffect(() => {
        requestData();
    }, []);

    const requestData = () => {
        setLoadingList(true);
        CounterStore.fetchData(true).then(res => {
            setServices(res);
            setLoadingList(false);
        });
    };

    const getProductList = (id) => {
        ServiceReportStore.setCustomerServiceId(id);
        navigation.navigate('reports')
    };


    return useObserver(() => (
        <SafeView>
            <Header
                title="گزارش سرویس‌ها"
                noBack
            />

            <List
                data={services}
                onRefresh={() => requestData()}
                showLoading={loadingList}
                renderItem={(item) => {
                    return (
                        <ServiceShoppingCard
                            onPress={() => getProductList(item.customerServiceId)}
                            title={item.productServiceInfraName}
                            phone={item.phoneNumber}
                            statusTitle={item.sitName}
                            statusCode={item.sitValue}
                        />
                    )
                }}
            />

            {loading ? <Loading /> : null}
        </SafeView>
    ));
};

export const ServiceShoppingCard = ({ title, phone, statusTitle, onPress, statusCode }) => {
    return (
        <TouchableHighlight
            onPress={onPress}
            underlayColor={'transparent'}>
            <View style={styles.cardContainer}>
                <View style={{ alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between' }}>
                    <View style={styles.statusContainer}>
                        <Text style={styles.primaryTitle}>{statusTitle}</Text>

                        <View style={[styles.activeContainer, {
                            backgroundColor: statusCode === 13 ? '#FF9800' : statusCode === 12 ? '#6CD7B0' : 'red',
                        }]} />
                    </View>

                    <Text style={styles.hdrTitle}>{title}</Text>
                </View>
                <Text style={styles.primaryTitle}>{phone}</Text>

            </View>
        </TouchableHighlight>
    );
};

const styles = EStyleSheet.create({
    activeContainer: {
        width: 10,
        marginLeft: 10,
        height: 10,
        borderRadius: 15,
        backgroundColor: '#6CD7B0'
    },
    statusContainer: {
        alignItems: 'center',
        backgroundColor: '#EDF1FD',
        flexDirection: 'row',
        alignSelf: 'flex-end',
        paddingHorizontal: 7,
        borderRadius: 16,
        paddingVertical: 1,
    },
    primaryTitle: {
        fontSize: 13,
        color: '#505050',
        textAlign: 'right',
        fontFamily: '$REGULAR_FONT'
    },
    hdrTitle: {
        fontSize: 13,
        color: '#2D2D2D',
        textAlign: 'right',
        fontFamily: '$BOLD_FONT',
    },
    cardContainer: {
        height: height / 8,
        width: width - 16,
        alignSelf: 'center',
        borderWidth: 1,
        borderColor: '#fff',
        marginTop: 10,
        borderRadius: 5,
        backgroundColor: '#fff',
        padding: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,

        elevation: 3,
        shadowRadius: 10,
    },
});