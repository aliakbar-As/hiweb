import React, { useState, useContext, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    Dimensions, TouchableHighlight,
<<<<<<< HEAD
    TouchableOpacity,
=======
    TouchableOpacity, NativeModules,
>>>>>>> f4d81353b7a9c0d70ec16fd7a5857df2f5c9617b
    Modal, TouchableWithoutFeedback, FlatList, Linking, Platform
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Swiper from 'react-native-swiper';
import { Button, Header, List, Loading, ModalComponent, SafeView } from '../../../Commons';
import { ServiceCard, ServiceShoppingCard, SlideCard } from '../../../Elements';
import ProgressCircle from 'react-native-progress-circle';
import StoreContext from '../../../../Stores';
import { useObserver } from 'mobx-react-lite';
import moment from 'moment-jalaali';
import Icon from 'react-native-vector-icons/AntDesign';
import { Storage, getCurrentFirebaseToken, Logger, monitorFirebaseToken } from '../../../../Utils';
import { CommonActions } from '@react-navigation/native';

const { width, height } = Dimensions.get('window');

const SharedStorage = NativeModules.SharedStorage;

export default Counter = ({ navigation }) => {
    const { AuthStore, CounterStore, CounterProfileStore, ShoppingStore, ChallengeStore } = useContext(StoreContext);

    const [loading, setLoading] = useState(false);
    const [loadingList, setLoadingList] = useState(false);
    const [services, setServices] = useState([]);
    const [modalVisible, setModalVisible] = useState(false);

    const [versionModal, setVersionModal] = useState(false);
    const [forceUpdate, setForceUpdate] = useState(false);

    const [serviceId, setServiceId] = useState('');

<<<<<<< HEAD
=======
    const [widgetDate, setWidgetData] = useState(null);
>>>>>>> f4d81353b7a9c0d70ec16fd7a5857df2f5c9617b

    let onFirebaseTokenRefreshListener = monitorFirebaseToken();


    useEffect(() => {
<<<<<<< HEAD
=======

>>>>>>> f4d81353b7a9c0d70ec16fd7a5857df2f5c9617b
        checkUserAppVersion();
        getCurrentFirebaseToken();

        getSliders();
        requestService();

<<<<<<< HEAD
=======

>>>>>>> f4d81353b7a9c0d70ec16fd7a5857df2f5c9617b

        return () => {
            onFirebaseTokenRefreshListener();
        }
    }, []);

    const checkUserAppVersion = () => {
        AuthStore.userModel.checkUserAppVersion().then((isForce) => {
            console.log('here', isForce);

            setVersionModal(true);
            setForceUpdate(isForce);
        });
    };

    
    const getSliders = () => {
        setLoadingList(true);
        CounterStore.getSliders().then(res => {
            setLoadingList(false);
        });
    };

    const requestService = async () => {

        let serviceId = await Storage.GetItem('@serviceId');
<<<<<<< HEAD
        // console.log('id kjnhb', serviceId)

        setLoadingList(true);
        CounterStore.fetchData(true).then(res => {

            if (serviceId === null || serviceId.length === 0) {
                let mainId = res.filter(item => item.sitName === "فعال");

=======

        setLoadingList(true);
        CounterStore.fetchData(true).then(res => {
            if (serviceId === null || serviceId.length === 0) {
                let mainId = res.filter(item => item.sitName === "فعال");


>>>>>>> f4d81353b7a9c0d70ec16fd7a5857df2f5c9617b
                if (mainId.length !== 0) {
                    let currentId = mainId[0].customerServiceId;
                    setServiceId(currentId);
                    Storage.SetItem('@serviceId', currentId);
                } else {
                    let currentId = res.map(item => item.customerServiceId)[0];
<<<<<<< HEAD
=======
                    console.clear();
                    console.log('current', currentId)
                    // ShoppingStore.setCustomerServiceId(currentId.productServiceInfraName);
>>>>>>> f4d81353b7a9c0d70ec16fd7a5857df2f5c9617b
                    setServiceId(currentId);
                    Storage.SetItem('@serviceId', currentId);
                }
            } else {
                setServiceId(serviceId);
            };

            SharedStorage.set(
                JSON.stringify({
                    sit: `وضعیت حساب : ${CounterStore.wSitName}`,
                    username: `نام کاربری : ${CounterStore.wPhoneNumber === null ? CounterStore.wPostalCode : CounterStore.wPostalCode === null ? CounterStore.wPhoneNumber : CounterStore.wPhoneNumber}`,

                    totalDay: `زمان کل : ${CounterStore.wTotalDay}`,

                    remainedDay: `مانده : ${CounterStore.wService_Time_Remained_Title}`,
                    trafficTxt: CounterStore.wTraffic_RemainedTitle,

                    totalTraffic: `حجم خریداری شده : ${CounterStore.wTotalTraffic}`,


                    trafficProgressBar: CounterStore.wTraffic_Remained_Percent,
                    remainedDayProgressBar: CounterStore.wRemainedDayProgressBar,

                })
            );
            setServices(res);
            setLoadingList(false);
        });
    };

    const getProductList = (item) => {
        setLoading(true);
        ShoppingStore.setCustomerServiceId(item.productServiceInfraName);
        ShoppingStore.fetchData(true).then(res => {
            navigation.navigate('extraTraffic');
            setLoading(false);
        });
    };


    const getProductDetails = (id) => {
        setLoadingList(true);
        // ShoppingStore.clearCart();

        setModalVisible(false);

        setServiceId(id);
        Storage.SetItem('@serviceId', id);
        CounterStore.fetchData(true).then(res => {
            setServices(res);

            navigation.dispatch(CommonActions.reset({
                routes: [{ name: 'tabScreens' }],
            }));

            setLoadingList(false);
        });
    };


    return useObserver(() => (
        <SafeView>
            <Header
                moreOnclick={() => setModalVisible(true)}
                userOnclick={() => navigation.navigate('profile')}
                title={'پارس‌آنلاین'}
                main
            />

            <List
                showLoading={loadingList}
                ListHeader={() => (
                    <Swiper
                        autoplay
                        dot={<View style={styles.dotContainer} />}
                        activeDot={<View style={styles.activeDotContainer} />}
                        key={CounterStore.sliders.length}
                        height={height / 3 + 30}
                        // showsPagination={false}
                        autoplayTimeout={4}

                        style={{ marginVertical: 16 }}
                        loop
                    >
                        {CounterStore.sliders.map((item, i) => {
                            return (
                                <SlideCard
                                    key={i}
                                    onPress={() => Linking.openURL(item.link)}
                                    disabled={item.link === "" || item.link === null}
                                    source={{ uri: item.picture }} />
                            )
                        })}
                    </Swiper>
                )}
                data={services}
                onRefresh={() => requestService()}
                renderItem={(item) => {
                    let date = moment(item.trafficInfo.servieExpireDate).format('jYYYY/jMM/jDD');
                    let imsi = item.imsi === null ? '' : item.imsi.substring(4);

                    if (item.customerServiceId === serviceId) {
                        ShoppingStore.setInfraName(item.productServiceInfraName);
                        return (
                            <ServiceCard
                                serviceName={item.productServiceInfraName}
                                statusTitle={item.sitName}
                                statusCode={item.sitValue}
                                expireDate={item.trafficInfo.servieExpireDate !== null ? date : null}
                                stopDate={item.stopDate}
                                remindTrafficPercentage={item.trafficInfo.traffic_Remained_Percent}
                                remindTrafficTitle={item.trafficInfo.traffic_RemainedTitle}
                                trafficTitle={item.trafficInfo.trafficTitle}
                                timeRemindPercentage={item.trafficInfo.service_Time_Remained_Percent}
                                timeRemindTitle={item.trafficInfo.service_Time_Remained_Title}
                                serviceTime={item.trafficInfo.service_Time_Title}
                                onPress={() => getProductList(item)}
                                mainOnPress={() => navigation.navigate('serviceDetails', { items: item })}
                                showTrafficButton={item.sitValue === 12 && item.productServiceInfraName !== 'LTE'}
                                phoneNumber={(item.phoneNumber === '0' || item.phoneNumber === null) ? null : item.phoneNumber}
                                // postalCode={(item.postalCode === '0' || item.phoneNumber === null) ? null : (item.productServiceInfraName === 'LTE' && item.imsi !== null)? imsi : item.postalCode}
                                postalCode={item.postalCode === null ? imsi : item.postalCode}
<<<<<<< HEAD
=======
                                onGetService={() => navigation.navigate('shopping')}
>>>>>>> f4d81353b7a9c0d70ec16fd7a5857df2f5c9617b

                            />
                        );
                    };
                }}
            />

            {loading ? <Loading /> : null}

            <ModalComponent
                modalVisible={versionModal}
                okModal
                title={'بروزرسانی نرم افزار'}
<<<<<<< HEAD
                description={'برای استفاده بهتر از نرم افزار پارس‌آنلاین، نسخه جدید را بروزرسانی کنید.'}
=======
                description={'برای استفاده بهتر از نرم افزار های‌وب، نسخه جدید را بروزرسانی کنید.'}
>>>>>>> f4d81353b7a9c0d70ec16fd7a5857df2f5c9617b
                okText={'بروزرسانی'}
                onPress={() => {
                    let link = Platform.OS === 'android' ? AuthStore.userModel.androidLink : AuthStore.userModel.iosLink;
                    Linking.openURL(link);
                }}
                extraTitle={forceUpdate ? undefined : 'بیخیال'}
                extraButtonOnPress={() => setVersionModal(false)}
            />


            <Modal
                animationType="slide"
                transparent={true}
                onRequestClose={() => setModalVisible(false)}
                visible={modalVisible}
            >
                <View style={styles.modalView}>
                    <View style={styles.modalContainerStyle}>


                        <View style={styles.hdrAccountView}>
                            <Icon
                                name={"close"}
                                size={25}
                                color={'#000'}
                                onPress={() => setModalVisible(false)}
                            />

                            <Text style={[styles.hdrTitle, { fontSize: 16, color: '#0F61AB' }]}>
                                انتخاب سرویس
                            </Text>
                        </View>

                        <List
                            data={services}
                            style={{ width: '100%' }}
                            onRefresh={() => console.log('onRefresh')}
                            showLoading={loadingList}
                            renderItem={(item) => {
                                let imsi = item.imsi === null ? '' : item.imsi.substring(4);

                                return (
                                    <ServiceShoppingCard
                                        extraStyles={{
                                            width: '95%',
                                            backgroundColor: item.customerServiceId === serviceId ? '#eee' : '#fff',
                                            borderWidth: item.customerServiceId === serviceId ? 0 : 1,
                                        }}
                                        onPress={() => getProductDetails(item.customerServiceId)}
                                        title={item.productServiceInfraName}
                                        phone={item.phoneNumber}
                                        statusTitle={item.sitName}
                                        statusCode={item.sitValue}
                                        postalCode={item.postalCode === null ? imsi : item.postalCode}
                                    />
                                )
                            }}
                        />
                    </View>
                </View>
            </Modal>
        </SafeView>
    ));
};



const styles = EStyleSheet.create({
    hdrAccountView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#eee',
        width: '100%',
        paddingHorizontal: 10,
        paddingVertical: 10,
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
    },
    modalView: {
        backgroundColor: 'rgba(0,0,0,0.7)',
        flex: 1,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    modalContainerStyle: {
        backgroundColor: '#fff',
        // padding: 16,
        borderRadius: 10,
        alignItems: 'center',
        width: width - 32,
        height: height - 64,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        borderColor: '#ccc',
        borderWidth: 1,
    },
    pHdrTitle: {
        fontSize: 16,
        color: '#2D2D2D',
        textAlign: 'center',
        fontFamily: '$BOLD_FONT',
    },
    btnStyle: {
        position: 'absolute',
        right: '2.5%',
        left: '2.5%',
        bottom: -40,
        width: '95%',
    },
    progressContainers: {
        alignItems: 'center',
        justifyContent: 'space-around',
        alignItems: 'center',
        flexDirection: 'row',
    },
    hdrTitle: {
        fontSize: 13,
        color: '#2D2D2D',
        textAlign: 'right',
        fontFamily: '$BOLD_FONT',
    },

    counterHdrView: {
        padding: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    primaryTitle: {
        fontSize: 13,
        color: '#505050',
        textAlign: 'right',
        fontFamily: '$REGULAR_FONT'
    },
    counterContainer: {
        width: width - 32,
        height: height / 2 - 35,
        backgroundColor: '#fff',
        borderRadius: 10,
        alignSelf: 'center',
        borderWidth: 1,
        borderColor: '#eee',
        marginBottom: 32,

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,

        elevation: 3,
        shadowRadius: 10,
    },
    dotContainer: {
        backgroundColor: 'transparent',
        width: 8,
        height: 8,
        borderRadius: 4,
        marginLeft: 3,
        marginRight: 3,
        marginTop: 3,
        marginBottom: 3,
        borderColor: '#fff',
        borderWidth: 1,
    },
    activeDotContainer: {
        backgroundColor: '#FF005A',
        width: 8,
        height: 8,
        borderRadius: 4,
        marginLeft: 3,
        marginRight: 3,
        marginTop: 3,
        marginBottom: 3,
        borderColor: '#fff',
        borderWidth: 1,
    },
});