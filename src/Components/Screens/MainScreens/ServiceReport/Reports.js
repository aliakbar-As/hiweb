import React, { useState, useContext, useEffect, useCallback } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    Dimensions, TouchableHighlight,
    TouchableOpacity,
    Modal, TouchableWithoutFeedback, FlatList, Alert
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Button, Header, List, Loading, SafeView } from '../../../Commons';
import StoreContext from '../../../../Stores';
import { useObserver } from 'mobx-react-lite';
import moment from 'moment-jalaali';
import Icon from 'react-native-vector-icons/AntDesign';
import PersianCalendarPicker from 'react-native-persian-calendar-picker';

const { width, height } = Dimensions.get('window');
let hdrItems = [
    {
        id: 0,
        title: 'نام سرویس',
        filedName: 'serviceInfo'
    },
    {
        id: 1,
        title: 'تعداد کانکشن',
        filedName: 'connectionTime'
    },
    {
        id: 2,
        title: 'مدت زمان اتصال',
        filedName: 'connectionTimeString'
    },
    {
        id: 3,
        title: 'دانلود MB',
        filedName: 'downloadString'
    },
    {
        id: 4,
        title: 'آپلود MB',
        filedName: 'uploadString'
    },
    {
        id: 5,
        title: 'جمع MB',
        filedName: 'totalUploadDownloadString'
    },
    {
        id: 6,
        title: 'دانلود MB اعمال ضریب',
        filedName: 'downloadWithCoefficientString'
    },
    {
        id: 7,
        title: 'آپلود MB اعمال ضریب',
        filedName: 'uploadWithCoefficientString'
    },
    {
        id: 8,
        title: 'جمع MB',
        filedName: 'totalUploadDownloadWithCoefficientString'
    },
]

export default Reports = ({ navigation }) => {
    const { AuthStore, ServiceReportStore } = useContext(StoreContext);

    const [loadingList, setLoadingList] = useState(false);
    const [startDate, setStartDate] = useState('');
    const [endDate, setEndDate] = useState('');

    const [startDateModal, setStartDateModal] = useState(false);
    const [endDateModal, setEndDateModal] = useState(false);


    const [dateStatus, setDateStatus] = useState(0);

    const [sessionSummary, setSessionSummary] = useState({});
    const [showSessionSummary, setShowSessionSummary] = useState(false);

    useEffect(() => {
        // setShowSessionSummary(false);

        const date = new Date();
        setStartDate(date);
        ServiceReportStore.setStartDate(date);
        setEndDate(date);
        ServiceReportStore.setEndDate(date);
    }, []);

    const onStartDateChange = useCallback(
        (date) => {
            let pickDate = date.toString();
            let convertDate = moment(pickDate).format('YYYY-MM-DD');
            setStartDate(convertDate);
            ServiceReportStore.setStartDate(pickDate);
            setStartDateModal(false);
            setEndDateModal(false);
        },
        [],
    );

    const onEndDateChange = useCallback(
        (date) => {
            let pickDate = date.toString();
            let convertDate = moment(pickDate).format('YYYY-MM-DD');
            setEndDate(convertDate);
            ServiceReportStore.setEndDate(pickDate);
            setStartDateModal(false);
            setEndDateModal(false);
        },
        [],
    );

    const getReports = () => {
        setShowSessionSummary(false);

        setLoadingList(true);
        ServiceReportStore.fetchData(true).then(data => {
            console.log('session data', data)
            if (data === false) {
                setLoadingList(false);
                return;
            };
            setSessionSummary(data);
            setShowSessionSummary(true);
            setLoadingList(false);
        });
    };

    return useObserver(() => (
        <SafeView>
            <Header
                title="گزارش‌ها"
                backOnClick={() => navigation.pop()}

            />


            <List
                ListHeader={() => (
                    <View>
                        <View style={styles.hdrView}>
                            <TouchableHighlight
                                disabled={loadingList}
                                onPress={() => {
                                    setDateStatus(1);
                                    setEndDateModal(true);
                                }}
                                underlayColor={'transparent'}>
                                <View style={{ alignItems: 'flex-end' }}>
                                    <Text style={styles.hdrTitles}>تاریخ پایان</Text>

                                    <View style={styles.innerView}>
                                        <Icon
                                            name={'calendar'}
                                            size={25}
                                            color={'#2d2d2d'}
                                        />
                                        <Text style={styles.desTitle}>{moment(endDate).format('jYYYY/jM/jD')}</Text>
                                    </View>
                                </View>
                            </TouchableHighlight>


                            <TouchableHighlight
                                disabled={loadingList}
                                onPress={() => {
                                    setStartDateModal(true);
                                    setDateStatus(0);
                                }}
                                underlayColor={'transparent'}>
                                <View style={{ alignItems: 'flex-end' }}>
                                    <Text style={styles.hdrTitles}>تاریخ شروع</Text>

                                    <View style={styles.innerView}>
                                        <Icon
                                            name={'calendar'}
                                            size={25}
                                            color={'#2d2d2d'}
                                        />
                                        <Text style={styles.desTitle}>{moment(startDate).format('jYYYY/jM/jD')}</Text>
                                    </View>
                                </View>
                            </TouchableHighlight>





                        </View>
                        {/* 
                        <List
                            contentContainerStyle={{ alignItems: 'flex-end', justifyContent: 'space-around' }}
                            showsHorizontalScrollIndicator={false}
                            style={styles.cardViews}
                            disableRefresh
                            horizontal
                            ListHeader={() => (
                                <View style={styles.hdrContainer}>
                                    {hdrItems.slice(0).map(item => (
                                        <Text key={item.id} style={styles.hdrHeaderTitle}>
                                            {item.title}
                                        </Text>
                                    ))}
                                </View>
                            )}
                            data={sessionSummary.items}
                            renderItem={(item) => {
                                return (
                                    <View style={{ paddingHorizontal: 10, flexDirection: 'row', alignItems: 'center', }}>
                                        <Text style={[styles.desTitle, { width: 100 }]}>
                                            {item.serviceInfo}
                                        </Text>

                                        <Text style={[styles.desTitle, { width: 100 }]}>
                                            {item.connectionTime}
                                        </Text>

                                        <Text style={[styles.desTitle, { width: 100 }]}>
                                            {item.connectionTimeString}
                                        </Text>

                                        <Text style={[styles.desTitle, { width: 100 }]}>
                                            {item.downloadString}
                                        </Text>

                                        <Text style={[styles.desTitle, { width: 100 }]}>
                                            {item.uploadString}
                                        </Text>

                                        <Text style={[styles.desTitle, { width: 100 }]}>
                                            {item.totalUploadDownloadString}
                                        </Text>

                                        <Text style={[styles.desTitle, { width: 100 }]}>
                                            {item.downloadWithCoefficientString}
                                        </Text>
                                        <Text style={[styles.desTitle, { width: 100 }]}>
                                            {item.uploadWithCoefficientString}
                                        </Text>

                                        <Text style={[styles.desTitle, { width: 100 }]}>
                                            {item.totalUploadDownloadWithCoefficientString}
                                        </Text>
                                    </View>
                                )
                            }}
                        /> */}

                        {showSessionSummary ?
                            <ScrollView
<<<<<<< HEAD
                                showsHorizontalScrollIndicator={false}
=======
                                showsHorizontalScrollIndicator
>>>>>>> f4d81353b7a9c0d70ec16fd7a5857df2f5c9617b
                                horizontal>
                                <View>
                                    <View style={{ backgroundColor: '#fff', borderBottomWidth: 1, borderColor: '#eee', }}>
                                        <View style={styles.hdrContainer}>
                                            {hdrItems.map(item => (
                                                <Text key={item.id} style={styles.hdrHeaderTitle}>
                                                    {item.title}
                                                </Text>
                                            ))}
                                        </View>


                                        {sessionSummary.items.map(item => {
                                            return (
                                                <View key={item.id} style={styles.cartContainer}>
                                                    <Text style={styles.desTitles}>
                                                        {item.serviceInfo}
                                                    </Text>

                                                    <Text style={styles.desTitles}>
                                                        {item.connections}
                                                    </Text>

                                                    <Text style={styles.desTitles}>
                                                        {item.connectionTimeString}
                                                    </Text>

                                                    <Text style={styles.desTitles}>
                                                        {item.downloadString}
                                                    </Text>

                                                    <Text style={styles.desTitles}>
                                                        {item.uploadString}
                                                    </Text>

                                                    <Text style={styles.desTitles}>
                                                        {item.totalUploadDownloadString}
                                                    </Text>

                                                    <Text style={styles.desTitles}>
                                                        {item.downloadWithCoefficientString}
                                                    </Text>
                                                    <Text style={styles.desTitles}>
                                                        {item.uploadWithCoefficientString}
                                                    </Text>

                                                    <Text style={styles.desTitles}>
                                                        {item.totalUploadDownloadWithCoefficientString}
                                                    </Text>
                                                </View>

                                            )
                                        })}
                                    </View>

                                    <View style={[styles.hdrContainer, styles.extraHdrView]}>
                                        <View style={{ width: 100 }}>
                                            <Text style={styles.desTitles}>جمع</Text>
                                        </View>

                                        <View style={{ width: 100 }}>
                                            <Text style={styles.desTitles}>   </Text>
                                        </View>

                                        <View style={{ width: 100 }}>
                                            <Text style={styles.desTitles}>   </Text>
                                        </View>


                                        <View style={{ width: 100 }}>
                                            <Text style={styles.desTitles}>{sessionSummary.totalDownloadString}</Text>
                                        </View>

                                        <View style={{ width: 100 }}>
                                            <Text style={styles.desTitles}>{sessionSummary.totalUploadString}</Text>
                                        </View>

                                        <View style={{ width: 100 }}>
                                            <Text style={styles.desTitles}>{sessionSummary.totalDownloadAndUploadString}</Text>
                                        </View>


                                        <View style={{ width: 100 }}>
                                            <Text style={styles.desTitles}>{sessionSummary.totalCoefficientDownloadString}</Text>
                                        </View>

                                        <View style={{ width: 100 }}>
                                            <Text style={styles.desTitles}>{sessionSummary.totalCoefficientUploadString}</Text>
                                        </View>

                                        <View style={{ width: 100 }}>
                                            <Text style={styles.desTitles}>{sessionSummary.totalCoefficientDownloadAndUploadString}</Text>
                                        </View>
                                    </View>
                                </View>
                            </ScrollView>
                            : null}


                    </View>
                )}
                onRefresh={() => getReports()}
                showLoading={loadingList}
                store={ServiceReportStore}
                ListEmptyComponent={() => {
                    if (!loadingList) {
                        return (
                            <Text style={styles.emptyDataTitle}>
                                موردی یافت نشد!
                            </Text>
                        )
                    } else return null;
                }}
                renderItem={(item, index) => {
                    return (
                        <TouchableHighlight
                            onPress={() => navigation.navigate('reportDetails', { data: item })}
                            underlayColor={'transparent'}>
                            <View style={styles.cardView}>

                                <View style={styles.indexContainer}>
                                    <Text style={styles.cardNubmerTitle}>{index + 1}</Text>
                                </View>

                                <View style={styles.innerCardView}>
                                    <Text style={styles.hdrTitle}>
                                        {item.connectionTimeString}
                                        {'\n'}
                                        {item.startDateTimeString}
                                        {'\n'}
                                        {item.lastUpdateDateTimeString}
                                    </Text>

                                    <Text style={styles.primaryTitle}>
                                        مدت اتصال :
                                        {'\n'}
                                        شروع :
                                        {'\n'}
                                        پایان :
                                    </Text>
                                </View>


                                <View style={styles.innerCardView}>
                                    <Text style={styles.hdrTitle}>
                                        {item.totalCoefficientDownloadString} MB
                                        {'\n'}
                                        {item.totalCoefficientUploadString} MB
                                    </Text>

                                    <Text style={styles.primaryTitle}>
                                        دانلود اعمال ضریب
                                        {'\n'}
                                        آپلود اعمال ضریب
                                    </Text>
                                </View>
                            </View>
                        </TouchableHighlight>
                    )
                }}
            />


            < Button
                title={'مشاهده عملکرد'}
                showLoading={loadingList}
                disabled={loadingList || startDate === endDate}
                onPress={() => getReports()}
                extraStyles={{ width: '95%', opacity: loadingList || startDate === endDate ? 0.4 : 1 }}
            />
            < Modal
                animationType="slide"
                transparent={true}
                visible={startDateModal || endDateModal}
                onRequestClose={() => {
                    setStartDateModal(false);
                    setEndDateModal(false);
                }}
            >
                <TouchableWithoutFeedback onPress={() => {
                    setStartDateModal(false);
                    setEndDateModal(false);
                }}>
                    <View style={styles.modalContainer}>
                        <TouchableWithoutFeedback onPress={() => console.log('attach')}>
                            <View style={styles.innerModalView}>
                                <Text style={styles.hdrTitle}>{dateStatus === 0 ? 'تاریخ شروع ' : 'تاریخ پایان'}</Text>

                                <PersianCalendarPicker
                                    onDateChange={startDateModal ? onStartDateChange : onEndDateChange}
                                />
                            </View>
                        </TouchableWithoutFeedback>

                    </View>
                </TouchableWithoutFeedback>
            </Modal >
        </SafeView >
    ));
};


const styles = EStyleSheet.create({
    cartContainer: {
        paddingHorizontal: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    extraHdrView: {
        backgroundColor: '#fff',
        borderBottomWidth: 1,
        borderColor: '#ccc',
        elevation: 13,
        paddingHorizontal: 10,
        paddingVertical: 5,
    },
    footerViewStyle: {
        borderTopWidth: 1,
        flexDirection: 'row',
        borderColor: '#2d2d2d',
        alignItems: 'center',
        justifyContent: 'space-around',
        paddingHorizontal: 10,
        backgroundColor: '#fff',
        marginTop: -16,
        width: width - 16,
        alignSelf: 'center',
        paddingTop: 5,
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
        marginBottom: 16,
        paddingBottom: 10,
    },
    desTitles: {
        fontSize: 12,
        color: '#2D2D2D',
        textAlign: 'center',
        fontFamily: '$REGULAR_FONT',
        width: 100
    },
    cardViews: {
        alignSelf: 'center',
    },
    hdrContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 10,
<<<<<<< HEAD
        backgroundColor: '#0F61AB'
=======
        backgroundColor: '#DE0021'
>>>>>>> f4d81353b7a9c0d70ec16fd7a5857df2f5c9617b
    },
    hdrHeaderTitle: {
        width: 100,
        color: '#fff',
        textAlign: 'center',
        fontFamily: '$BOLD_FONT',
        fontSize: 13
    },
    emptyDataTitle: {
        color: '#2d2d2d',
        textAlign: 'center',
        fontSize: 16,
        fontFamily: '$BOLD_FONT',
        marginTop: '50%',
        marginHorizontal: 32,
    },
    indexContainer: {
        borderRightWidth: 1,
        borderBottomWidth: 1,
        padding: 5,
        // borderRadius: 10,
        borderBottomRightRadius: 10,
        position: 'absolute',
        left: 0,
        top: 0,
        backgroundColor: '#0F61AB',
        borderColor: '#0F61AB'
    },
    cardNubmerTitle: {
        fontSize: 18,
        color: '#fff',
        textAlign: 'center',
        fontFamily: '$REGULAR_FONT'

    },
    titles: {
        fontSize: 13,
        color: '#505050',
        textAlign: 'right',
        fontFamily: '$REGULAR_FONT'
    },
    innerCardView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    hdrTitle: {
        fontSize: 16,
        color: '#505050',
        textAlign: 'right',
        fontFamily: '$BOLD_FONT',
        width: '70%'
    },
    cardView: {
        backgroundColor: '#fff',
        marginHorizontal: 16,
        marginTop: 10,
        borderRadius: 10,
        padding: 10,
        alignItems: 'flex-end',
        justifyContent: 'space-around',
        width: '95%',
        alignSelf: 'center',
        borderWidth: 1,
        borderColor: '#eee',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,

        elevation: 5,
        shadowRadius: 10,
    },
    primaryTitle: {
        fontSize: 15,
        color: '#505050',
        textAlign: 'right',
        fontFamily: '$REGULAR_FONT',
        width: '35%',
    },
    emptyDataTitle: {
        color: '#2d2d2d',
        textAlign: 'center',
        fontSize: 16,
        fontFamily: '$BOLD_FONT',
        marginVertical: '30%',
        marginHorizontal: 32,
    },

    /////////////////////////////////////////////////////////////////
    innerModalView: {
        width: '100%',
        height: '70%',
        backgroundColor: '#fff',
        borderRadius: 3,
        elevation: 1,
        padding: 10,
        marginBottom: '4%',
    },
    modalContainer: {
        width: width,
        height: height,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
    },
    // /////////////////////////////
    desTitle: {
        fontSize: 16,
        color: '#2D2D2D',
        textAlign: 'center',
        fontFamily: '$REGULAR_FONT',
    },
    hdrTitles: {
        fontSize: 16,
        color: '#2D2D2D',
        textAlign: 'center',
        fontFamily: '$BOLD_FONT',
    },
    innerView: {
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'gray',
        width: width / 2 - 20,
        justifyContent: 'center',
        padding: 5,
        paddingHorizontal: 16,
        justifyContent: 'space-between',
        marginTop: 5,
    },
    hdrView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 10,
    },
});