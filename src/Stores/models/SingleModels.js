import { types, applySnapshot, getSnapshot } from 'mobx-state-tree';
import moment from 'moment-jalaali';


export const SingleSlider = types.model({
    picture: types.maybeNull(types.string),
    link: types.maybeNull(types.string),
    description: types.maybeNull(types.string),
    id: types.maybeNull(types.string),
    saveDate: types.maybeNull(types.string),
});

export const SingleService = types.model({
    customerServiceId: types.maybeNull(types.string),
    servicePurchaseDate: types.maybeNull(types.string),
    traffic: types.maybeNull(types.number),
    traffic_Remained: types.maybeNull(types.number),
    traffic_Remained_Percent: types.maybeNull(types.number),
    traffic_Remained_Internal: types.maybeNull(types.number),
    trafficTitle: types.maybeNull(types.string),
    traffic_RemainedTitle: types.maybeNull(types.string),
    traffic_Remained_InternalTitle: types.maybeNull(types.string),
    service_Time_Remained_Percent: types.maybeNull(types.number),
    service_Time_Title: types.maybeNull(types.string),
    service_Time_Remained_Title: types.maybeNull(types.string),
    servieActivationDate: types.maybeNull(types.string),
    servieExpireDate: types.maybeNull(types.string),
    serviceNextStepTraffic: types.maybeNull(types.number),
    serviceNextStepTrafficTitle: types.maybeNull(types.string),
    serviceNextStepDate: types.maybeNull(types.string),
    extraTraffic: types.maybeNull(types.number),
    extraTraffic_Remained: types.maybeNull(types.number),
    extraTraffic_Remained_Percent: types.maybeNull(types.number),
    extraTraffic_Remained_Time: types.maybeNull(types.number),
});

export const SingleBundle = types.model({
    activationDate: types.maybeNull(types.string),
    activationDateStr: types.maybeNull(types.string),
    adslPhoneNo: types.maybeNull(types.string),
    allActiveRecord: types.maybeNull(types.number),
    allExpireRecord: types.maybeNull(types.number),
    allReadyToActiveRecord: types.maybeNull(types.number),
    allowActive: types.maybeNull(types.boolean),
    allowDeactivateService: types.maybeNull(types.boolean),
    allowDeactive: types.maybeNull(types.boolean),
    boostUplink: types.maybeNull(types.number),
    canTransfar: types.maybeNull(types.boolean),
    customerService: types.maybeNull(types.string),
    customerServiceId: types.maybeNull(types.string),
    customerServiceProductId: types.maybeNull(types.string),
    expirationDate: types.maybeNull(types.string),
    expirationDateStr: types.maybeNull(types.string),
    infraTypeId: types.maybeNull(types.number),
    isFUP: types.maybeNull(types.boolean),
    isGift: types.maybeNull(types.boolean),
    isServiceBase: types.maybeNull(types.boolean, false),
    isTransfered: types.maybeNull(types.boolean),
    productName: types.maybeNull(types.string),
    productType: types.maybeNull(types.number),
    purchaseDate: types.maybeNull(types.string),
    purchaseDateStr: types.maybeNull(types.string),
    stopDate: types.maybeNull(types.string),
    remainTraffic: types.maybeNull(types.number),
    rowNO: types.maybeNull(types.number),
    showAmount: types.maybeNull(types.number),
    status: types.maybeNull(types.number),
    statusId: types.maybeNull(types.number),
    totalRecordCount: types.maybeNull(types.number),

});

export const SingleInfo = types.model({
    customerServiceId: types.optional(types.string, ''),
    productServiceInfraName: types.optional(types.string, ''),
    telecomCenter: types.optional(types.string, ''),
    sitName: types.optional(types.string, ''),
    trafficInfo: SingleService,
});


const SingleProduct = types.model({
    productId: types.maybeNull(types.string),
    productPriority: types.maybeNull(types.number),
    productName: types.maybeNull(types.string),
    promotionProductId: types.maybeNull(types.string),
    promotionConfigId: types.maybeNull(types.string),
    promotionConfigName: types.maybeNull(types.string),
    category: types.maybeNull(types.string),
    amount: types.maybeNull(types.number),
    categoryPriority: types.maybeNull(types.number),
    categoryId: types.maybeNull(types.string),
    traffic: types.maybeNull(types.number),
    trafficWithUnit: types.maybeNull(types.string),
    trafficInternal: types.maybeNull(types.number),
    trafficInternalWithUnit: types.maybeNull(types.string),
    duration: types.maybeNull(types.number),
    productTypeCode: types.maybeNull(types.number),
    bundleTypeCode: types.maybeNull(types.number),
    purchaseQuotaPerUser: types.maybeNull(types.number),
    productType: types.maybeNull(types.string),
    imageUrl: types.maybeNull(types.string),
    summary: types.maybeNull(types.string),
    durationtype: types.maybeNull(types.number),
    bundleType: types.maybeNull(types.string),
    time: types.maybeNull(types.number),
    speed: types.maybeNull(types.number),
});


export const SingleProductList = types.model({
    id: types.optional(types.string, ''),
    name: types.optional(types.string, ''),
    poducts: types.optional(types.array(SingleProduct), []),


    productId: types.maybeNull(types.string),
    productName: types.maybeNull(types.string),

    promotionConfigId: types.maybeNull(types.string),
    promotionConfigName: types.maybeNull(types.string),
    promotionProductId: types.maybeNull(types.string),
    promotionProductName: types.maybeNull(types.string),
    description: types.maybeNull(types.string),
    canTransfer: types.maybeNull(types.string),
    endDate: types.maybeNull(types.string),
    bundleContent: types.maybeNull(types.string),
    productTypeCode: types.maybeNull(types.number),
    traffic: types.maybeNull(types.number),




    //   "productNumber": null,
    //   "productUnit": null,
    //   "description": null,
    //   "imageUrl": null,
    //   "summary": null,
});

const SingleCartItem = types.model({
    productId: types.maybeNull(types.string),
    isService: types.maybeNull(types.boolean),
    productName: types.maybeNull(types.string),
    bundleType: types.maybeNull(types.number),
    productTypeCode: types.maybeNull(types.number),
    isInvalidItem: types.maybeNull(types.boolean),
    readOnly: types.maybeNull(types.boolean),
    pricePerUnit: types.maybeNull(types.number),
    quantity: types.maybeNull(types.number),
    extendedAmount: types.maybeNull(types.number),
    promotionConfigId: types.maybeNull(types.string),
    promotionConfigName: types.maybeNull(types.string),
    promotionProductId: types.maybeNull(types.string),
    promotionProductName: types.maybeNull(types.string),
    infraTypeId: types.maybeNull(types.number),
    phoneNumber: types.maybeNull(types.string),
    customerServiceId: types.maybeNull(types.string),
    qouteDetailId: types.maybeNull(types.string),
    duplicate: types.maybeNull(types.boolean),
    orderMin: types.maybeNull(types.number),
    orderMax: types.maybeNull(types.number),
    count: types.optional(types.number, 0),
});

export const SingleShoppingCart = types.model({
    id: types.maybeNull(types.string),
    promotionProductId: types.maybeNull(types.string),
    title: types.maybeNull(types.string),
    customerServiceId: types.maybeNull(types.string),
    isService: types.maybeNull(types.boolean),
    infraTypeId: types.maybeNull(types.number),
    phoneNumber: types.maybeNull(types.string),
    postalCode: types.maybeNull(types.string),
    totalAmount: types.maybeNull(types.number),
    createOn: types.maybeNull(types.string),
    isInvalidItem: types.maybeNull(types.boolean),
    items: types.optional(types.array(SingleCartItem), []),
});


const Annotations = types.model({
    receivedGift: types.maybeNull(types.boolean),
    documentType: types.maybeNull(types.number),
    entityStatus: types.maybeNull(types.number),
    height: types.maybeNull(types.number),
    size: types.maybeNull(types.number),
    width: types.maybeNull(types.number),
    fileName: types.maybeNull(types.string),
    id: types.maybeNull(types.string),
    mimeType: types.maybeNull(types.string),
    filePath: types.maybeNull(types.string),
});

export const SingleTicket = types.model({
    annotations: types.optional(types.array(Annotations), []),
    createdOn: types.maybeNull(types.string),
    customerId: types.maybeNull(types.string),
    customerServiceId: types.maybeNull(types.string),
    customerServiceName: types.maybeNull(types.string),
    description: types.maybeNull(types.string),
    entityStatus: types.maybeNull(types.number),
    fcpProvider: types.maybeNull(types.number),
    id: types.maybeNull(types.string),
    subjectId: types.maybeNull(types.string),
    title: types.maybeNull(types.string),
    subjectName: types.maybeNull(types.string),
});




const SubSessions = types.model({
    serviceInfo: types.maybeNull(types.string),
    downloadMBString: types.maybeNull(types.string),
    uploadMBString: types.maybeNull(types.string),
    downloadWithCoefficientMBString: types.maybeNull(types.string),
    uploadWithCoefficientMBString: types.maybeNull(types.string),
    coefficientDownload: types.maybeNull(types.number),
    coefficientUpload: types.maybeNull(types.number),
});

export const SingleReport = types.model({
    connectionTime: types.maybeNull(types.number),
    subSessions: types.optional(types.array(SubSessions), []),
    connectionTimeString: types.maybeNull(types.string),
    disconnect: types.maybeNull(types.string),
    downloadMB: types.maybeNull(types.number),
    download: types.maybeNull(types.number),
    downloadMBString: types.maybeNull(types.string),
    startDateTimeString: types.maybeNull(types.string),
    start: types.maybeNull(types.string),
    lastUpdateDateTimeString: types.maybeNull(types.string),
    lastUpdate: types.maybeNull(types.string),
    ipAddress: types.maybeNull(types.string),
    downloadString: types.maybeNull(types.string),
    callerId: types.maybeNull(types.string),
    totalDownload: types.maybeNull(types.number),
    totalDownloadAndUpload: types.maybeNull(types.number),
    totalDownloadAndUploadString: types.maybeNull(types.string),
    totalDownloadString: types.maybeNull(types.string),
    totalUpload: types.maybeNull(types.number),
    totalUploadDownload: types.maybeNull(types.number),
    totalUploadDownloadString: types.maybeNull(types.string),
    totalUploadString: types.maybeNull(types.string),
    uploadMBString: types.maybeNull(types.string),
    uploadString: types.maybeNull(types.string),
    totalCoefficientDownloadString: types.maybeNull(types.string),
    totalCoefficientUploadString: types.maybeNull(types.string),
    uploadMB: types.maybeNull(types.number),
    upload: types.maybeNull(types.number),
});

export const SingleSession = types.model({
    uploadString: types.maybeNull(types.string),
    uploadWithCoefficient: types.maybeNull(types.number),
    uploadWithCoefficientString: types.maybeNull(types.string),
    totalUploadDownloadWithCoefficient: types.maybeNull(types.number),
    totalUploadDownloadWithCoefficientString: types.maybeNull(types.string),
    upload: types.maybeNull(types.number),
    uploadByte: types.maybeNull(types.number),
    downloadWithCoefficientString: types.maybeNull(types.string),
    serviceInfo: types.maybeNull(types.string),
    totalUploadDownload: types.maybeNull(types.number),
    totalUploadDownloadString: types.maybeNull(types.string),
    download: types.maybeNull(types.number),
    downloadByte: types.maybeNull(types.number),
    downloadString: types.maybeNull(types.string),
    downloadWithCoefficient: types.maybeNull(types.number),
    coefficientDownload: types.maybeNull(types.number),
    coefficientUpload: types.maybeNull(types.number),
    connectionTime: types.maybeNull(types.number),
    connectionTimeString: types.maybeNull(types.string),
    connections: types.maybeNull(types.number),
});


export const SingleChallenge = types.model({
    contributors: types.maybeNull(types.string),
    description: types.maybeNull(types.string),
    entrance: types.maybeNull(types.number),
    fromDate: types.maybeNull(types.string),
    id: types.maybeNull(types.string),
    isActive: types.maybeNull(types.boolean),
    picture: types.maybeNull(types.string),
    reward: types.maybeNull(types.string),
    saveDate: types.maybeNull(types.string),
    softDelete: types.maybeNull(types.boolean),
    templates: types.maybeNull(types.string),
    title: types.maybeNull(types.string),
    toDate: types.maybeNull(types.string),
    image_File: types.maybeNull(types.string),
    videoFile: types.maybeNull(types.string),
}).views(self => {
    return {
        get date() {
            return self.saveDate ?
                moment(self.saveDate, 'YYYY-M-DTHH:mm:ss').format('jYYYY/jM/jD') : '';
        },
        get remainDays() {
            const challengeDate = new Date(self.toDate.slice(0, 10));
            const currentDate = new Date();
            const diffTime = challengeDate - currentDate;
            return Math.ceil(diffTime / (1000 * 60 * 60 * 24));
        },
    };
});