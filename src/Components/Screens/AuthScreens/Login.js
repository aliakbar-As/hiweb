import React, { useState, useContext, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView, Modal,
    TouchableHighlight,
    Dimensions,
    Keyboard,
    Alert,
    ImageBackground,
    Linking,
    StatusBar, TouchableOpacity,
    TouchableWithoutFeedback,
    Platform
} from 'react-native';
import { Input, Button, SafeView, ModalComponent, Loading } from '../../Commons';
import Icon from 'react-native-vector-icons/FontAwesome';
import StoreContext from '../../../Stores';
import EStyleSheet from 'react-native-extended-stylesheet';
import { CommonActions } from '@react-navigation/native';

const { width, height } = Dimensions.get('window');

const hiwebLogo = require('../../../assets/images/parsLogo.png');
const bgImage = require('../../../assets/images/alex.jpg');

export default Login = ({ navigation }) => {

    const { AuthStore, ShoppingStore, ChallengeStore } = useContext(StoreContext);

    const [loading, setLoading] = useState(false);
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [errModal, setErrModal] = useState(0);
    const [errMessage, setErrMessage] = useState('');

    const [versionModal, setVersionModal] = useState(false);
    const [forceUpdate, setForceUpdate] = useState(false);

    useEffect(() => {
        checkUserAppVersion();
    }, []);

    const checkUserAppVersion = () => {
        AuthStore.userModel.checkUserAppVersion().then((isForce) => {
            console.log('here', isForce);

            setVersionModal(true);
            setForceUpdate(isForce);
        });
    };

    const submitUserInfo = () => {
        Keyboard.dismiss();
        if (username === '' || password === '') {
            setErrModal(true);
            setErrMessage('لطفا نام کاربری و رمز عبور خود را وارد کنید!');

            return;
        };
        setLoading(true);
        ShoppingStore.setBadge(0);
        AuthStore.loginUser(username, password).then(res => {
            if (res) {
                ChallengeStore.fetchData(true).then(() => {
                    navigation.dispatch(CommonActions.reset({
                        routes: [{ name: 'tabScreens' }],
                    }));
                    setLoading(false);
                });
            } else {
                setErrModal(true);
                setErrMessage(AuthStore.errMessage);

                setLoading(false);

            }
        });
    };




    return (
        <SafeView>
            <StatusBar hidden />
            <ScrollView contentContainerStyle={{ flexGrow: 1 }}
                keyboardShouldPersistTaps='handled'>

                <View style={styles.container}>

                    <Image
                        source={hiwebLogo}
                        resizeMode={'contain'}
                        style={{ width: width - 100, height: width / 2, alignSelf: 'center' }}
                    />

                    {/* <Icon
                        name={'user-circle-o'}
                        size={150}
                        color={'#EC1D25'}
                        style={{ alignSelf: 'center' }}
                    /> */}


                    <Input
                        value={username}
                        // login
                        iconName={'user'}
                        extraStyles={{ marginTop: 10 }}
                        title={'شماره همراه، ایمیل یا نام کاربری سرویس را وارد نمایید'}
                        onChangeText={username => setUsername(username)}
                        placeholder={'مثلا: 2177889944'}
                    />



                    <Input
                        // login
                        iconName={'unlock-alt'}
                        value={password}
                        title={'رمز عبور'}
                        extraStyles={{ marginTop: 10 }}
                        secureTextEntry
                        onChangeText={password => setPassword(password)}
                        placeholder={'مثلا: 021294000000'}
                    />

                    <Button
                        extraStyles={{ width: width - 32 }}
                        title={'ورود'}
                        showLoading={loading}
                        disabled={loading}
                        onPress={() => submitUserInfo()}
                    />



                    <View style={styles.line} />

                    <View style={styles.innerInputsView}>

                        <TouchableOpacity onPress={() => Linking.openURL('https://onebill.parsonline.com/inquiry')}>
                            <Text style={styles.hdrTitle}>
                                ثبت نام کنید
                            </Text>
                        </TouchableOpacity>

<<<<<<< HEAD
                        <TouchableOpacity onPress={() => Linking.openURL('https://onebill.parsonline.com/passwordrecovery')}>
=======
                        <TouchableOpacity onPress={() => Linking.openURL('https://p.hiweb.ir/passwordrecovery')}>
>>>>>>> f4d81353b7a9c0d70ec16fd7a5857df2f5c9617b
                            <Text style={styles.hdrTitle}>
                                کلمه عبورم را فراموش کردم
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>

            </ScrollView>

            {/* {loading ? <Loading /> : null} */}


            {errModal ?
                <ModalComponent
                    modalVisible={errModal}
                    okModal
                    title={'خطا!'}
                    description={errMessage}
                    okText={'باشه'}
                    onPress={() => setErrModal(0)}
                    onRequestClose={() => setErrModal(0)}
                /> : null}

<<<<<<< HEAD
=======

>>>>>>> f4d81353b7a9c0d70ec16fd7a5857df2f5c9617b
            <ModalComponent
                modalVisible={versionModal}
                okModal
                title={'بروزرسانی نرم افزار'}
<<<<<<< HEAD
                description={'برای استفاده بهتر از نرم افزار پارس‌آنلاین، نسخه جدید را بروزرسانی کنید.'}
=======
                description={'برای استفاده بهتر از نرم افزار های‌وب، نسخه جدید را بروزرسانی کنید.'}
>>>>>>> f4d81353b7a9c0d70ec16fd7a5857df2f5c9617b
                okText={'بروزرسانی'}
                onPress={() => {
                    let link = Platform.OS === 'android' ? AuthStore.userModel.androidLink : AuthStore.userModel.iosLink;
                    Linking.openURL(link);
                }}
                extraTitle={forceUpdate ? undefined : 'بیخیال'}
                extraButtonOnPress={() => setVersionModal(false)}
            />
        </SafeView>
    );
};

const styles = EStyleSheet.create({
    line: {
        width: '100%',
        height: 2,
        backgroundColor: '#0F61AB',
        borderRadius: 10,
        alignSelf: 'center',
        marginVertical: 32,
    },
    footerButton: {
        backgroundColor: 'orange',
        position: 'absolute',
        bottom: -20,
        borderRadius: 0,
        right: 0, left: 0,
        height: 50
    },
    forgetPasswordTitle: {
        fontSize: 15,
        textAlign: 'center',
        color: '#007ACC',
        fontFamily: '$BOLD_FONT',
        textDecorationLine: 'underline',
    },
    innerInputsView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    inputsContainer: {
        height: '50%',
        width: '100%',
        borderBottomWidth: 1,
        borderColor: 'gray',
        padding: 10,
    },
    loginContainer: {
        backgroundColor: 'rgba(255,255,255, 0.7)',
        width: width - 32,
        height: height / 4 + 30,
        alignSelf: 'center',
        borderRadius: 10,
        marginTop: 16,

    },
    logoStyle: {
        height: 100,
        height: 85,
        alignSelf: 'center'
    },
    linearContainer: {
        width: '100%',
        height: '100%',
        backgroundColor: 'rgba(0,0,0,0.8)',
    },
    desTitle: {
        fontSize: 15,
        textAlign: 'center',
        color: '#091315',
        fontFamily: '$REGULAR_FONT',
    },
    hdrTitle: {
        fontSize: 14,
        textAlign: 'center',
        color: '#091315',
        fontFamily: '$BOLD_FONT'
    },
    container: {
        width: width - 16,
        height: height - 16,
        // borderWidth: 1,
        borderColor: '#EC1D25',
        backgroundColor: '#EDF0F5',
        alignSelf: 'center',
        borderRadius: 10,
        marginTop: 10, padding: 10,
    }
});