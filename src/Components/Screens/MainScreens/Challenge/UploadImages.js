import React, { useContext, useState, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    Dimensions,
    ScrollView,
    BackHandler,
    Platform, TouchableHighlight,
    ToastAndroid, ImageBackground
} from 'react-native';
import { SafeView, Header, Button, Input, ModalComponent } from '../../../Commons';
import EStyleSheet from 'react-native-extended-stylesheet';
import ImagePicker from 'react-native-image-crop-picker';
import Icon from 'react-native-vector-icons/AntDesign';
import { Logger, Storage, htmlStyles, CleanHtml } from '../../../../Utils';
import StoreContext from '../../../../Stores';
import { CommonActions } from '@react-navigation/native';
import moment from 'moment-jalaali';
import Axios from 'axios';
import ProgressCircle from 'react-native-progress-circle'
import HTMLView from 'react-native-htmlview';


const widthScreen = Dimensions.get('window').width;
const heightScreen = Dimensions.get('window').height;
const checked = require('../../../../assets/images/checked.png');


export default UploadImages = ({ route, navigation }) => {
    const { ChallengeStore, ChallengeProfileStore } = useContext(StoreContext);

    const [avatarSource, setAvatarSource] = useState([]);
    const [videoUri, setVideoUri] = useState('');
    const [videoName, setVideoName] = useState('');
    const [statusTitle, setStatusTitle] = useState('');
    const [progressbar, setProgressbar] = useState(0);
    const [loading, setLoading] = useState(false);

    const [modalVisible, setModalVisible] = useState(false);

    const [fileSize, setFileSize] = useState(0);
    const [fileSizeString, setFileSizeString] = useState(0);
    const [fileFormat, setFileFormat] = useState('');
    const [size, setSize] = useState('');


    const niceBytes = (bytes) => {
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes == 0) return '0 Byte';
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        let convertedSize = (bytes / Math.pow(1024, i)).toFixed(2) + ' ' + sizes[i];

        setFileSize((bytes / Math.pow(1024, i)).toFixed(2));
        setFileSizeString(convertedSize);
        setSize(sizes[i]);
        return convertedSize;
    };

    const handleSelectFiles = () => {
        ImagePicker.openPicker({
            mediaType: 'photo',
        }).then(image => {
            console.log('image uploaded', image);
            setProgressbar(0)
            setLoading(false);
            setStatusTitle('');

            const source = { uri: image.path };

            // setAvatarSource(source);
            const path = image.path.split('/');
            const name = path[path.length - 1];
            let format = image.path.split('.').pop();

            setVideoName(name);
            setVideoUri(image.path);

            let bytes = niceBytes(image.size);

            const imageData = {
                uri: image.path,
                name: name,
                type: `video/${name.split('.')[1]}`,
                size: bytes,
                format: format
            };
            setAvatarSource(avatarSource => [...avatarSource, imageData]);

        }).catch(err => Logger(err))
    };

    const uploadVideo = async () => {
        setLoading(true);

        let formData = new FormData();
        avatarSource.forEach(file =>
            formData.append('files', {
                uri: file.uri,
                type: file.type,
                name: file.name,
            })
        );



        console.log('data form', formData);
        const token = await Storage.GetItem('@token');
        Axios.post('https://mobileapp.hiweb.ir/api/v1/User/Upload/Post', formData, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            params: {
                section: 'Challenge',
            },
            onUploadProgress: (progressEvent) => {
                const { loaded, total } = progressEvent;
                let percent = Math.floor(loaded * 100 / total);
                // console.log(`${loaded}kb of ${total} | ${percent}%`);
                if (percent < 101) {
                    setProgressbar(percent)
                }
            },
        }).then(data => {
            Logger(data.data, 'UploadChallange')
            ChallengeProfileStore.postImages(data.data, 'image').then(data => {
                if (data) {
                    setProgressbar(0)
                    setLoading(false);

                    ChallengeProfileStore.getChallengeDetails().then(() => {
                        setModalVisible(true);
                        setLoading(false);
                    });

                    setAvatarSource([]);

                };
            });
        }).catch(err => {
            setAvatarSource([]);
            setLoading(false);
            setStatusTitle('مشکلی در سرور برنامه رخ داده است');
            Logger(err, 's err')
        });
    };

    return (
        <SafeView>
            <Header
                title={"ارسال فایل"}
                backOnClick={() => navigation.pop()}
            />



            <ScrollView style={{ paddingTop: 10 }}>
                <TouchableHighlight
                    onPress={() => handleSelectFiles()}
                    underlayColor={'transparent'}>
                    <View style={styles.uploadVideoContainer}>
                        <Text style={styles.statusTitleStyle}>
                            بارگزاری فایل
                        </Text>

                        <Icon
                            name={'clouduploado'}
                            size={35}
                            color={'#12141D'}
                        />

                    </View>
                </TouchableHighlight>

                {avatarSource.length !== 0 ?
                    <View>
                        {avatarSource.map((item, i) => {
                            return (
                                <View key={i} style={styles.uploadVideoContainer}>
                                    {progressbar !== 0 ?
                                        <ProgressCircle
                                            percent={progressbar}
                                            radius={20}
                                            borderWidth={4}
                                            color={'#1AA15F'}
                                            shadowColor={'grey'}
                                            bgColor={'#12141D'}
                                            outerCircleStyle={styles.closeIcon}
                                        >
                                            <Text style={styles.progressTitle}>%{progressbar}</Text>
                                        </ProgressCircle>
                                        :
                                        <Icon
                                            name={'closecircle'}
                                            size={25}
                                            color={'grey'}
                                            onPress={() => setAvatarSource(avatarSource.filter(x => x.uri !== item.uri))}
                                            style={styles.closeIcon}
                                        />
                                    }

                                    <ImageBackground
                                        source={{ uri: item.uri }}
                                        style={styles.avatarImage} >
                                        <View style={styles.footerImageView}>
                                            <Text style={styles.videoInfoTitle}> فرمت فایل شما : {item.format} </Text>
                                            <Text style={styles.videoInfoTitle}> حجم فایل شما : {item.size} </Text>
                                        </View>
                                    </ImageBackground>
                                </View>
                            )
                        })}
                    </View> : null}

            </ScrollView>

            <Button
                disabled={avatarSource.length === 0 || loading}
                showLoading={loading}
                extraStyles={[styles.btnstyles, { opacity: avatarSource.length === 0 || loading ? 0.4 : 1 }]}
                onPress={() => uploadVideo()}
                title={'ارسال فایل'}
            />

            <ModalComponent
                modalVisible={modalVisible}
                okModal
                title={'ثبت شد!'}
                description={'اطلاعات شما با موفقیت ثبت شده‌است.'}
                okText={'باشه'}
                onPress={() => {
                    navigation.navigate('challengeDetails');
                    setModalVisible(false);
                }}
                onRequestClose={() => {
                    navigation.navigate('challengeDetails');
                    setModalVisible(false);
                }}
            />
        </SafeView>
    );
};
const styles = EStyleSheet.create({
    footerImageView: {
        position: 'absolute',
        bottom: 0,
        right: 0,
        left: 0,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 5,
        backgroundColor: 'rgba(0,0,0,0.7)',
    },
    videoInfoTitle: {
        fontSize: 14,
        color: '#fff',
        fontFamily: '$REGULAR_FONT',
        textAlign: 'center',
    },
    line: {
        width: '65%',
        alignSelf: 'center',
        height: 2,
        backgroundColor: '#12141D',
        marginBottom: 32
    },
    progressTitle: {
        fontSize: 13,
        color: '#fff',
        fontFamily: '$BOLD_FONT',
        textAlign: 'center',
    },
    nationalCodeContainer: {
        flexDirection: 'row-reverse',
        alignItems: 'center',
        marginTop: 32,
        justifyContent: 'center',
    },
    nationalCode: {
        fontFamily: '$BOLD_FONT',
        textAlign: 'left',
        color: '#12141D',
        fontSize: 23,
    },
    nationalCodeTitle: {
        fontFamily: '$REGULAR_FONT',
        textAlign: 'center',
        color: '#12141D',
        fontSize: 18,
        marginLeft: 10
    },
    statusTitleStyle: {
        color: '#12141D',
        textAlign: 'center',
        fontSize: 16,
        margin: 16,
        marginBottom: 10,
        fontFamily: '$BOLD_FONT'
    },
    closeIcon: {
        position: 'absolute',
        right: 10,
        top: 10,
        zIndex: 1,
    },
    uploadVideoContainer: {
        width: '90%',
        height: widthScreen / 4,
        backgroundColor: 'transparent',
        borderWidth: 1,
        borderColor: 'grey',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        alignSelf: 'center',
        marginBottom: 10,
    },
    avatarImage: {
        width: '100%',
        height: '100%',
        alignSelf: 'center',
        borderRadius: 5,
    },
    checkedIcon: {
        width: 50,
        height: 50,
        alignSelf: 'center',
        marginVertical: 10,
    },
    checkedcontainer: {
        width: widthScreen - 85,
        alignSelf: 'center',
        borderRadius: 30,
        borderColor: '#12141D',
        alignItems: 'center',
        borderWidth: 1,
        height: 250,
        marginTop: 32,
    },
    desContainer: {
        borderWidth: 1,
        borderColor: 'grey',
        alignItems: 'center',
        marginHorizontal: 16,
        padding: 32,
        borderRadius: 50,
        width: '85%',
        alignSelf: 'center',
    },
    btnstyles: {
        width: '90%',
        borderRadius: 100,
        height: 45,
    },
    desTitle: {
        fontFamily: '$REGULAR_FONT',
        textAlign: 'right',
        color: '#12141D',
        fontSize: 14,
    },
    btn: {
        backgroundColor: 'transparent',
        borderWidth: 1,
        width: '85%',
        height: 70,
        borderRadius: 50,
        borderColor: '#12141D'
    },
})
