import { types, applySnapshot } from 'mobx-state-tree';
import { UserModel } from './models';
import { request, Storage, Base64, Logger, getString } from '../Utils';
import { SingleCourse } from './models/SingleModels';
import Axios from 'axios';
import RNRestart from 'react-native-restart';
import { Platform } from 'react-native';


const authStore = types.model('authStore', {
    loading: types.optional(types.boolean, false),
    username: types.maybeNull(types.string),
    password: types.maybeNull(types.string),
    errMessage: types.maybeNull(types.string),

    userModel: UserModel,
}).actions((self) => {
    return {
        async getUserInfo() {
            return new Promise(async (resolve, reject) => {
                request.get('/v1/User/User/Get')
                    .then(userInfo => {
                        Logger(userInfo, 'user login');
                        if (userInfo.data.isSuccess) {

                            const userInformations = userInfo.data.data;
                            this.fillUserModel({
                                ...self.userModel,
                                ...userInfo,
                                fullName: userInformations.fullName,
                                fatherName: userInformations.fatherName,
                                customerId: userInformations.customerId,
                                birthDate: userInformations.birthDate,
                                gender: userInformations.gender,
                                email: userInformations.email,
                                mobile: userInformations.mobile,
                                nationalId: userInformations.nationalId,
                                organizationRegisterId: userInformations.organizationRegisterId,
                                organizationEconomicCode: userInformations.organizationEconomicCode,
                                organizationPhone: userInformations.organizationPhone,
                                organizationName: userInformations.organizationName,
                                isMobileVerified: userInformations.isMobileVerified,
                                isEmailVerified: userInformations.isEmailVerified,
                                certificateNo: userInformations.certificateNo,
                                postalCode: userInformations.postalCode,
                                isContact: userInformations.isContact,
                                cancelPreviousService: userInformations.cancelPreviousService
                            });
                            this.setUserPass();

                            resolve(true);
                        } else {
                            resolve(false);
                        };
                    }).catch(err => Logger(err));
            }).catch(err => Logger(err));
        },


        async loginUser(username, password) {
            Storage.SetItem('@password', password);
            this.setUserPass(username, password);

            return new Promise(async (resolve, reject) => {
                request.post('/v1/User/Login/Post', {
                    username: self.username,
                    password: self.password,
<<<<<<< HEAD
                    fcpProvider: 'Parsonline',
=======
                    fcpProvider: 'Hiweb',
>>>>>>> f4d81353b7a9c0d70ec16fd7a5857df2f5c9617b
                    platform: Platform.OS,
                    version: AuthStore.userModel.userAppVersion,
                }, {}, false)
                    .then(({ data }) => {
                        Logger(data, 'user login post');
                        if (data.isSuccess) {
                            Storage.SetItem('@token', data.data.accessToken.access_token);
                            Storage.SetItem('@refreshToken', data.data.accessToken.refresh_token);

                            this.fillUserModel({
                                ...self.userModel,
                                ...data.data,
                                message: data.data.message,
                                innerMessage: data.data.innerMessage,
                                customerId: data.data.id,
                            });
                            this.getUserInfo();
                            resolve(data.isSuccess);

                        } else {
                            this.setErrorMessage(data.message);
                            resolve(data.isSuccess);
                        };
                    }).catch(err => Logger(err));
            }).catch(err => Logger(err));
        },

        async refreshToken() {
            const reToken = await Storage.GetItem('@refreshToken');

            Axios.put('https://mobileapp.hiweb.ir/api/v1/User/Login/Put', {
                refreshToken: reToken,
                fCPProvider: 'Parsonline'
            }, {
                headers: {
                    'Content-Type': 'application/json',
                },
                params: {
                }
            }).then(data => {
                Logger(data.data, 'refresh token');
                if (data.data.isSuccess) {
                    Storage.SetItem('@token', data.data.data.access_token);
                    Storage.SetItem('@refreshToken', data.data.data.refresh_token);
                    RNRestart.Restart();
                    return;
                };
                RNRestart.Restart();

            }).catch(err => {
                Logger(err, 'refreshToken err');
                RNRestart.Restart();
            });
        },


        setUserPass(username, password) {
            self.username = username;
            self.password = password;
        },

        setErrorMessage(message) {
            self.errMessage = message;
        },

        fillUserModel(data) {
            applySnapshot(self.userModel, data);
        },
        changeLoading(value) {
            return self.loading = value;
        },
    };
});

export const AuthStore = authStore.create({
    userModel: UserModel.create(),
});
