import { types, applySnapshot } from 'mobx-state-tree';
import { UserModel } from './models';
import { request, Storage, Base64, Logger, getString } from '../Utils';
import { SingleProductList, SingleShoppingCart } from './models/SingleModels';


const shoppingStore = types.model('shoppingStore', {
    data: types.optional(types.array(SingleProductList), []),

    loading: types.optional(types.boolean, false),
    errMessage: types.maybeNull(types.string),
    statusService: types.optional(types.string, ''),

    customerServiceId: types.maybeNull(types.string),
    cartData: types.optional(types.array(SingleShoppingCart), []),

    badgeNumber: types.optional(types.number, 0),
    productServiceInfraName: types.maybeNull(types.string),

}).actions((self) => {
    return {
        async fetchData(clear = false) {
            let serviceId = await Storage.GetItem('@serviceId');

            return new Promise(async (resolve, reject) => {
                if (clear) this.resetList();

                request.get('/v1/User/Product/List', {
                    params: {
                        customerServiceId: serviceId,
                        Size: 50,
                        Page: 1,
                        Sort: 1,
                    }
                }).then(({ data }) => {
                    Logger(data, 'Product List');
                    if (data.isSuccess) {
                        this.fillData(data.data)
                        resolve(data.isSuccess);
                    } else {
                        this.setErrorMessage(data.message);
                        resolve(data.isSuccess);
                    };
                }).catch(err => Logger(err));
            }).catch(err => Logger(err));
        },

        async plusOnclick(id) {
            return new Promise(async (resolve, reject) => {

                self.cartData[0].items.filter(item => {
                    if (item.productId === id) {
                        let count = item.quantity + 1;
                        this.postCart(item.promotionProductId, 'add', count).then(() => this.getShoppingCart(true));
                        console.log('p counter', count);
                        resolve();
                    };

                });
            }).catch(err => Logger(err));
        },

        async minusOnclick(id) {
            return new Promise(async (resolve, reject) => {

                self.cartData[0].items.filter(item => {
                    if (item.productId === id) {
                        let count = item.quantity < 0 ? 0 : item.quantity - 1;
                        this.postCart(item.promotionProductId, 'add', count).then(() => this.getShoppingCart(true));
                        console.log('m counter', count)
                        resolve();
                    };

                })
            }).catch(err => Logger(err));
        },

        setItemCount(count) {
        },

        async getShoppingCart(clear = false) {
            let serviceId = await Storage.GetItem('@serviceId');

            return new Promise(async (resolve, reject) => {

                if (clear) this.resetCartList();

                request.get('/v1/User/ShoppingCard/Get', {
                    params: {
                        CustomerServiceId: serviceId
                    }
                })
                    .then(({ data }) => {
                        Logger(data, 'ShoppingCard get');
                        if (data.isSuccess) {
                            this.fillCartData(data.data);
                            let shoppingListLength = data.data.filter(item => item.customerServiceId === serviceId)[0].items.length;
                            this.setBadge(shoppingListLength);
                            resolve(data.isSuccess);
                        } else {
                            this.setBadge(0);
                            this.setErrorMessage(data.message);
                            resolve(data.isSuccess);
                        };
                    }).catch(err => {
                        Logger(err);
                        resolve();
                    });
            }).catch(err => {
                Logger(err);
                resolve();
            });
        },

        resetCartList() {
            self.cartData = [];
        },

        async setBadgeNumber() {
            let serviceId = await Storage.GetItem('@serviceId');

            this.getShoppingCart(true).then(() => {
                let shoppingListLength = self.cartData.filter(item => item.customerServiceId === serviceId)[0].items.length;
                this.setBadge(shoppingListLength);
            });
        },

        setBadge(data) {
            self.badgeNumber = data;
        },

        async postCart(promotionId, type, count) {
            this.setErrorMessage(null);
            let customerServiceId = await Storage.GetItem('@serviceId');

            return new Promise(async (resolve, reject) => {

                request.post('/v1/User/ShoppingCard/Post', {
                    customerServiceId: customerServiceId,
                    promotionProductId: promotionId,
                    quantity: count,
                }, {}, false).then(({ data }) => {
                    Logger(data, 'ShoppingCard post');
                    resolve(data.isSuccess);
                    if (!data.isSuccess) this.setErrorMessage(data.message);
                }).catch(err => {
                    resolve(false);
                    Logger(err)
                });
            }).catch(err => {
                resolve(false);
                Logger(err)
            });
        },

        async putCart(promotionId, cartId, quantity) {
            let customerServiceId = await Storage.GetItem('@serviceId');

            return new Promise(async (resolve, reject) => {

                request.put('/v1/User/ShoppingCard/Put', {
                    customerServiceId: customerServiceId,
                    quoteId: cartId,

                    quantityItems: [
                        {
                            promotionProductId: promotionId,
                            quantity: quantity
                        }
                    ]
                }).then(({ data }) => {
                    Logger(data, 'ShoppingCard Put');
                    resolve(data.isSuccess);
                }).catch(err => {
                    resolve(false);
                    Logger(err);
                });
            }).catch(err => {
                resolve(false);
                Logger(err);
            });
        },

        async getCheckoutPeyment(cartId) {
            return new Promise(async (resolve, reject) => {

                request.get('/v1/User/Payment/Checkout', {
                    params: {
                        QuoteId: cartId
                    }
                }).then(({ data }) => {
                    Logger(data, 'Payment Checkout');
                    resolve(data.data.result);

                }).catch(err => Logger(err));
            }).catch(err => Logger(err));
        },

        async getPaymentGateWay(cartId) {
            const password = await Storage.GetItem('@password');

            return new Promise(async (resolve, reject) => {

                request.get('/v1/User/Payment/Get', {
                    params: {
                        QuoteId: cartId,
                        Password: password,
                    }
                }).then(({ data }) => {
                    Logger(data, 'Gateway');

                    resolve(data.data);
                }).catch(err => Logger(err));
            }).catch(err => Logger(err));
        },

        async deleteCart(cartId) {
            return new Promise(async (resolve, reject) => {
                request.delete('/v1/User/ShoppingCard/Delete', {
                    params: {
                        QuoteId: cartId
                    }
                }).then(({ data }) => {
                    Logger(data, 'ShoppingCard Delete');
                    resolve(data.isSuccess);
                }).catch(err => Logger(err));
            }).catch(err => Logger(err));
        },

        fillCartData(data) {
            if (data.length === 0) {
                return false;
            }
            Array.prototype.push.apply(self.cartData, data.map(item => item));
        },

        async setCustomerServiceId() {
            let serviceId = await Storage.GetItem('@serviceId');

            self.customerServiceId = serviceId;


        },

        setInfraName(data) {
            self.productServiceInfraName = data;
        },

        clearCart() {
            this.getShoppingCart(true).then(() => {
                self.cartData.map((cart => {
                    this.deleteCart(cart.id);
                }));
            });
        },

        fillData(data) {
            if (data.length === 0) {
                self.isLastPage = true;
                return false;
            }
            Array.prototype.push.apply(self.data, data.map(item => item));
        },
        setErrorMessage(message) {
            self.errMessage = message;
        },
        resetList() {
            self.page = 1;
            self.data = [];
            self.isLastPage = false;
        },
        onEndReached() {
            if (self.isLastPage || self.loading) {
                return false;
            };
            self.page += 1;
            this.fetchData();
        },
        changeLoading(data) {
            self.loading = data;
        },
    };
});
export const ShoppingStore = shoppingStore.create();


const shoppingProfileStore = types.model('shoppingProfileStore', {
    loading: types.optional(types.boolean, false),
    errMessage: types.maybeNull(types.string),

    productDetails: SingleProductList,
}).actions((self) => {
    return {
        async getProductDetail(productId) {
            return new Promise(async (resolve, reject) => {
                request.get('/v1/User/Product/Details', {
                    params: {
                        promotionProductId: productId,
                    }
                }).then(({ data }) => {
                    Logger(data.data, 'Product Details');
                    if (data.isSuccess) {
                        this.fillProductInfo(data.data.result);
                        resolve(data.data.result);
                    }
                }).catch(err => Logger(err));
            }).catch(err => Logger(err));
        },

        fillProductInfo(data) {
            self.productDetails = data;
        },

        setErrorMessage(message) {
            self.errMessage = message;
        },
        changeLoading(value) {
            return self.loading = value;
        },
    };
});

export const ShoppingProfileStore = shoppingProfileStore.create({
    productDetails: SingleProductList.create(),
});
