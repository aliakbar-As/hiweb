import React, { useContext, useEffect, useState } from 'react';
import {
    View,
    Text,
    Image, Dimensions, TouchableHighlight, TouchableWithoutFeedback
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Icon from 'react-native-vector-icons/AntDesign';

const widthScreen = Dimensions.get('window').width;
const heightScreen = Dimensions.get('window').height;

export const ChallengeCard = ({
    file,
    title,
    onPress,
    date,
    status,
    active,
    reward,
    remainDays,
    noDate
}) => {
    return (
        <TouchableHighlight
            onPress={onPress}
            underlayColor={'transparent'}>
            <View style={styles.tutorialBox}>
                <View style={styles.innerContainer}>
                    <View style={styles.leftSide}>
                        <Text numberOfLines={2}
                            style={styles.tutorialTextStyle}>
                            {title}
                        </Text>


                        <Text numberOfLines={1} style={styles.title}>
                            تاریخ برگزاری : {date}
                        </Text>


                        <Text numberOfLines={1} style={styles.title}>
                            جایزه : {reward}
                        </Text>
                    </View>

                    <Image source={{ uri: file }} style={styles.tutorialImageStyle} />
                </View>

                <View style={styles.footer}>
                    <Icon
                        name={active ? 'checkcircle' : active === false ? 'closecircle' : 'minuscircle'}
                        color={active ? '#629E22' : active === false ? '#0F61AB' : '#FC4136'}
                        size={20}
                        style={{ marginRight: 16 }}
                    />

                    <Text style={[styles.title, {
                        fontSize: 14,
                        color: active ? '#629E22' :
                            active === null ? '#FC4136' : '#0F61AB'
                    }]}>
                        {active ? 'در حال برگزاری' : active === null ? 'بزودی' : 'مهلت تمام شده'}
                    </Text>

                </View>

                {remainDays < 0 ? null :
                    <View style={styles.badgeContainer}>
                        <Text style={styles.badgeTitle}>
                            {remainDays} روز تا پایان
                        </Text>
                    </View>}
            </View>
        </TouchableHighlight>
    );
};

const styles = EStyleSheet.create({
    badgeContainer: {
        position: 'absolute',
        left: 0,
        top: 0,
        bottom: 0,
        backgroundColor: '#629E22',
        height: '75%',
        width: '10%',
    },
    badgeTitle: {
        transform: [{ rotate: '-90deg' }],
        fontSize: 14,
        width: 300,
        alignSelf: 'center',
        textAlign: 'center',
        color: '#fff',
        textAlignVertical: 'center',
        flex: 1,
        fontFamily: '$REGULAR_FONT'
    },
    innerContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        marginRight: 6,
        marginTop: 6
    },
    title: {
        fontFamily: '$REGULAR_FONT',
        color: '#12141D',
        fontSize: 13,
        textAlign: 'right',
    },
    footer: {
        height: '25%',
        borderTopWidth: 1,
        width: '100%',
        position: 'absolute',
        bottom: 0,
        right: 0,
        left: 0,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        borderColor: '#DEDEDE',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    leftSide: {
        alignItems: 'flex-end',
        flexDirection: 'column',
        justifyContent: 'flex-end',
        width: '65%',
        marginRight: 10
    },
    tutorialBox: {
        width: '90%',
        height: heightScreen / 4,
        borderWidth: 1,
        borderColor: '#D5D5D5',
        alignItems: 'center',
        borderRadius: 10,
        marginTop: 16,
        backgroundColor: '#FCFBFB',
        flexWrap: 'nowrap',
        overflow: 'hidden',
        alignSelf: 'center'
    },
    tutorialImageStyle: {
        width: widthScreen / 4,
        height: widthScreen / 4,
        borderRadius: 15,
        // right: -10,
    },
    tutorialTextStyle: {
        fontFamily: '$BOLD_FONT',
        color: '#0E0E0E',
        fontSize: 16,
        flexWrap: 'nowrap',
        textAlign: 'right',
        marginBottom: 6,
        width: '90%',
    },
})