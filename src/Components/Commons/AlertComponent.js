import React from 'react';
import {
    View,
    Text,
    Image,
    Modal,
} from 'react-native';
import { Button } from './Button';
import EStyleSheet from 'react-native-extended-stylesheet';

export const ModalComponent = ({
    modalVisible,
    onRequestClose,
    title,
    description,
    onPress,
    extraButton,
    extraButtonOnPress,
    extraTitle,
    okText
}) => {
    return (
        <Modal
            animationType="fade"
            transparent={true}
            visible={modalVisible}
            onRequestClose={onRequestClose}
        >
            <View style={styles.modalContainer}>
                <View style={styles.modaInnerContainer}>
                    <View style={styles.hdrContainer}>
                        <Text style={styles.title}>
                            {title}
                        </Text>
                    </View>

                    <Text style={styles.desTitle}>
                        {description}
                    </Text>

                    <View style={[styles.footerContainer, {
                        justifyContent: extraTitle === undefined ? 'center' : 'space-between',
                    }]}>
                        <Button
                            title={okText}
                            extraStyles={extraTitle === undefined ? styles.okBtn : styles.btn}
                            onPress={onPress}
                        />

                        {extraTitle === undefined ? null :
                            <Button
                                title={extraTitle}
                                extraStyles={[styles.btn, { backgroundColor: "#000" }]}
                                onPress={extraButtonOnPress}
                            />}
                    </View>
                </View>
            </View>
        </Modal>
    );
};
const styles = EStyleSheet.create({
    okBtn: {
        width: '100%',
        borderRadius: 10,
        height: 35,
        marginBottom: -10
    },
    footerContainer: {
        flexDirection: 'row-reverse',
        width: '100%',
        paddingHorizontal: 16,
        alignItems: 'center',
    },
    btn: {
        width: '45%',
        borderRadius: 10,
        height: 35,
        marginBottom: -10
    },
    hdrContainer: {
        borderColor: '#ccc',
        width: '100%',
        // paddingBottom: 10
    },
    title: {
        fontFamily: '$BOLD_FONT',
        textAlign: 'right',
        fontSize: 16,
        color: '#2d2d2d',
    },
    desTitle: {
        textAlign: 'center',
        fontSize: 14,
        color: '#2d2d2d',
        marginTop: 16,
        fontFamily: '$REGULAR_FONT',
    },
    modalContainer: {
        backgroundColor: 'rgba(0,0,0,0.6)',
        flex: 1,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    modaInnerContainer: {
        backgroundColor: '#fff',
        padding: 10,
        paddingBottom: 30,
        // borderRadius: 10,
        alignItems: 'center',
        width: '85%',
    },
});