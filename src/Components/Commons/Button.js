import React from 'react';
import {
    SafeAreaView,
    StatusBar,
    TouchableOpacity,
    Text,
    Image,
    ActivityIndicator,
    TouchableWithoutFeedback,
    TouchableHighlight
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import EStyleSheet from 'react-native-extended-stylesheet';


export const Button = ({
    title,
    onPress,
    extraStyles,
    leftHasIcon,
    leftIcon,
    rightHasIcon,
    rightIcon,
    simple,
    redStyle,
    showLoading,
    disabled,
    primary,
    extraTitleStyle,
    showIcon, showCheckIcon,
}) => {
    return (
        <TouchableOpacity
            disabled={disabled}
            style={[styles.btn, extraStyles]}
            onPress={onPress}>


            {showLoading ?
                <ActivityIndicator
                    size={'small'}
                    color={'#fff'}
                />
                :
                <Text style={[styles.mainTitle, extraTitleStyle]}>
                    {title}
                </Text>
            }

            {showIcon ?
                <Icon
                    color={'#fff'}
                    size={15}
                    name={'arrowright'}
                    style={[styles.arrowIcon, { alignSelf: 'flex-end', }]}
                />
                : null}


            {showCheckIcon ?
                <Icon
                    color={'#fff'}
                    size={20}
                    name={'checkcircle'}
                    style={styles.arrowIcon}
                />
                : null}
        </TouchableOpacity>
    );
};

const styles = EStyleSheet.create({
    arrowIcon: {
        padding: 5,
        borderRadius: 100,
        backgroundColor: 'rgba(0, 0, 0, 0.2)',
    },
    Pbtn: {
        width: '90%',
        height: 45,
        backgroundColor: '#FF055B',
        alignSelf: 'center',
        borderRadius: 10,
        marginVertical: 16,
        flexDirection: 'row',
        alignItems: 'center',
        padding: 10,
        justifyContent: 'center',
    },
    mainTitle: {
        fontSize: 16,
        color: '#fff',
        textAlign: 'center',
        textAlignVertical: 'center',
        flex: 1,
        fontFamily: '$REGULAR_FONT',
    },
    btn: {
        width: '100%',
        height: 45,
        backgroundColor: '$MAIN_THEME',
        alignSelf: 'center',
        borderRadius: 10,
        marginVertical: 16,
        flexDirection: 'row',
        alignItems: 'center',
        padding: 10,
        justifyContent: 'center',
        // borderWidth: 1,
        // borderColor: ''
    },
    container: {
        flex: 1,
        backgroundColor: '#000000'
    },
});
