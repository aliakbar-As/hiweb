import React, { useState, useContext, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    Dimensions, TouchableHighlight,
    TouchableOpacity,
    Modal, TouchableWithoutFeedback, FlatList
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Swiper from 'react-native-swiper';
import { Button, Header, List, Loading, ModalComponent, SafeView } from '../../../Commons';
import { SlideCard } from '../../../Elements';
import ProgressCircle from 'react-native-progress-circle'
import StoreContext from '../../../../Stores';
import { useObserver } from 'mobx-react-lite';
import moment from 'moment-jalaali';
import { Numbers } from '../../../../Utils';

const { width, height } = Dimensions.get('window');

let tabItems = [
    {
        id: 2,
        title: 'منقضی شده'
    },
    {
        id: 1,
        title: 'غیرفعال'
    },
    {
        id: 0,
        title: 'فعال'
    },
];

export default BundleScreen = ({ navigation, route }) => {
    const { AuthStore, ShoppingStore, BundleStore } = useContext(StoreContext);

    const [loading, setLoading] = useState(false);
    const [loadingList, setLoadingList] = useState(false);
    const [tabSelectedId, setTabSelectedId] = useState(0);

    const [errModal, setErrModal] = useState(0);
    const [errMessage, setErrMessage] = useState('');

    const [customerServiceProductId, setDustomerServiceProductId] = useState('');

    const getBundleList = (id) => {
        setTabSelectedId(id);
        setLoadingList(true);

        BundleStore.setActionType(id === 0 ? 'Active' : id === 1 ? 'ReadyToActive' : 'Deactived');
        BundleStore.fetchData(true).then((data) => {
            setLoadingList(false);
        });
    };


    const activeBundle = () => {
        setErrModal(0);
        setLoadingList(true);
        BundleStore.activeBundle(customerServiceProductId).then(res => {
            if (typeof res !== 'string') {
                setTabSelectedId(0);
                setLoadingList(false);
            } else {
                setErrModal(-1);
                setErrMessage(res);
                setLoadingList(false);

            }

        });
    };

    return useObserver(() => (
        <SafeView>
            <Header
                title={"گزارش بسته ها"}
                backOnClick={() => navigation.pop()}
            />


            <List
                ListHeader={() => (
                    <View style={styles.tabMainView}>
                        {tabItems.map((tabItem => {
                            return (
                                <TouchableHighlight
                                    key={tabItem.id}
                                    disabled={tabSelectedId === tabItem.id}
                                    onPress={() => getBundleList(tabItem.id)}
                                    underlayColor={'transparent'}>
                                    <View style={[styles.filterView, {
                                        borderBottomColor: tabSelectedId === tabItem.id ? '#FF0909' : '#060606',
                                        borderBottomWidth: tabSelectedId === tabItem.id ? 3 : 1,
                                    }]}>
                                        <Text style={[styles.filterTitle, {
                                            color: tabSelectedId === tabItem.id ? '#FF0909' : '#060606',
                                        }]}>
                                            {tabItem.title}
                                        </Text>
                                    </View>
                                </TouchableHighlight>
                            )
                        }))}
                    </View>
                )}
                onRefresh={() => getBundleList(tabSelectedId)}
                showLoading={loadingList}
                store={BundleStore}
                ListEmptyComponent={() => {
                    if (!loadingList) {
                        return (
                            <Text style={styles.emptyDataTitle}>
                                موردی یافت نشد!
                            </Text>
                        )
                    } else return null;
                }}
                renderItem={(item, index) => {
                    return (
                        <View style={styles.cardView}>
                            <View style={styles.innerCardView}>
                                <Text style={styles.hdrTitle}>{index + 1}</Text>
                                <Text style={styles.primaryTitle}>
                                    ردیف:
                                </Text>
                            </View>

                            <View style={styles.innerCardView}>
                                <Text style={styles.hdrTitle}>
                                    {item.productName}
                                    {'\n'}
                                    {item.customerService}
                                </Text>

                                <Text style={styles.primaryTitle}>
                                    عنوان :
                                </Text>
                            </View>

                            <View style={styles.innerCardView}>
                                <Text style={styles.hdrTitle}>
                                    {item.isServiceBase ? 'سرویس پایه' : 'ترافیک اضافه'}
                                </Text>
                                <Text style={styles.primaryTitle}>نوع:</Text>
                            </View>

                            <View style={styles.innerCardView}>
                                <Text style={styles.hdrTitle}>{item.remainTraffic} MB</Text>
                                <Text style={styles.primaryTitle}>ترافیک باقیمانده:</Text>
                            </View>

                            {item.purchaseDate !== null ?
                                <View style={styles.innerCardView}>
                                    <Text style={styles.hdrTitle}>{moment(item.purchaseDate).format('jYYYY/jMM/jDD   HH:mm')}</Text>
                                    <Text style={styles.primaryTitle}>تاریخ خرید: </Text>
                                </View>
                                : null}


                            {tabSelectedId === 1 ? null :
                                <View style={styles.innerCardView}>
                                    <Text style={styles.hdrTitle}>
                                        {item.activationDate !== null ? moment(item.activationDate).format('jYYYY/jMM/jDD   HH:mm') : null}
                                        {'\n'}
                                        {item.expirationDate !== null ? moment(item.expirationDate).format('jYYYY/jMM/jDD   HH:mm') : null}
                                    </Text>

                                    <Text style={styles.primaryTitle}>
                                        {item.activationDate !== null ? 'تاریخ فعال سازی:' : null}
                                        {'\n'}

                                        {item.expirationDate !== null ? 'انقضاء:' : null}
                                    </Text>
                                </View>}

                            {item.stopDate !== null ?
                                <View style={styles.innerCardView}>
                                    <Text style={styles.hdrTitle}>{moment(item.stopDate).format('jYYYY/jMM/jDD   HH:mm')}</Text>
                                    <Text style={styles.primaryTitle}>تاریخ ابطال: </Text>
                                </View>
                                : null}

                            {tabSelectedId === 1 ?
                                <Button
                                    showLoading={loading}
                                    disabled={loading}
                                    extraStyles={{ backgroundColor: '#8BC34A' }}
                                    onPress={() => {
                                        setDustomerServiceProductId(item.customerServiceProductId);
                                        setErrModal(1000);
                                        setErrMessage('توجه داشته باشید با انجام این عملیات :\n تمامی سرویس های فعال شما غیر فعال خواهد شد. \nسرویس جدید فعال خواهد شد \nمبلغ محاسبه شده جهت ابطال سرویس های فعلی 0 ریال است. \nمبلغ محاسبه شده جهت ابطال خدمات/آبونمان 0 ریال است.')
                                        // activeBundle(item.customerServiceProductId)
                                    }}
                                    title={'فعالسازی'} />
                                : null}
                        </View>
                    )
                }}

            />

            {errModal !== 0 ?
                <ModalComponent
                    modalVisible={errModal !== 0}
                    okModal
                    title={errModal === 1000 ? 'اطلاعیه' : 'خطا!'}
                    description={errMessage}
                    extraTitle={errModal === 1000 ? 'مخالفم' : undefined}
                    okText={errModal === 1000 ? 'موافقم' : 'باشه'}
                    onPress={() => errModal === 1000 ? activeBundle() : setErrModal(0)}
                    extraButtonOnPress={() => setErrModal(0)}
                    onRequestClose={() => setErrModal(0)}
                /> : null}

        </SafeView>
    ));
};


const styles = EStyleSheet.create({
    titles: {
        fontSize: 13,
        color: '#505050',
        textAlign: 'right',
        fontFamily: '$REGULAR_FONT'
    },
    activeContainer: {
        width: 10,
        marginLeft: 10,
        height: 10,
        borderRadius: 15,
        backgroundColor: '#6CD7B0'
    },
    statusContainer: {
        alignItems: 'center',
        backgroundColor: '#EDF1FD',
        flexDirection: 'row',
        paddingHorizontal: 5,
        borderRadius: 16,
        paddingVertical: 1,
        position: 'absolute',
        left: 0,
    },
    innerCardView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',

    },
    hdrTitle: {
        fontSize: 16,
        color: '#505050',
        textAlign: 'right',
        fontFamily: '$BOLD_FONT',
        width: '70%'
    },
    cardView: {
        backgroundColor: '#fff',
        marginHorizontal: 16,
        marginTop: 10,
        borderRadius: 10,
        padding: 10,
        alignItems: 'flex-end',
        justifyContent: 'space-around'
    },
    primaryTitle: {
        fontSize: 16,
        color: '#505050',
        textAlign: 'right',
        fontFamily: '$REGULAR_FONT',
        width: '35%',
    },
    emptyDataTitle: {
        color: '#2d2d2d',
        textAlign: 'center',
        fontSize: 16,
        fontFamily: '$BOLD_FONT',
        marginTop: '50%',
        marginHorizontal: 32,
    },
    tabMainView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    filterView: {
        width: width / 3,
        alignSelf: 'center',
        justifyContent: 'center',
        height: width / 9,
        overflow: 'hidden',
        borderBottomWidth: 1,
        backgroundColor: '#fff'
    },
    filterTitle: {
        color: '#12141D',
        textAlign: 'center',
        fontFamily: '$BOLD_FONT',
        fontSize: 16
    },
});