import React, { useState, useContext, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    Dimensions, TouchableHighlight,
    TouchableOpacity,
    Modal, TouchableWithoutFeedback,

} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Button, Header, List, Loading, SafeView } from '../../../Commons';
import StoreContext from '../../../../Stores';
import { useObserver } from 'mobx-react-lite';
import Icon from 'react-native-vector-icons/FontAwesome';

const { width, height } = Dimensions.get('window');

let reportsList = [
    {
        id: 0,
        title: 'گزارش بسته‌ها',
        icon: 'bar-chart-o',
    },
    {
        id: 1,
        title: 'گزارش مصرف ترافیک',
        icon: 'line-chart'
    },
];

export default ReportsList = ({ navigation, route }) => {
    const { AuthStore, CounterStore, ServiceReportStore, BundleStore } = useContext(StoreContext);

    const [loading, setLoading] = useState(false);

    const reportListOnclick = (id) => {
        switch (id) {
            case 0:
                setLoading(true);
                BundleStore.fetchData(true).then((data) => {
                    navigation.navigate('bundle');
                    setLoading(false);
                });
                break;

            case 1:
                ServiceReportStore.resetList();
                navigation.navigate('reports');
                break;

            default:
                break;
        };
    };

    return useObserver(() => (
        <SafeView>
            <Header
                title="لیست گزارش‌ها"
                backOnClick={() => navigation.pop()}
                noBack
                userOnclick={() => navigation.navigate('profile')}
                hasUserIcon
            />

            {reportsList.map((report => {
                return (
                    <TouchableHighlight
                        key={report.id}
                        onPress={() => reportListOnclick(report.id)}
                        underlayColor={'transparent'}>
                        <View style={styles.reportView}>

                            <Icon
                                name={'angle-left'}
                                size={20}
                                color={'gray'}
                            />
                            <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.titles}>{report.title}</Text>

                                <Icon
                                    name={report.icon}
                                    color={EStyleSheet.value('$LINK_COLOR')}
                                    size={20}
                                    style={{ marginLeft: 10 }}
                                />
                            </View>

                        </View>
                    </TouchableHighlight>
                )
            }))}

            {loading ? <Loading /> : null}
        </SafeView>
    ));
};


const styles = EStyleSheet.create({
    reportView: {
        borderBottomWidth: 1,
        borderColor: '#ccc',
        padding: 16,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    titles: {
        fontSize: 16,
        color: '#505050',
        textAlign: 'right',
        fontFamily: '$REGULAR_FONT'
    },

});