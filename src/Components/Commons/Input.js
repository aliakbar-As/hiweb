import React from 'react';
import {
    SafeAreaView,
    StatusBar,
    TouchableOpacity,
    Text,
    Image,
    View,
    TouchableHighlight,
    TextInput,
    Dimensions
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import EStyleSheet from 'react-native-extended-stylesheet';


export const Input = ({
    extraStyles,
    value,
    onChangeText,
    placeholder,
    placeholderTextColor,
    secureTextEntry,
    multiline,
    keyboardType,
    editable,
    extraInpuStyles,
    onFocus,
    autoFocus,
    maxLength,
    onSubmitEditing,
    returnKeyType,
    login, title, pExtraStyles,
    iconName
}) => {
    if (login) {
        return (
            <View style={[styles.container, extraStyles]}>

                <View style={styles.inputContainer}>

                    <Icon
                        size={20}
                        name={iconName}
                        color={'#EC1D25'}
                    />

                    <TextInput
                        value={value}
                        selectionColor={'#FF055B'}
                        onFocus={onFocus}
                        style={[styles.input, extraInpuStyles]}
                        placeholderTextColor={placeholderTextColor}
                        autoFocus={autoFocus}
                        onSubmitEditing={onSubmitEditing}
                        editable={editable}
                        returnKeyType={returnKeyType}
                        keyboardType={keyboardType}
                        multiline={multiline}
                        secureTextEntry={secureTextEntry}
                        placeholder={placeholder}
                        onChangeText={onChangeText}
                        maxLength={maxLength}
                    />
                </View>
            </View>
        );
    } else {
        return (
            <View style={[styles.container, extraStyles]}>

                {title !== undefined ? <Text style={styles.hdrTitle}>{title}</Text> : null}

                <View style={[styles.pInputContainer, pExtraStyles]}>
                    <TextInput
                        value={value}
                        selectionColor={'#FF055B'}
                        onFocus={onFocus}
                        style={[styles.input, extraInpuStyles]}
                        placeholderTextColor={placeholderTextColor}
                        autoFocus={autoFocus}
                        onSubmitEditing={onSubmitEditing}
                        editable={editable}
                        returnKeyType={returnKeyType}
                        keyboardType={keyboardType}
                        multiline={multiline}
                        secureTextEntry={secureTextEntry}
                        placeholder={placeholder}
                        onChangeText={onChangeText}
                        maxLength={maxLength}
                    />
                </View>
            </View>
        );
    };

};

const styles = EStyleSheet.create({
    pInputContainer: {
        backgroundColor: '#fff',
        alignItems: 'center',
        borderRadius: 10,
        width: '100%',
        height: 40,
        marginTop: 5,
        borderWidth: 1,
        borderColor: '#ccc',
    },
    hdrTitle: {
        fontFamily: '$BOLD_FONT',
        textAlign: 'right',
        color: '#333333',
        fontSize: 16
    },
    input: {
        alignSelf: 'stretch',
        flex: 1,
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
        paddingRight: 10,
        textAlign: 'right',
        color: '#2d2d2d',
        fontFamily: '$REGULAR_FONT'
    },
    inputContainer: {
        backgroundColor: '#fff',
        flexDirection: 'row-reverse',
        alignItems: 'center',
        borderRadius: 10,
        width: '100%',
        height: 40,
        marginTop: 5,
        borderColor: '#EC1D25',
        borderWidth: 1,
        paddingHorizontal: 10,
    },
    container: {
        width: '100%',
        alignSelf: 'center',
        backgroundColor: 'transparent',
    },
});
