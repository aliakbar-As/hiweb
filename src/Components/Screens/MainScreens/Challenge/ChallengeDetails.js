import React, { useContext, useEffect, useState, useRef } from 'react';
import {
    View,
    Text,
    Image,
    Dimensions,
    ScrollView,
    ImageBackground,
    Linking,
    // AppState
} from 'react-native';
import { SafeView, Header, Button, Input, ModalComponent, List } from '../../../Commons';
import EStyleSheet from 'react-native-extended-stylesheet';
import StoreContext from '../../../../Stores';
import { useObserver } from 'mobx-react-lite';
import HTMLView from 'react-native-htmlview';
import { CleanHtml, htmlStyles } from '../../../../Utils';

let tabItem = [
    {
        id: 1,
        title: 'آزمون کمربند',
    },
    {
        id: 2,
        title: 'رویدادهای آنلاین',
    },
];


const { width, height } = Dimensions.get('window');


export default ChallengeDetails = ({ route, navigation }) => {
    const { ChallengeStore, ChallengeProfileStore } = useContext(StoreContext);

    const [modalVisible, setModalVisible] = useState(false);
    const [errMessage, setErrorMessage] = useState('');

    const [contributorId, setContributorId] = useState(0);
    const [loading, setLoading] = useState(false);
    const [loadingImages, setLoadingImages] = useState(false);



    const sendVideo = () => {
        setLoading(true);
        ChallengeProfileStore.postContributer().then(data => {
            if (typeof data === 'string') {
                setErrorMessage(data);
                setModalVisible(true);
                setLoading(false);

            } else {
                ChallengeProfileStore.getTemplateSingle('video').then(res => {
                    navigation.navigate('videoState', { templateData: res });
                    setLoading(false);
                });
            };
        });
    };

    const sendPicture = () => {
        setLoadingImages(true);
        ChallengeProfileStore.postContributer().then(data => {
            if (typeof data === 'string') {
                setErrorMessage(data);
                setModalVisible(true);
                setLoadingImages(false);

            } else {
                ChallengeProfileStore.getTemplateSingle().then(res => {
                    navigation.navigate('picturesState', { templateData: res });
                    setLoadingImages(false);
                });
            };
        });
    };

    return useObserver(() => (
        <SafeView>
            <Header
                title={"مسابقه"}
                backOnClick={() => navigation.pop()}
            />

            <ScrollView>
                <ImageBackground
                    source={{ uri: ChallengeProfileStore.challengeDetails.picture }}
                    resizeMode={'stretch'}
                    style={styles.hdrImg}
                />

                <Text style={styles.hdrTitle}>
                    {ChallengeProfileStore.challengeDetails.title}
                </Text>

                <View style={styles.line} />

                <View style={styles.desContainer}>
                    <HTMLView
                        value={CleanHtml(ChallengeProfileStore.challengeDetails.description)}
                        stylesheet={{ ...htmlStyles }}
                    />

                </View>

                <Button
                    showCheckIcon={ChallengeProfileStore.challengeDetails.videoFile !== null}
                    extraStyles={[styles.btn, {
                        opacity: !ChallengeProfileStore.challengeDetails.isActive ||
                            ChallengeProfileStore.challengeDetails.videoFile !== null ||
                            ChallengeProfileStore.challengeDetails.isActive === null ? 0.5 : 1
                    }]}
                    showLoading={loading}
                    disabled={!ChallengeProfileStore.challengeDetails.isActive ||
                        loading || ChallengeProfileStore.challengeDetails.videoFile !== null
                        || ChallengeProfileStore.challengeDetails.isActive === null}
                    onPress={() => sendVideo()}
                    title={!ChallengeProfileStore.challengeDetails.isActive ||
                        ChallengeProfileStore.challengeDetails.isActive === null ? 'مهلت تمام شده است' : 'چالش ویدیو'}
                />

                <Button
                    showCheckIcon={ChallengeProfileStore.challengeDetails.image_File !== null}
                    extraStyles={[styles.btn, { marginTop: 0 }, {
                        opacity: !ChallengeProfileStore.challengeDetails.isActive ||
                            ChallengeProfileStore.challengeDetails.image_File !== null ||
                            loading || ChallengeProfileStore.challengeDetails.videoFile === null ||
                            ChallengeProfileStore.challengeDetails.isActive === null ? 0.5 : 1
                    }]}
                    showLoading={loading}
                    disabled={!ChallengeProfileStore.challengeDetails.isActive || loadingImages ||
                        ChallengeProfileStore.challengeDetails.videoFile === null ||
                        ChallengeProfileStore.challengeDetails.image_File !== null ||
                        ChallengeProfileStore.challengeDetails.isActive === null}
                    onPress={() => sendPicture()}
                    title={!ChallengeProfileStore.challengeDetails.isActive ||
                        ChallengeProfileStore.challengeDetails.isActive === null ? 'مهلت تمام شده است' : 'چالش عکس'}
                />

                {ChallengeProfileStore.userHasLogged ?
                    <Text style={styles.alertTitle}>
                        شما قبلا در این مسابقه شرکت کرده‌اید
                    </Text>
                    : null}

                {modalVisible ?
                    <ModalComponent
                        modalVisible={modalVisible}
                        onRequestClose={() => setModalVisible(false)}
                        OkModal
                        title={'خطا!'}
                        description={errMessage}
                        okText={'باشه'}
                        disbaleCancelOption
                        onPress={() => setModalVisible(false)}
                    />
                    : null}

            </ScrollView>
        </SafeView>
    ));
};



const styles = EStyleSheet.create({
    alertTitle: {
        fontFamily: '$BOLD_FONT',
        textAlign: 'center',
        color: 'red',
        fontSize: 15,
    },
    desContainer: {
        borderWidth: 1,
        borderColor: 'gray',
        alignSelf: 'center',
        alignItems: 'center',
        borderRadius: 20,
        backgroundColor: '#fff',
        padding: 16,
        margin: 16,
    },
    line: {
        width: '40%',
        alignSelf: 'center',
        height: 3,
        backgroundColor: 'grey',
        marginTop: 10,
    },
    hdrImg: {
        height: height / 4,
        width: width - 32,
        borderRadius: 15,
        marginTop: 10,
        alignSelf: 'center',
        overflow: 'hidden',
        borderWidth: 2,
        borderColor: '#000'
    },
    btn: {
        width: '90%',
        borderRadius: 100,
        height: 45,
        marginTop: 16
    },
    codeInputstyles: {
        width: '70%',
        borderRadius: 10,
        borderColor: 'grey',
    },
    desTitle: {
        fontFamily: '$REGULAR_FONT',
        textAlign: 'center',
        color: '#000',
        fontSize: 14,
    },
    hdrTitle: {
        fontFamily: '$BOLD_FONT',
        textAlign: 'center',
        color: '#000',
        fontSize: 15,
        marginTop: 16
    },
});