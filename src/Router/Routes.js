import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';


import Splash from '../Components/Screens/AuthScreens/Splash';
import Login from '../Components/Screens/AuthScreens/Login';
import ForgetPassword from '../Components/Screens/AuthScreens/ForgetPassword';

import TabScreens from './TabScreens/TabScreens';
import ProductDetails from '../Components/Screens/MainScreens/Shopping/ProductDetails';
import ChallengeList from '../Components/Screens/MainScreens/Challenge/ChallengeList';
import ChallengeDetails from '../Components/Screens/MainScreens/Challenge/ChallengeDetails';



const Stack = createStackNavigator();


export default RouterComponent = () => {
    return (
        <NavigationContainer>

            <Stack.Navigator
                initialRouteName="splash"
                headerMode="none"
                mode={'card'}
            >
                <Stack.Screen
                    name="splash"
                    component={Splash}
                />

                <Stack.Screen
                    name="login"
                    component={Login}
                />

                <Stack.Screen
                    name="forgetPassword"
                    component={ForgetPassword}
                />

                <Stack.Screen
                    name="tabScreens"
                    component={TabScreens}
                />


                {/* hide tab bottom in this specific screen */}
                <Stack.Screen
                    name="productDetails"
                    component={ProductDetails}
                />


                

            </Stack.Navigator>


        </NavigationContainer>

    )
};
