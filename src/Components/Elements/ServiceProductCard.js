
import React, { useState, useContext, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    Dimensions, TouchableHighlight,
    TouchableOpacity,
    Modal, TouchableWithoutFeedback, FlatList
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import moment from 'moment-jalaali';
import { Numbers } from '../../Utils';
import { Button } from '../Commons';

const { width, height } = Dimensions.get('window');



export const ServiceProductCard = ({
    categoryPriority,
    trafficInternalWithUnit,
    trafficWithUnit,
    productName, hasProductName,
    amount,
    onPress,
    buyOnclick,
    speed, time, trafficInternal,
    catId, showInternalTraffic
}) => {
    return (
        <TouchableHighlight
            onPress={onPress}
            underlayColor={'transparent'}>
            <View style={styles.productCardContainer}>

                {hasProductName ? null :
                    <View style={[styles.innerProductCard, { justifyContent: time !== null ? 'space-between' : 'center', }]}>
                        {time !== null ?
                            <View style={[styles.titleView, { borderRightWidth: 1, borderColor: '#eee' }]}>
                                <Text style={[styles.hdrTitle, { fontSize: 25 }]}>
                                    {time}
                                    {'\n'}
                                    <Text style={styles.primaryTitle}>ماه</Text>
                                </Text>
                            </View> : null}

                        {trafficWithUnit !== null ?
                            <View style={styles.titleView}>
                                <Text style={[styles.hdrTitle, { fontSize: 25 }]}>
                                    {trafficInternal}
                                    {'\n'}
                                    <Text style={styles.primaryTitle}>گیگابایت</Text>
                                </Text>
                            </View> : null}

                    </View>}

                {hasProductName ?
                    <View style={styles.titleView3}>
                        <Text style={styles.hdrTitle}>
                            <Text style={styles.primaryTitle}>
                                {productName}
                            </Text>
                        </Text>
                    </View> : null}

                <View style={[styles.innerProductCard, { justifyContent: 'center', }]}>
                    {catId === 'd7f8d2e6-f25d-ea11-a94d-005056818579' || catId === 'dbf8d2e6-f25d-ea11-a94d-005056818579' ?
                        <View style={styles.titleView2}>
                            <Text style={styles.hdrTitle}>
                                <Text style={styles.primaryTitle}>
                                    {productName}
                                </Text>
                            </Text>
                        </View>
                        :
                        <View style={styles.titleView2}>
                            <Text style={styles.hdrTitle}>
                                {trafficWithUnit} ترافیک بین الملل
                                {'\n'}
<<<<<<< HEAD
                                <Text style={styles.primaryTitle}>
                                    معادل {trafficInternalWithUnit} داخلی
                                </Text>
=======

                                {showInternalTraffic ?
                                    <Text style={styles.primaryTitle}>
                                        معادل {trafficInternalWithUnit} داخلی
                                    </Text> : null}
>>>>>>> f4d81353b7a9c0d70ec16fd7a5857df2f5c9617b
                            </Text>
                        </View>}


                </View>



                {speed === 0 ? null :
                    <View style={[styles.innerProductCard, { justifyContent: 'center', }]}>
<<<<<<< HEAD
                        <Text style={[styles.primaryTitle, { fontSize: 16, color: '#2d2d2d' }]}>
=======
                        <Text style={[styles.primaryTitle, { fontSize: 16, color: '#2d2d2d', fontFamily: EStyleSheet.value('$BOLD_FONT') }]}>
>>>>>>> f4d81353b7a9c0d70ec16fd7a5857df2f5c9617b
                            سرعت {speed} مگابیت
                        </Text>
                    </View>}


                <View style={[styles.innerProductCard, { justifyContent: 'center', }]}>
                    <View style={styles.titleView2}>
                        <Text style={styles.hdrTitle}>{Numbers.putCommas(amount)} ریال</Text>
                    </View>
                </View>

                <Button
                    title={'خرید'}
                    onPress={buyOnclick}
                    showIcon
                />
            </View>
        </TouchableHighlight>
    );
};



const styles = EStyleSheet.create({
    titleView3: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        borderTopWidth: 1,
        borderColor: '#eee',
        borderBottomWidth: 1,
        padding: 10,
    },
    titleView2: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleView: {
        width: '50%',
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center'
    },
    innerProductCard: {
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: 10,
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderColor: '#eee',
    },
    trafficView: {
        borderWidth: 1,
        borderColor: '#eee',
        borderRadius: 10,
        alignSelf: 'center',
        marginTop: 10,
        padding: 2,
        paddingHorizontal: 10,
    },
    hdrView: {
        borderWidth: 1,
        borderTopWidth: 0,
        borderBottomRightRadius: 50,
        borderBottomLeftRadius: 50,
        borderColor: '#eee',
        paddingHorizontal: 32,
        // backgroundColor: 'red',
        alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,

        elevation: 1,
        shadowRadius: 10,
    },
    hdrContainer: {
        backgroundColor: '#CFD8DC',
        marginTop: 16,
        padding: 10,
        width: '100%',
    },
    productCardContainer: {
        width: width / 2 + 75,
        // height: height / 2 - 50,
        padding: 15,
        backgroundColor: '#fff',
        alignSelf: 'center',
        marginTop: 10,
        borderRadius: 5,
        marginHorizontal: 5,

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,

        elevation: 5,
        shadowRadius: 10,
    },
    amountTitle: {
        color: '$MAIN_THEME',
        textAlign: 'right',
        fontSize: 14,
        position: 'absolute',
        left: 10,
        fontFamily: '$BOLD_FONT',
        bottom: 5,
    },
    primaryTitle: {
        fontSize: 15,
        color: 'grey',
        textAlign: 'center',
        fontFamily: '$REGULAR_FONT'
    },
    hdrTitle: {
        fontSize: 18,
        color: '#0F61AB',
        textAlign: 'center',
        fontFamily: '$BOLD_FONT',
    },
});