import firebase from 'react-native-firebase';
import { Logger } from './SmallUtils';
import Axios from 'axios';
import { request } from './Request';
import { Alert } from 'react-native';
import { AuthStore } from '../Stores/AuthStore';

export const getCurrentFirebaseToken = async () => {

    firebase.messaging().requestPermission().then(() => {

        firebase.messaging().getToken().then(async fcmToken => {
            console.log('fcmToken', fcmToken);
            if (fcmToken) {
                request.post('/v1/User/Fcm/Post', {
                    fcmToken: fcmToken
                }, {}, false)
                    .then(data => Logger(data, 'PutFcm'))
                    .catch(err => Logger('UserFcmToken err', err));
            }
        });
    }).catch(error => console.log('eror', error));

    firebase.notifications().onNotification((notification) => {


        console.log('notification', notification);
        console.log(notification._title, notification._body);
        // Alert.alert(JSON.stringify(notification._title), JSON.stringify(notification._body));
        AuthStore.userModel.setNotificationInfo(true, notification._title, notification._body);

        setTimeout(() => {
            AuthStore.userModel.setNotificationInfo(false, '', '');
        }, 4000);

    });

    firebase.notifications().onNotificationOpened((notificationOpen) => {

        console.log('notificationOpen', notificationOpen);
        console.log('notificationOpen.notification._data.Data', notificationOpen.notification._data.Data)

        // Alert.alert('notification2', JSON.stringify(notificationOpen.notification._data.Data));
    });
};

export const monitorFirebaseToken = () => firebase.messaging().onTokenRefresh(async fcmToken => {
    console.log('fcmToken onTokenRefresh', fcmToken);

    request.post('/v1/User/Fcm/Post', {
        fcmToken: fcmToken
    }, {}, false)
        .then(data => Logger(data, 'PutFcm'))
        .catch(err => Logger('UserFcmToken err', err));
});
