import React, { useState, useContext, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    Dimensions, TouchableHighlight,
    TouchableOpacity,
    Modal, TouchableWithoutFeedback, FlatList, Linking
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Button, Header, List, Loading, ModalComponent, SafeView } from '../../../Commons';
import StoreContext from '../../../../Stores';
import { useObserver } from 'mobx-react-lite';
import { ChallengeCard } from '../../../Elements/ChallengeCard';
import moment from 'moment-jalaali';


const { width, height } = Dimensions.get('window');


export default ChallengeList = ({ navigation }) => {
    const { ChallengeStore, ChallengeProfileStore } = useContext(StoreContext);

    const [loading, setLoading] = useState(false);
    const [loadingList, setLoadingList] = useState(false);

    useEffect(() => {
        getChallengeList();
    }, []);

    const getChallengeList = () => {
        setLoadingList(true);
        ChallengeStore.fetchData(true).then(() => {
            // navigation.navigate('challengeList');
            setLoadingList(false);
        });
    };

    const challengeOnclick = (item) => {
        setLoading(true);
        ChallengeProfileStore.setChallengeId(item.id);
        ChallengeProfileStore.getChallengeDetails().then(res => {
            if (res.videoFile !== null && res.image_File !== null) {
                ChallengeProfileStore.setUserHasLogged(true);
                navigation.navigate('challengeDetails');
                setLoading(false);
            } else {
                ChallengeProfileStore.setUserHasLogged(false);
                navigation.navigate('challengeDetails');
                setLoading(false);
            };
        });
    };

    return useObserver(() => (
        <SafeView>
            <Header
                title={'لیست چالش‌ها'}
                backOnClick={() => navigation.pop()}
                noBack
                hasUserIcon
                userOnclick={() => navigation.navigate('profile')}
            />


            <List
                style={{ marginTop: 10 }}
                onRefresh={() => getChallengeList()}
                store={ChallengeStore}
                showLoading={loadingList}
                renderItem={(item) => {
                    const date = moment(item.fromDate).format('jYYYY/jMM/jDD');
                    return (
                        <ChallengeCard
                            key={item.id}
                            active={item.isActive}
                            date={date}
                            remainDays={item.remainDays}
                            title={item.title}
                            onPress={() => challengeOnclick(item)}
                            file={item.picture}
                            reward={item.reward}
                        />
                    )
                }}
            />

            {loading ? <Loading /> : null}
        </SafeView>
    ));
};



const styles = EStyleSheet.create({
});