import { types, applySnapshot } from 'mobx-state-tree';
import { UserModel } from './models';
import { request, Storage, Base64, Logger, getString } from '../Utils';
import { SingleService, SingleBundle, SingleSlider } from './models/SingleModels';


const counterStore = types.model('counterStore', {
    data: types.optional(types.array(SingleService), []),

    loading: types.optional(types.boolean, false),
    errMessage: types.maybeNull(types.string),
    statusService: types.optional(types.string, ''),

    sliders: types.optional(types.array(SingleSlider), []),


    wSitName: types.maybeNull(types.string),
    wProductServiceInfraName: types.maybeNull(types.string),
    wPhoneNumber: types.maybeNull(types.string),
    wPostalCode: types.maybeNull(types.string),
    wService_Time_Remained_Title: types.maybeNull(types.string),
    wTraffic_Remained_Percent: types.maybeNull(types.number),
    wTraffic_RemainedTitle: types.maybeNull(types.string),
    wTotalDay: types.maybeNull(types.string),
    wTotalTraffic: types.maybeNull(types.string),
    wRemainedDayProgressBar: types.maybeNull(types.number),

}).actions((self) => {
    return {
        async fetchData(clear = false) {
            return new Promise(async (resolve, reject) => {
                let serviceId = await Storage.GetItem('@serviceId');

                if (clear) this.resetList();

                request.get('/v1/User/Service/Get')
                    .then(({ data }) => {
                        Logger(data, 'service info');
                        if (data.isSuccess) {
                            let traficData = data.data.map(item => item.trafficInfo);
                            this.fillData(traficData);
                            resolve(data.data);


                            let widgetData = data.data.filter(item => item.customerServiceId === serviceId)[0];

                            this.fillWidgetData(widgetData);
                        } else {
                            this.setErrorMessage(data.message);
                            resolve(data.isSuccess);
                        };
                    }, {}, false).catch(err => Logger(err));
            }).catch(err => Logger(err));
        },

        async fillWidgetData(data) {

            console.log('widget data', data)

            self.wSitName = data.sitName;
            self.wProductServiceInfraName = data.productServiceInfraName;
            self.wPhoneNumber = data.phoneNumber;
            self.wPostalCode = data.postalCode;

            self.wService_Time_Remained_Title = data.trafficInfo.service_Time_Remained_Title;
            self.wTraffic_Remained_Percent = data.trafficInfo.traffic_Remained_Percent;
            self.wTraffic_RemainedTitle = data.trafficInfo.traffic_RemainedTitle;

            
            self.wTotalDay = data.trafficInfo.service_Time_Title;
            self.wTotalTraffic = data.trafficInfo.trafficTitle;
            self.wRemainedDayProgressBar = data.trafficInfo.service_Time_Remained_Percent;


            console.log('w data', data)
        },
        async getSliders() {
            return new Promise(async (resolve, reject) => {

                request.get('/v1/User/Sliders/Get', {
                    params: {
                        Size: 5,
                        Page: 1,
                        Sort: 1
                    }
                })
                    .then(({ data }) => {
                        Logger(data, 'Sliders');
                        if (data.isSuccess) {
                            this.fillSliders(data.data);
                        };
                        resolve(data.isSuccess);
                    }, {}, false).catch(err => Logger(err));
            }).catch(err => Logger(err));
        },

        async cancelService(data) {
            return new Promise(async (resolve, reject) => {
                request.post('/v1/User/Service/CancelPreviousService', {
                    isCancel: data
                })
                    .then(({ data }) => {
                        resolve();
                        Logger(data, 'CancelPreviousService');
                    }, {}, false).catch(err => Logger(err));
            });
        },


        fillSliders(data) {
            applySnapshot(self.sliders, data);
        },
        fillData(data) {
            if (data.length === 0) {
                self.isLastPage = true;
                return false;
            }
            Array.prototype.push.apply(self.data, data.map(item => item));
        },
        setStatusService(status) {
            self.statusService = status;
        },
        setErrorMessage(message) {
            self.errMessage = message;
        },
        resetList() {
            self.page = 1;
            self.data = [];
            self.isLastPage = false;
        },
        onEndReached() {
            if (self.isLastPage || self.loading) {
                return false;
            };
            self.page += 1;
            this.fetchData();
        },
        changeLoading(data) {
            self.loading = data;
        },
    };
});

export const CounterStore = counterStore.create();


const counterProfileStore = types.model('counterProfileStore', {
    loading: types.optional(types.boolean, false),
    errMessage: types.maybeNull(types.string),
    statusService: types.optional(types.string, ''),

    traficInfo: SingleService,
}).actions((self) => {
    return {
        async getUserServiceInfo(userId) {
            return new Promise(async (resolve, reject) => {
                request.get('/v1/User/Service/Get', {
                    params: {
                        customerId: userId,
                    }
                }).then(({ data }) => {
                    Logger(data, 'service info');
                    if (data.isSuccess) {
                        this.setStatusService(data)
                        resolve(data.isSuccess);
                    }
                }).catch(err => Logger(err));
            }).catch(err => Logger(err));
        },

        setStatusService(status) {
            self.statusService = status;
        },
        setErrorMessage(message) {
            self.errMessage = message;
        },
        fillUserModel(data) {
            applySnapshot(self.userModel, data);
        },
        changeLoading(value) {
            return self.loading = value;
        },
    };
});

export const CounterProfileStore = counterProfileStore.create({
    traficInfo: SingleService.create(),
});


const bundleStore = types.model('bundleStore', {
    loading: types.optional(types.boolean, false),
    errMessage: types.maybeNull(types.string),
    data: types.optional(types.array(SingleBundle), []),

    limit: types.optional(types.number, 25),
    page: types.optional(types.number, 1),
    isLastPage: types.optional(types.boolean, false),

    customerServiceId: types.maybeNull(types.string),
    actionType: types.optional(types.string, 'Active'),
}).actions((self) => {
    return {
        async fetchData(clear = false) {
            let serviceId = await Storage.GetItem('@serviceId');

            return new Promise(async (resolve, reject) => {
                if (clear) this.resetList();

                request.get('/v1/User/Service/BundleList', {
                    params: {
                        CustomerServiceId: serviceId,
                        Action: self.actionType,
                        Size: self.limit,
                        Page: self.page,
                        Sort: 1,
                    }
                }).then(({ data }) => {
                    Logger(data, 'Service BundleList');
                    if (data.isSuccess) {
                        this.fillData(data.data);
                        resolve(data.isSuccess);
                    } else {
                        this.setErrorMessage(data.message);
                        resolve(data.isSuccess);
                    };
                }).catch(err => Logger(err));
            }).catch(err => Logger(err));
        },

        async activeBundle(id) {
            let serviceId = await Storage.GetItem('@serviceId');

            // console.log('customerServiceProductId',id)
            // console.log('customerServiceId', serviceId)
            return new Promise(async (resolve, reject) => {

                request.post('/v1/User/Service/Active', {
                    customerServiceProductId: id,
                    customerServiceId: serviceId,
                }).then(({ data }) => {
                    Logger(data, 'Active Bundle');
                    if (!data.isSuccess) {
                        resolve(data.message);
                        return;
                    };
                    resolve(data.isSuccess);
                }).catch(err => Logger(err));
            }).catch(err => Logger(err));
        },

        async changePassword(currentPassword, newPassword) {

            return new Promise(async (resolve, reject) => {

                request.put('/v1/User/Password/Put', {
                    currentPassword: currentPassword,
                    newPassword: newPassword,
                }, {}, false).then(({ data }) => {
                    Logger(data, 'change password');
                    resolve(data.isSuccess);
                }).catch(err => Logger(err));
            }).catch(err => Logger(err));
        },

        setActionType(type) {
            self.actionType = type;
        },

        setCustomerServiceId(id) {
            self.customerServiceId = id;
        },

        fillData(data) {
            if (data.length === 0) {
                self.isLastPage = true;
                return false;
            }
            Array.prototype.push.apply(self.data, data.map(item => item));
        },
        setErrorMessage(message) {
            self.errMessage = message;
        },
        resetList() {
            self.page = 1;
            self.data = [];
            self.isLastPage = false;
        },
        onEndReached() {
            if (self.isLastPage || self.loading) {
                return false;
            };
            self.page += 1;
            this.fetchData();
        },
        changeLoading(data) {
            self.loading = data;
        },
    };
});

export const BundleStore = bundleStore.create();
