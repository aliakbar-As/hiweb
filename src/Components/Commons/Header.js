import React from 'react';
import {
    SafeAreaView,
    StatusBar,
    TouchableOpacity,
    Text,
    Image,
    TouchableHighlight,
    View,
    TouchableWithoutFeedback,
    Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import EStyleSheet from 'react-native-extended-stylesheet';

const { width, height } = Dimensions.get('window');

export const Header = ({
    extraStyles,
    main,
    title,
    noBack,
    backOnClick,
    hasCart,
    badgeNumber,
    cartOnPress, userOnclick,
    hasPlus, plusOnclick,
    hasEdit, editOnclick,
    hasUserIcon, moreOnclick, showServicesOption,
    hasMessageIcon, messageOnclick
}) => {
    if (main) {
        return (
            <View style={[styles.container, extraStyles, { padding: 0, }]}>
                <TouchableHighlight
                    onPress={userOnclick}
                    underlayColor={'transparent'}>
                    <Icon
                        style={{ width: width / 3, alignSelf: 'center', justifyContent: 'flex-start', alignItems: 'center', marginLeft: 10, marginRight: -10 }}
                        size={25}
                        color={'#2d2d2d'}
                        name={'user'}
                    />
                </TouchableHighlight>


                <View style={{ alignSelf: 'center', justifyContent: 'center', alignItems: 'center', width: width / 3, }}>
                    <Text style={styles.hdrTitle}>
                        پارس‌آنلاین
                    {'\n'}
                        <Text style={{ color: '#36618F' }}>PARSONLINE</Text>
                    </Text>
                </View>


                {/* {showServicesOption ? */}
                <TouchableHighlight
                    onPress={moreOnclick}
                    underlayColor={'transparent'}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end', width: width / 3, alignSelf: 'center', marginRight: 10, marginLeft: -10 }}>
                        <Icon
                            size={15}
                            color={'#2d2d2d'}
                            name={'chevron-down'}
                        />
                        <Text style={styles.primaryTitle}>اشتراک‌ها</Text>
                    </View>
                </TouchableHighlight>
                {/* : null} */}

            </View>
        );
    } else {
        return (
            <View style={[styles.container, extraStyles]}>
                {noBack ? <View /> :
                    <TouchableOpacity
                        style={styles.onclicks}
                        onPress={backOnClick}>
                        <Icon
                            size={25}
                            color={'#2d2d2d'}
                            name={'arrow-left'}
                        />
                    </TouchableOpacity>
                }

                {hasUserIcon ?
                    <TouchableHighlight
                        onPress={userOnclick}
                        underlayColor={'transparent'}>
                        <Icon
                            size={25}
                            color={'#2d2d2d'}
                            name={'user'}
                        />
                    </TouchableHighlight> : <View />}


                {hasMessageIcon ?
                    <TouchableHighlight
                        onPress={messageOnclick}
                        underlayColor={'transparent'}>
                        <Icon
                            size={25}
                            color={'#2d2d2d'}
                            name={'message-circle'}
                        />
                    </TouchableHighlight> : <View />}

                <Text style={[styles.hdrTitle, { fontSize: 16 }]}>
                    {title}
                </Text>


                {hasCart ?
                    <TouchableOpacity
                        style={styles.onclicks}
                        onPress={cartOnPress}>
                        <Icon
                            size={25}
                            color={'#2d2d2d'}
                            name={'shopping-cart'}
                        />
                        {badgeNumber !== 0 ?
                            <Text style={styles.badgeTitle}>{badgeNumber}</Text> : null}
                    </TouchableOpacity>
                    : <View />}

                {hasPlus ?
                    <TouchableHighlight
                        underlayColor={'transparent'}
                        style={styles.onclicks}
                        onPress={plusOnclick}>
                        <Icon
                            size={25}
                            color={'#2d2d2d'}
                            name={'plus'}
                        />
                    </TouchableHighlight> : <View />}


                {hasEdit ?
                    <TouchableHighlight
                        underlayColor={'transparent'}
                        style={styles.onclicks}
                        onPress={editOnclick}>
                        <Icon
                            size={25}
                            color={'#2d2d2d'}
                            name={'edit'}
                        />
                    </TouchableHighlight> : <View />}
            </View >
        );
    }

};

const styles = EStyleSheet.create({
    emptyView: { height: 25, width: 25 },
    badgeTitle: {
        backgroundColor: 'red',
        height: 17,
        width: 17,
        color: '#fff',
        borderRadius: 17,
        textAlign: 'center',
        position: 'absolute',
        top: 7,
        right: '30%',
        fontFamily: '$REGULAR_FONT'
    },
    onclicks: {
        padding: 16,
        margin: -16
    },
    primaryTitle: {
        fontSize: 14,
        color: '#2d2d2d',
        textAlign: 'center',
        fontFamily: '$REGULAR_FONT',
    },
    hdrTitle: {
        fontSize: 15,
        color: '#2d2d2d',
        textAlign: 'center',
        fontFamily: '$BOLD_FONT',
        flex: 1,
    },
    container: {
        padding: 10,
        backgroundColor: '#fafafa',
        flexDirection: 'row',
        alignItems: 'center',
        height: 50,
        borderBottomWidth: 1,
        borderColor: '#ccc',
        width: '100%',
        justifyContent: 'space-between',


        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,

        elevation: 3,
        shadowRadius: 10,
    },
});
