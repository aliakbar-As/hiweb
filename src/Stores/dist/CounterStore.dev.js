"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CounterProfileStore = exports.CounterStore = void 0;

var _mobxStateTree = require("mobx-state-tree");

var _models = require("./models");

var _Utils = require("../Utils");

var _SingleModels = require("./models/SingleModels");

var counterStore = _mobxStateTree.types.model('counterStore', {
  data: _mobxStateTree.types.optional(_mobxStateTree.types.array(_SingleModels.SingleService), []),
  loading: _mobxStateTree.types.optional(_mobxStateTree.types["boolean"], false),
  errMessage: _mobxStateTree.types.maybeNull(_mobxStateTree.types.string),
  statusService: _mobxStateTree.types.optional(_mobxStateTree.types.string, '')
}).actions(function (self) {
  return {
    fetchData: function fetchData() {
      var _this = this;

      var clear,
          userId,
          _args2 = arguments;
      return regeneratorRuntime.async(function fetchData$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              clear = _args2.length > 0 && _args2[0] !== undefined ? _args2[0] : false;
              userId = _args2.length > 1 ? _args2[1] : undefined;
              return _context2.abrupt("return", new Promise(function _callee(resolve, reject) {
                return regeneratorRuntime.async(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        if (clear) _this.resetList();

                        _Utils.request.get('/User/Service/Get', {
                          params: {
                            customerId: userId,
                            fCPProvider: 'Hiweb'
                          }
                        }).then(function (_ref) {
                          var data = _ref.data;
                          (0, _Utils.Logger)(data, 'service info');

                          if (data.isSuccess) {
                            var traficData = data.data.map(function (item) {
                              return item.trafficInfo;
                            });

                            _this.fillData(traficData);

                            resolve(data.data);
                          } else {
                            _this.setErrorMessage(data.message);

                            resolve(data.isSuccess);
                          }

                          ;
                        })["catch"](function (err) {
                          return (0, _Utils.Logger)(err);
                        });

                      case 2:
                      case "end":
                        return _context.stop();
                    }
                  }
                });
              })["catch"](function (err) {
                return (0, _Utils.Logger)(err);
              }));

            case 3:
            case "end":
              return _context2.stop();
          }
        }
      });
    },
    fillData: function fillData(data) {
      if (data.length === 0) {
        self.isLastPage = true;
        return false;
      }

      Array.prototype.push.apply(self.data, data.map(function (item) {
        return item;
      }));
    },
    setStatusService: function setStatusService(status) {
      self.statusService = status;
    },
    setErrorMessage: function setErrorMessage(message) {
      self.errMessage = message;
    },
    resetList: function resetList() {
      self.page = 1;
      self.data = [];
      self.isLastPage = false;
    },
    onEndReached: function onEndReached() {
      if (self.isLastPage || self.loading) {
        return false;
      }

      ;
      self.page += 1;
      this.fetchData();
    },
    changeLoading: function changeLoading(data) {
      self.loading = data;
    }
  };
});

var CounterStore = counterStore.create();
exports.CounterStore = CounterStore;

var counterProfileStore = _mobxStateTree.types.model('counterProfileStore', {
  loading: _mobxStateTree.types.optional(_mobxStateTree.types["boolean"], false),
  errMessage: _mobxStateTree.types.maybeNull(_mobxStateTree.types.string),
  statusService: _mobxStateTree.types.optional(_mobxStateTree.types.string, ''),
  traficInfo: _SingleModels.SingleService
}).actions(function (self) {
  return {
    getUserServiceInfo: function getUserServiceInfo(userId) {
      var _this2 = this;

      return regeneratorRuntime.async(function getUserServiceInfo$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              return _context4.abrupt("return", new Promise(function _callee2(resolve, reject) {
                return regeneratorRuntime.async(function _callee2$(_context3) {
                  while (1) {
                    switch (_context3.prev = _context3.next) {
                      case 0:
                        _Utils.request.get('/User/Service/Get', {
                          params: {
                            customerId: userId,
                            fCPProvider: 'Hiweb'
                          }
                        }).then(function (_ref2) {
                          var data = _ref2.data;
                          (0, _Utils.Logger)(data, 'service info');

                          if (data.isSuccess) {
                            _this2.setStatusService(data);

                            resolve(data.isSuccess);
                          }
                        })["catch"](function (err) {
                          return (0, _Utils.Logger)(err);
                        });

                      case 1:
                      case "end":
                        return _context3.stop();
                    }
                  }
                });
              })["catch"](function (err) {
                return (0, _Utils.Logger)(err);
              }));

            case 1:
            case "end":
              return _context4.stop();
          }
        }
      });
    },
    setStatusService: function setStatusService(status) {
      self.statusService = status;
    },
    setErrorMessage: function setErrorMessage(message) {
      self.errMessage = message;
    },
    fillUserModel: function fillUserModel(data) {
      (0, _mobxStateTree.applySnapshot)(self.userModel, data);
    },
    changeLoading: function changeLoading(value) {
      return self.loading = value;
    }
  };
});

var CounterProfileStore = counterProfileStore.create({
  traficInfo: _SingleModels.SingleService.create()
});
exports.CounterProfileStore = CounterProfileStore;