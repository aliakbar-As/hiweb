import ImagePicker from 'react-native-image-crop-picker';
import { Logger } from './SmallUtils';

export const GalleryPicker = async () => {

    return new Promise((resolve, reject) => {

        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true
        }).then(image => {
            console.log(image);
            const source = {
                path: image.path,
                mime: image.mime,
                modificationDate: image.modificationDate,
            };
           
            resolve(source);
        });
    });
};
