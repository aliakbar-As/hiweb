import React, { useContext, useState, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    Dimensions,
    ScrollView,
    BackHandler,
    Platform, TouchableHighlight,
    ToastAndroid,
    Alert,
    ImageBackground
} from 'react-native';
import { SafeView, Header, Button, Input, ModalComponent } from '../../../Commons';
import EStyleSheet from 'react-native-extended-stylesheet';
import ImagePicker from 'react-native-image-crop-picker';
import Icon from 'react-native-vector-icons/AntDesign';
import { Logger, Storage, htmlStyles, CleanHtml } from '../../../../Utils';
import StoreContext from '../../../../Stores';
import { CommonActions } from '@react-navigation/native';
import moment from 'moment-jalaali';
import Axios from 'axios';
import ProgressCircle from 'react-native-progress-circle'
import HTMLView from 'react-native-htmlview';
import { set } from 'mobx';


const widthScreen = Dimensions.get('window').width;
const heightScreen = Dimensions.get('window').height;
const checked = require('../../../../assets/images/checked.png');


export default UploadVideo = ({ route, navigation }) => {
    const { ChallengeStore, ChallengeProfileStore } = useContext(StoreContext);

    const { uploadSize } = route.params;

    const [avatarSource, setAvatarSource] = useState('');
    const [videoUri, setVideoUri] = useState('');
    const [videoName, setVideoName] = useState('');
    const [statusTitle, setStatusTitle] = useState('');
    const [progressbar, setProgressbar] = useState(0);
    const [loading, setLoading] = useState(false);

    const [fileSize, setFileSize] = useState(0);
    const [fileSizeString, setFileSizeString] = useState(0);
    const [fileFormat, setFileFormat] = useState('');
    const [size, setSize] = useState('');

    const [errModal, setErrModal] = useState(0);
    const [errMessage, setErrMessage] = useState(0);


    const niceBytes = (bytes) => {
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes == 0) return '0 Byte';
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        let convertedSize = (bytes / Math.pow(1024, i)).toFixed(2) + ' ' + sizes[i];

        setFileSize((bytes / Math.pow(1024, i)).toFixed(2));
        setFileSizeString(convertedSize);
        setSize(sizes[i]);
        console.log('size', convertedSize);
        return convertedSize;
    };

    const handleSelectFiles = () => {

        ImagePicker.openPicker({
            mediaType: 'video',
        }).then(image => {
            console.log('image uploaded', image);
            let fileFormat = image.path.toString().endsWith('mp4');
            let format = image.path.split('.').pop();
            setFileFormat(format);
            console.log('format', format);
            if (!fileFormat) {
                setErrModal(2);
                setErrMessage(`فرمت ویدیو صحیح نمیباشد!`);
                return;
            };

            setProgressbar(0)
            setStatusTitle('');

            const source = { uri: image.path };
            setAvatarSource(source);
            const path = image.path.split('/');
            const name = path[path.length - 1];
            setVideoName(name);
            setVideoUri(image.path);

            niceBytes(image.size);
        }).catch(err => Logger(err))
    };

    const uploadVideo = async () => {
        if (fileSize > uploadSize && size === 'MB') {
            setErrModal(1);
            setErrMessage(`بارگذاری فایل با حجم بیشتر از ${uploadSize} مجاز نمی باشد!`);
            return;
        };

        setLoading(true);
        const videoData = {
            uri: videoUri,
            name: videoName,
            type: `video/${videoName.split('.')[1]}`,
        };

        const formData = new FormData();
        formData.append('files', videoData);

        const token = await Storage.GetItem('@token');
        Axios.post('https://mobileapp.hiweb.ir/api/v1/User/Upload/Post', formData, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            params: {
                section: 'Challenge',
            },
            onUploadProgress: (progressEvent) => {
                const { loaded, total } = progressEvent;
                let percent = Math.floor(loaded * 100 / total);
                // console.log(`${loaded}kb of ${total} | ${percent}%`);
                if (percent < 101) {
                    setProgressbar(percent)
                }
            },
        }).then(data => {
            Logger(data.data, 'UploadChallange');

            ChallengeProfileStore.postVideo(data.data, 'Text').then(data => {
                if (data) {
                    setProgressbar(0)
                    setLoading(false);
                    ChallengeProfileStore.getChallengeDetails().then(() => {
                        setErrModal(1000);
                        setErrMessage('اطلاعات شما با موفقیت ثبت شد.')
                        setLoading(false);
                    });

                    setStatusTitle('');

                };
            });
        }).catch(err => {
            setAvatarSource('');
            setLoading(false);
            setStatusTitle('مشکلی در سرور برنامه رخ داده است');
            Logger(err, 's err');
        });
    };

    const navigatePage = () => {
        navigation.navigate('challengeDetails');
        setErrModal(0)
    };

    return (
        <SafeView>
            <Header
                title={"ارسال فایل"}
                backOnClick={() => navigation.pop()}
            />



            <ScrollView>
                {avatarSource === '' ?
                    <TouchableHighlight
                        onPress={() => handleSelectFiles()}
                        underlayColor={'transparent'}>
                        <View style={styles.uploadVideoContainer}>
                            <Text style={styles.statusTitleStyle}>
                                بارگزاری فایل
                            </Text>

                            <Icon
                                name={'clouduploado'}
                                size={50}
                                color={'#12141D'}
                            />

                        </View>
                    </TouchableHighlight>
                    :
                    <View style={styles.uploadVideoContainer}>
                        {progressbar !== 0 ?
                            <ProgressCircle
                                percent={progressbar}
                                radius={20}
                                borderWidth={4}
                                color={'#1AA15F'}
                                shadowColor={'grey'}
                                bgColor={'#12141D'}
                                outerCircleStyle={styles.closeIcon}
                            >
                                <Text style={styles.progressTitle}>%{progressbar}</Text>
                            </ProgressCircle>
                            :
                            <Icon
                                name={'closecircle'}
                                size={30}
                                color={'grey'}
                                onPress={() => {
                                    setVideoName('');
                                    setAvatarSource('');
                                }}
                                style={styles.closeIcon}
                            />
                        }

                        <ImageBackground
                            source={avatarSource}
                            style={styles.avatarImage} >
                            <View style={styles.footerImageView}>
                                <Text style={styles.videoInfoTitle}> فرمت فایل شما : {fileFormat} </Text>
                                <Text style={styles.videoInfoTitle}> حجم فایل شما : {fileSizeString} </Text>
                            </View>
                        </ImageBackground>
                    </View>
                }

                <View style={styles.desContainer}>
                    <Text style={styles.desTitle}>
                        حجم ویدیو باید {uploadSize} مگابایت باشد
                    </Text>

                    <Text style={styles.desTitle}>
                        همچنین فرمت ویدیو باید mp4 باشد
                    </Text>
                </View>

            </ScrollView>

            <Button
                disabled={avatarSource === '' || loading}
                showLoading={loading}
                extraStyles={[styles.btnstyles, { opacity: avatarSource === '' || loading ? 0.4 : 1 }]}
                onPress={() => uploadVideo()}
                title={'ارسال فایل'}
            />

            <ModalComponent
                modalVisible={errModal !== 0}
                okModal
                title={errModal === 1000 ? 'ثبت شد!' : 'خطا!'}
                description={errMessage}
                okText={'باشه'}
                onPress={() => errModal === 1000 ? navigatePage() : setErrModal(0)}
                onRequestClose={() => errModal === 1000 ? navigatePage() : setErrModal(0)}
            />
        </SafeView>
    );

};
const styles = EStyleSheet.create({
    footerImageView: {
        position: 'absolute',
        bottom: 0,
        right: 0,
        left: 0,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 5,
        backgroundColor: 'rgba(0,0,0,0.7)',
    },
    videoInfoTitle: {
        fontSize: 14,
        color: '#fff',
        fontFamily: '$REGULAR_FONT',
        textAlign: 'center',
    },
    line: {
        width: '65%',
        alignSelf: 'center',
        height: 2,
        backgroundColor: '#12141D',
        marginBottom: 32
    },
    progressTitle: {
        fontSize: 13,
        color: '#fff',
        fontFamily: '$BOLD_FONT',
        textAlign: 'center',
    },
    nationalCodeContainer: {
        flexDirection: 'row-reverse',
        alignItems: 'center',
        marginTop: 32,
        justifyContent: 'center',
    },
    nationalCode: {
        fontFamily: '$BOLD_FONT',
        textAlign: 'left',
        color: '#12141D',
        fontSize: 23,
    },
    nationalCodeTitle: {
        fontFamily: '$REGULAR_FONT',
        textAlign: 'center',
        color: '#12141D',
        fontSize: 18,
        marginLeft: 10
    },
    statusTitleStyle: {
        color: '#12141D',
        textAlign: 'center',
        fontSize: 18,
        margin: 16,
        marginBottom: 24,
        fontFamily: '$BOLD_FONT'
    },
    closeIcon: {
        position: 'absolute',
        right: 10,
        top: 10,
        zIndex: 1,
    },
    uploadVideoContainer: {
        width: '90%',
        height: widthScreen / 2,
        backgroundColor: 'transparent',
        borderWidth: 1,
        borderColor: 'grey',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        alignSelf: 'center',
        marginBottom: 32,
        marginTop: 16,
    },
    avatarImage: {
        width: '100%',
        height: '100%',
        alignSelf: 'center',
        borderRadius: 5,
    },
    checkedIcon: {
        width: 50,
        height: 50,
        alignSelf: 'center',
        marginVertical: 10,
    },
    checkedcontainer: {
        width: widthScreen - 85,
        alignSelf: 'center',
        borderRadius: 30,
        borderColor: '#12141D',
        alignItems: 'center',
        borderWidth: 1,
        height: 250,
        marginTop: 32,
    },
    desContainer: {
        borderWidth: 1,
        borderColor: 'grey',
        alignItems: 'center',
        marginHorizontal: 16,
        padding: 32,
        borderRadius: 10,
        width: '85%',
        alignSelf: 'center',
        marginTop: 10,
    },
    btnstyles: {
        width: '90%',
        borderRadius: 100,
        height: 45,
    },
    desTitle: {
        fontFamily: '$REGULAR_FONT',
        textAlign: 'right',
        color: '#12141D',
        fontSize: 14,
    },
    btn: {
        backgroundColor: 'transparent',
        borderWidth: 1,
        width: '85%',
        height: 70,
        borderRadius: 50,
        borderColor: '#12141D'
    },
})
