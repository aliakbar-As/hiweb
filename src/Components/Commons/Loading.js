import React from 'react';
import {
    View,
    Modal,
    Text,
    Image,
    ActivityIndicator
} from 'react-native';

export const Loading = ({ onRequestClose }) => {
    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible
            onRequestClose={onRequestClose}
        >
            <View style={styles.modalView}>
                <View style={styles.modalContainerStyle}>
                    <ActivityIndicator size={'large'} color={'#FF005A'} />
                </View>
            </View>
        </Modal>
    );
};
const styles = {
    modalView: {
        backgroundColor: 'rgba(0,0,0,0.5)',
        flex: 1,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    modalContainerStyle: {
        backgroundColor: 'transparent',
        padding: 30,
        borderRadius: 10,
        alignItems: 'center',
    },
}