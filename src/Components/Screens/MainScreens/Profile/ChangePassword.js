import React, { useState, useContext, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    Dimensions, TouchableHighlight,
    TouchableOpacity,
    Modal, TouchableWithoutFeedback, FlatList,
    ToastAndroid,
    Alert
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Swiper from 'react-native-swiper';
import { Button, Header, Input, List, Loading, ModalComponent, SafeView } from '../../../Commons';
import { SlideCard } from '../../../Elements';
import ProgressCircle from 'react-native-progress-circle'
import StoreContext from '../../../../Stores';
import { useObserver } from 'mobx-react-lite';
import moment from 'moment-jalaali';
import { Numbers, Storage } from '../../../../Utils';
import { CommonActions } from '@react-navigation/native';

const { width, height } = Dimensions.get('window');


export default ChangePassword = ({ navigation, route }) => {
    const { AuthStore, ShoppingStore, BundleStore } = useContext(StoreContext);

    const [loading, setLoading] = useState(false);
    const [currentPassword, setCurrentPassword] = useState('');
    const [newPassword, setNewPassword] = useState('');
    const [confirmNewPassword, setConfirmNewPassword] = useState('');

    const [errModal, setErrModal] = useState(false);
    const [errMessage, setErrMessage] = useState('');

    const changePasswordOnclick = () => {

        if (newPassword === confirmNewPassword) {
            setLoading(true);
            BundleStore.changePassword(currentPassword, newPassword).then(res => {
                if (res) {
                    setLoading(false);
                    ToastAndroid.show("رمز شما با موفقیت تغییر کرد!", ToastAndroid.SHORT);
                    Storage.Clear();
                    navigation.dispatch(CommonActions.reset({
                        routes: [{ name: 'login' }],
                    }));
                } else {
                    setErrModal(true);
                    setErrMessage('کلمه عبور وارد شده نامعتبر است!');
                    setLoading(false);
                };
            });
        } else {
            setErrModal(true);
            setErrMessage('کلمه عبور وارد شده همخوانی ندارد!');
        };
    };

    return useObserver(() => (
        <SafeView>
            <Header
                title={"تغییر کلمه عبور"}
                backOnClick={() => navigation.pop()}
            />

            <ScrollView>
                <View style={styles.warningView}>
                    <Text style={styles.titles}>
                        مشترک گرامی
                        {'\n'}
                        به اطلاع می‌رساند تغییر کلمه عبور در این قسمت منجر به تغییر کلمه عبور شما در ورود به پنل کاربری می گردد و موجب تغییر کلمه عبور وایرلس شما نخواهد شد.
                    </Text>
                </View>

                <View style={{ margin: 10 }}>
                    <Input
                        placeholder={"کلمه عبور فعلی"}
                        value={currentPassword}
                        onChangeText={currentPassword => setCurrentPassword(currentPassword)}
                    />

                    <Text style={[styles.titles, { marginVertical: 10 }]}>
                        لطفا کلمه عبور جدید خود را در دو کادر زیر وارد نمایید.
                    </Text>

                    <Input
                        placeholder={"کلمه عبور جدید"}
                        value={newPassword}
                        onChangeText={newPassword => setNewPassword(newPassword)}
                    />

                    <Input
                        placeholder={"تکرار کلمه عبور جدید"}
                        value={confirmNewPassword}
                        onChangeText={confirmNewPassword => setConfirmNewPassword(confirmNewPassword)}
                    />
                </View>

                <Button
                    showLoading={loading}
                    disabled={loading}
                    onPress={() => changePasswordOnclick()}
                    title={'تغییر کلمه عبور'}
                    extraStyles={{ margin: 10, width: '95%' }}
                />

            </ScrollView>


            {errModal ?
                <ModalComponent
                    modalVisible={errModal}
                    okModal
                    title={'خطا!'}
                    description={errMessage}
                    okText={'باشه'}
                    onPress={() => setErrModal(0)}
                    onRequestClose={() => setErrModal(0)}
                /> : null}

        </SafeView>
    ));
};


const styles = EStyleSheet.create({
    warningView: {
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#5778AA',
        backgroundColor: '#D9EDF7',
        padding: 10,
        margin: 10
    },
    titles: {
        fontSize: 14,
        color: '#5778AA',
        textAlign: 'right',
        fontFamily: '$REGULAR_FONT',
    },
});