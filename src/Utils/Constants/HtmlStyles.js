import EStyleSheet from 'react-native-extended-stylesheet';
import { Platform } from 'react-native';

const styles = {
    fontFamily: '$REGULAR_FONT',
    color: '#12141D',
    fontSize: 15,
    textAlign: 'right',
    // textAlign: Platform.OS === 'android' ? 'right' : 'justify',
};

export const htmlStyles = EStyleSheet.create({
    ul: styles,
    li: styles,
    a: styles,
    p: styles,
    h1: styles,
    h2: styles,
    h3: styles,
    h4: styles,
    div: styles
});
