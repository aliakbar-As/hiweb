import React, { useState, useContext, useEffect } from 'react';
import {

    Dimensions, TouchableHighlight,

} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Button, Header, Input, List, Loading, SafeView } from '../../../Commons';
import StoreContext from '../../../../Stores';
import { useObserver } from 'mobx-react-lite';
import { WebView } from 'react-native-webview';

const { width, height } = Dimensions.get('window');


export default FaqScreen = ({ navigation, route }) => {
    const { AuthStore, TicketStore, CounterStore } = useContext(StoreContext);

    const [loadingList, setLoadingList] = useState(false);
    const [loading, setLoading] = useState(false);

    const { link } = route.params;

    return useObserver(() => (
        <SafeView>
            <Header
                title={"پرسش‌های متداول"}
                backOnClick={() => navigation.pop()}
            />

            <WebView
                source={{ uri: link }}
            />
        </SafeView>
    ));
};


const styles = EStyleSheet.create({
});