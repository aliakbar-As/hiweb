import React, { useState, useContext, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    Dimensions, TouchableHighlight,
    TouchableOpacity,
    Modal, TouchableWithoutFeedback, FlatList,
    ToastAndroid
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Swiper from 'react-native-swiper';
import { Button, Header, List, Loading, ModalComponent, SafeView } from '../../../Commons';
import { SlideCard } from '../../../Elements';
import ProgressCircle from 'react-native-progress-circle'
import StoreContext from '../../../../Stores';
import { useObserver } from 'mobx-react-lite';
import moment from 'moment-jalaali';
import { Numbers, Storage } from '../../../../Utils';

const { width, height } = Dimensions.get('window');


export default ProductDetails = ({ navigation, route }) => {
    const { CounterStore, ShoppingStore, ShoppingProfileStore } = useContext(StoreContext);

    const [loading, setLoading] = useState(false);
    const [disableButton, setDisableButton] = useState(false);
    const [badgeNumber, setBadgeNumber] = useState(0);

    const [errModal, setErrModal] = useState(0);
    const [errMessage, setErrMessage] = useState('');

    const [modalVisible, setModalVisible] = useState(false);


    const { trafficInternalWithUnit } = route.params;
    const { trafficWithUnit } = route.params;
    const { amount } = route.params;
    const { catId } = route.params;

    useEffect(() => {
        getShoppingFirstCart();
    }, []);

    const getShoppingFirstCart = () => {
        setLoading(true);
        setDisableButton(false);

        ShoppingStore.getShoppingCart(true).then(() => {
            if (ShoppingStore.cartData.length === 0) {
                setDisableButton(false);
                setLoading(false);
                return;
            };
            ShoppingStore.cartData[0].items.filter(item => {
                if (item.promotionProductId === ShoppingProfileStore.productDetails.promotionProductId) {
                    setDisableButton(true);
                };
            });
            setLoading(false);
        });
    };



    const getShoppingCart = () => {
        setLoading(true);
        setDisableButton(false);

        ShoppingStore.getShoppingCart(true).then(() => {
            if (ShoppingStore.cartData.length === 0) {
                setDisableButton(false);
                setLoading(false);
                navigation.pop();
                return;
            };
            ShoppingStore.cartData[0].items.filter(item => {
                console.log('here')
                if (item.promotionProductId === ShoppingProfileStore.productDetails.promotionProductId) {
                    setDisableButton(true);
                    navigation.pop();
                };
            });
            setLoading(false);
        });
    };
<<<<<<< HEAD

=======
>>>>>>> f4d81353b7a9c0d70ec16fd7a5857df2f5c9617b

    const addToCart = async () => {

        setModalVisible(false);
        setLoading(true);
        ShoppingStore.postCart(ShoppingProfileStore.productDetails.promotionProductId, 'add', 1).then(res => {
            if (!res) {
                setErrModal(-1);
                setErrMessage(ShoppingStore.errMessage !== null ? ShoppingStore.errMessage : 'خطایی رخ داده است!');
                setLoading(false);
                return;
            };
            ToastAndroid.show("محصول مورد نظر به سبد شما اضافه شد", ToastAndroid.SHORT);
            getShoppingCart();
        });

    };


    return useObserver(() => (
        <SafeView>
            <Header
                title={"اطلاعات تکمیلی"}
                backOnClick={() => navigation.pop()}
                cartOnPress={() => {
                    ShoppingStore.resetCartList();
                    navigation.navigate('cart');
                }}
                hasCart
                badgeNumber={ShoppingStore.badgeNumber}
            />


            <ScrollView>
                <View style={styles.tableView}>

                    {catId === 'd7f8d2e6-f25d-ea11-a94d-005056818579' ? null :
                        <View style={styles.innerTableView}>
                            <View style={{ width: '70%' }}>
                                <Text style={styles.hdrTitle}>{ShoppingProfileStore.productDetails.productName}</Text>
                            </View>

                            <View style={{ width: '30%' }}>
                                <Text style={styles.primaryTitle}>نام سرویس</Text>
                            </View>
                        </View>}

                    {catId === 'd7f8d2e6-f25d-ea11-a94d-005056818579' || catId === 'c7f8d2e6-f25d-ea11-a94d-005056818579' ? null :

                        <View style={styles.innerTableView}>
                            <View style={{ width: '70%' }}>
                                <Text style={styles.hdrTitle}>ترافیک کل (FUP)</Text>
                            </View>

                            <View style={{ width: '30%' }}>
                                <Text style={styles.primaryTitle}>ترافیک</Text>
                            </View>
                        </View>}

                    {catId === 'd7f8d2e6-f25d-ea11-a94d-005056818579' || catId === 'c5f8d2e6-f25d-ea11-a94d-005056818579' ? null :
                        <View style={styles.innerTableView}>
                            <View style={{ width: '70%' }}>
                                <Text style={styles.hdrTitle}> {trafficWithUnit} ترافیک بین الملل</Text>
                            </View>

                            <View style={{ width: '30%' }}>
                                <Text style={styles.primaryTitle}>ترافیک بین‌الملل</Text>
                            </View>
                        </View>}


                    {catId === 'd7f8d2e6-f25d-ea11-a94d-005056818579' || catId === 'c5f8d2e6-f25d-ea11-a94d-005056818579' ? null :

                        <View style={styles.innerTableView}>
                            <View style={{ width: '70%' }}>
                                <Text style={styles.hdrTitle}>معادل {trafficInternalWithUnit} ترافیک داخلی</Text>
                            </View>

                            <View style={{ width: '30%' }}>
                                <Text style={styles.primaryTitle}>ترافیک داخلی</Text>
                            </View>
                        </View>}


                    <View style={styles.innerTableView}>
                        <View style={{ width: '70%' }}>
                            <Text style={styles.hdrTitle}>{ShoppingProfileStore.productDetails.canTransfer}</Text>
                        </View>

                        <View style={{ width: '30%' }}>
                            <Text style={styles.primaryTitle}>قابل انتقال</Text>
                        </View>
                    </View>

                    <View style={styles.innerTableView}>
                        <View style={{ width: '70%' }}>
                            <Text style={styles.hdrTitle}>{ShoppingProfileStore.productDetails.bundleContent}</Text>
                        </View>

                        <View style={{ width: '30%' }}>
                            <Text style={styles.primaryTitle}>محتویات بسته</Text>
                        </View>
                    </View>


                    <View style={styles.innerTableView}>
                        <View style={{ width: '70%' }}>
                            <Text style={[styles.hdrTitle, { color: '#0F61AB' }]}>{Numbers.putCommas(amount)} ریال</Text>
                        </View>

                        <View style={{ width: '30%' }}>
                            <Text style={styles.primaryTitle}>قیمت</Text>
                        </View>
                    </View>


                    {ShoppingProfileStore.productDetails.description !== null ?
                        <View style={styles.innerTableView}>
                            <View style={{ width: '70%' }}>
                                <Text style={styles.hdrTitle}>{ShoppingProfileStore.productDetails.description}</Text>
                            </View>

                            <View style={{ width: '30%' }}>
                                <Text style={styles.primaryTitle}>توضیحات تکمیلی</Text>
                            </View>
                        </View> : null}

                </View>


            </ScrollView>

            <Button
                title={'افزودن به سبد خرید'}
<<<<<<< HEAD
                onPress={() => addToCart()}
                showLoading={loading}
                extraStyles={{ width: '90%', opacity: disableButton || loading ? 0.4 : null }}
                disabled={disableButton || loading}
            />
            {/* {loading ? <Loading /> : null} */}

            {errModal !== 0 ?
                <ModalComponent
                    modalVisible={errModal !== 0}
                    okModal
                    title={errModal === 1000 ? 'اطلاعیه' : 'خطا!'}
                    description={errMessage}
                    extraTitle={errModal === 1000 ? 'مخالفم' : undefined}
                    okText={errModal === 1000 ? 'موافقم' : 'باشه'}
                    onPress={() => errModal === 1000 ? activeBundle() : setErrModal(0)}
                    extraButtonOnPress={() => setErrModal(0)}
                    onRequestClose={() => setErrModal(0)}
                /> : null}
=======
                onPress={() => setModalVisible(true)}
                showLoading={loading}
                extraStyles={{ width: '90%', opacity: disableButton || loading ? 0.4 : null }}
                disabled={disableButton || loading}
            />
            {/* {loading ? <Loading /> : null} */}

            <ModalComponent
                modalVisible={errModal !== 0}
                okModal
                title={errModal === 1000 ? 'اطلاعیه' : 'خطا!'}
                description={errMessage}
                extraTitle={errModal === 1000 ? 'مخالفم' : undefined}
                okText={errModal === 1000 ? 'موافقم' : 'باشه'}
                onPress={() => errModal === 1000 ? activeBundle() : setErrModal(0)}
                extraButtonOnPress={() => setErrModal(0)}
                onRequestClose={() => setErrModal(0)}
            />
>>>>>>> f4d81353b7a9c0d70ec16fd7a5857df2f5c9617b

            <ModalComponent
                modalVisible={modalVisible}
                okModal
                title={'اطلاعیه!'}
                description={`اطلاعات سرویسی که شما برای آن بسته انتخاب کردید \n ${CounterStore.wProductServiceInfraName} ${CounterStore.wPhoneNumber !== null ? ' | ' + CounterStore.wPhoneNumber : ''} ${CounterStore.wPostalCode !== null ? ' | ' + CounterStore.wPostalCode : ''} \n آیا از انتخاب خود مطمئن هستید؟`}
                extraTitle={'بی‌خیال'}
                okText={'مطمئنم'}
                onPress={() => addToCart()}
                extraButtonOnPress={() => setModalVisible(false)}
                onRequestClose={() => setModalVisible(false)}
            />

        </SafeView>
    ));
};


const styles = EStyleSheet.create({
    innerTableView: {
        borderBottomWidth: 1,
        borderColor: '#eee',
        padding: 10,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center'
    },
    tableView: {
        justifyContent: 'flex-end',
        borderWidth: 1,
        borderColor: '#ccc',
        margin: 10,
        borderRadius: 10,
        backgroundColor: '#fff',
    },
    hdrContainer: {
        backgroundColor: '#CFD8DC',
        marginTop: 16,
        padding: 10,
    },
    productCardContainer: {
        width: width - 16,
        height: height / 6,
        backgroundColor: '#fff',
        alignSelf: 'center',
        marginTop: 10,
        borderRadius: 10,
        padding: 10,


        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,

        elevation: 5
        ,
        shadowRadius: 10,
    },
    amountTitle: {
        color: '$MAIN_THEME',
        textAlign: 'right',
        fontSize: 14,
        position: 'absolute',
        left: 10,
        fontFamily: '$BOLD_FONT',
        bottom: 5,
    },
    primaryTitle: {
        fontSize: 14,
        color: '#505050',
        textAlign: 'right',
        fontFamily: '$REGULAR_FONT'
    },
    hdrTitle: {
        fontSize: 15,
        color: '#2D2D2D',
        textAlign: 'right',
        fontFamily: '$BOLD_FONT',
    },
});