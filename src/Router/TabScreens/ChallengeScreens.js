import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';


import ChallengeList from '../../Components/Screens/MainScreens/Challenge/ChallengeList';
import ChallengeDetails from '../../Components/Screens/MainScreens/Challenge/ChallengeDetails';
import VideoState from '../../Components/Screens/MainScreens/Challenge/VideoState';
import PicturesState from '../../Components/Screens/MainScreens/Challenge/PicturesState';
import UploadVideo from '../../Components/Screens/MainScreens/Challenge/UploadVideo';
import UploadImages from '../../Components/Screens/MainScreens/Challenge/UploadImages';
import Profile from '../../Components/Screens/MainScreens/Profile/Profile';
import ChangePassword from '../../Components/Screens/MainScreens/Profile/ChangePassword';
import EditProfile from '../../Components/Screens/MainScreens/Profile/EditProfile';


const HomeStack = createStackNavigator();
const Stack = createStackNavigator();

export default ChallengeScreens = ({ navigation }) => {
    return (
        <HomeStack.Navigator
            initialRouteName="challengeList"
            headerMode="none"
            mode={'card'}
        >

            <Stack.Screen
                name="challengeList"
                component={ChallengeList}
            />


            <Stack.Screen
                name="challengeDetails"
                component={ChallengeDetails}
            />

            <Stack.Screen
                name="videoState"
                component={VideoState}
            />

            <Stack.Screen
                name="picturesState"
                component={PicturesState}
            />

            <Stack.Screen
                name="uploadVideo"
                component={UploadVideo}
            />

            <Stack.Screen
                name="uploadImages"
                component={UploadImages}
            />

            <Stack.Screen
                name="profile"
                component={Profile}
            />

            <Stack.Screen
                name="changePassword"
                component={ChangePassword}
            />

            <Stack.Screen
                name="editProfile"
                component={EditProfile}
            />
        </HomeStack.Navigator>
    );
};