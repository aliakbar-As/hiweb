import React, { useCallback, useState } from 'react';
import { View, Text, TouchableWithoutFeedback } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Icon from 'react-native-vector-icons/Entypo';
import { useLayoutAnimation } from '../../Utils';

export const CollapseBox = ({ children, title, style, headerTextStyle }) => {
    const [open, setOpen] = useState(false);

    const openWithAnimation = useCallback(() => {
        useLayoutAnimation();
        setOpen(!open);
    }, [open]);

    return (
        <TouchableWithoutFeedback onPress={() => openWithAnimation()}>
            <View
                style={[styles.container, {
                    height: open ? 'auto' : 60
                }, style]}
            >
                <View style={styles.header}>
                    <Icon
                        color={'#2d2d2d'}
                        size={25}
                        name={open ? 'chevron-small-up' : 'chevron-small-down'} />
                    <Text style={[styles.title, headerTextStyle]}>{title}</Text>
                </View>
                <View style={styles.contentWrapper}>
                    {children}
                </View>
            </View>
        </TouchableWithoutFeedback>
    );
};

const styles = EStyleSheet.create({
    container: {
        width: '100%',
        borderColor: '#eee',
        borderWidth: 1,
        backgroundColor: '#fff',
        borderRadius: 10,
        overflow: 'hidden',
        marginBottom: 15,
        alignSelf: 'center'
    },
    header: {
        width: '100%',
        height: 'auto',
        minHeight: 40,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 10,
        paddingVertical: 10,
        borderBottomColor: '#252832',
        // borderBottomWidth: 1,
    },
    title: {
        fontFamily: '$REGULAR_FONT',
        color: '#2d2d2d',
        fontSize: 14,
        paddingHorizontal: 8
    },
    contentWrapper: {
        width: '100%',
        height: 'auto',
        minHeight: 50,
        paddingHorizontal: 20,
        paddingVertical: 10,
    }
});
