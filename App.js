import React, { useEffect, useState } from 'react';
import { NativeModules, } from 'react-native';
import RouterComponent from './src/Router/Routes';
import EStyleSheet from 'react-native-extended-stylesheet';


EStyleSheet.build({
  // Colors variables
  $MAIN_THEME: '#0F61AB',
  $LINK_COLOR: '#3399FF',

  // Fonts Variables
  $LITE_FONT: 'IRANYekanMobileFaNum-Light',
  $REGULAR_FONT: 'IRANYekanMobileFaNum',
  $BOLD_FONT: 'IRANYekanMobileFaNum-Bold',
  $ENG_FONT: 'CoreMellow-Regular',
  $NUM_FONT: 'num',
});

export default App = () => {

  useEffect(() => {
    

    console.disableYellowBox = true;
  });

  return (
    <RouterComponent />
  );
};