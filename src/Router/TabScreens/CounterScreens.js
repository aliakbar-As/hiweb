import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import Counter from '../../Components/Screens/MainScreens/Counter/Counter';
import ServiceDetails from '../../Components/Screens/MainScreens/Counter/ServiceDetails';
import ExtraTraffic from '../../Components/Screens/MainScreens/Counter/ExtraTraffic';
import BundleScreen from '../../Components/Screens/MainScreens/Counter/BundleScreen';


import ChangePassword from '../../Components/Screens/MainScreens/Profile/ChangePassword';
import Profile from '../../Components/Screens/MainScreens/Profile/Profile';
import Cart from '../../Components/Screens/MainScreens/Shopping/Cart';
import EditProfile from '../../Components/Screens/MainScreens/Profile/EditProfile';


const HomeStack = createStackNavigator();
const Stack = createStackNavigator();

export default CounterScreens = ({ navigation }) => {
    return (
        <HomeStack.Navigator
            initialRouteName="counter"
            headerMode="none"
            mode={'card'}
        >

            <Stack.Screen
                name="counter"
                component={Counter}
            />

            <Stack.Screen
                name="serviceDetails"
                component={ServiceDetails}
            />

            <Stack.Screen
                name="extraTraffic"
                component={ExtraTraffic}
            />



            <Stack.Screen
                name="profile"
                component={Profile}
            />

            <Stack.Screen
                name="changePassword"
                component={ChangePassword}
            />

            <Stack.Screen
                name="editProfile"
                component={EditProfile}
            />

            <Stack.Screen
                name="cart"
                component={Cart}
            />
        </HomeStack.Navigator>
    );
};