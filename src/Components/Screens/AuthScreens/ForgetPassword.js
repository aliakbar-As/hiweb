import React, { useState, useContext } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView, Modal,
    TouchableHighlight,
    Dimensions,
    Keyboard,
    Alert,
    ImageBackground,
    Linking,
    StatusBar, TouchableOpacity
} from 'react-native';
import { Input, Button, SafeView, Header } from '../../Commons';
import Icon from 'react-native-vector-icons/Ionicons';
import StoreContext from '../../../Stores';
import EStyleSheet from 'react-native-extended-stylesheet';
import { CommonActions } from '@react-navigation/native';
import { WebView } from 'react-native-webview';

const { width, height } = Dimensions.get('window');

let tabItems = [
    {
        id: 0,
        title: 'کلمه عبور',
    },
    {
        id: 1,
        title: 'نام کاربری و کلمه عبور',
    },
];
export default ForgetPassword = ({ navigation }) => {

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [tabSelectedId, setTabSelectedId] = useState(0);

    return (
        <SafeView>
            <Header
                title={"فراموشی رمزعبور"}
                backOnClick={() => navigation.pop()}
            />


            <WebView
                source={{ uri: 'https://panel.hiweb.ir/passwordrecovery' }}
            />
            {/* <ScrollView>

                <View style={{ padding: 16 }}>
                    <View style={styles.hdrIconView}>
                        <Icon
                            name={'lock-closed-outline'}
                            size={60}
                            color={'#070707'}
                        />
                    </View>

                    <Text style={styles.hdrTitle}>
                        مشکلی با ورود به اپلیکیشن دارید؟
                    </Text>

                    <Text style={styles.desTitle}>
                        مشترک گرامی
                        در این قسمت شما میتوانید مشخصات ورود به پنل کاربری خود را بازیابی کنید.
                    </Text>


                    <View style={styles.mainTabContainer}>
                        {tabItems.map((item, index) => {
                            return (
                                <TouchableOpacity onPress={() => setTabSelectedId(item.id)} key={index}>
                                    <View key={index} style={[styles.tabViews, {
                                        borderColor: item.id === tabSelectedId ? '#2D2D2D' : '#DEDEDE',
                                    }]}>
                                        <Text style={[styles.tabTitles, { color: item.id === tabSelectedId ? '#000' : '#8D8D8D' }]}>{item.title}</Text>
                                    </View>
                                </TouchableOpacity>
                            )
                        })}
                    </View>

                    {tabSelectedId === 0 ?
                        <View>
                            <Input
                                placeholder={'مثلا: 2177889944'}
                                title={'نام کاربری'}
                                extraStyles={{ marginTop: 10 }}
                            />
                        </View>
                        :
                        <View>
                            <Input
                                placeholder={'کد ملی 10 رقمی'}
                                title={'کد ملی'}
                                extraStyles={{ marginTop: 10 }}
                                maxLength={10}
                            />


                            <View style={styles.line} />


                            <Input
                                placeholder={'مثلا: 021294000000'}
                                title={'شماره ADSL'}
                                extraStyles={{ marginTop: 10 }}
                                maxLength={11}
                            />

                            <Input
                                placeholder={'مثلا: 09123456789'}
                                title={'موبایل'}
                                extraStyles={{ marginTop: 10 }}
                                maxLength={11}
                            />
                        </View>
                    }


                    <Button
                        onPress={() => console.log(tabSelectedId)}
                        title={tabSelectedId === 0 ? 'بازیابی کلمه عبور' : 'بازیابی نام کاربری و کلمه عبور'}
                    />
                </View>
            </ScrollView> */}


        </SafeView>
    );
};

const styles = EStyleSheet.create({
    line: {
        width: '100%',
        alignSelf: 'center',
        height: 1,
        // marginVertical: 32,
        marginTop: 32,
        marginBottom: 16,
        backgroundColor: '#000',
        zIndex: -1
    },
    mainTabContainer: {
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row-reverse',
        width: width,
        marginTop: 20,
        alignSelf: 'center',
    },
    tabTitles: {
        fontSize: 16,
        marginBottom: 10,
        textAlign: 'center',
        fontFamily: '$REGULAR_FONT'
    },
    tabViews: {
        borderBottomWidth: 1,
        width: width / 2,
    },
    desTitle: {
        fontSize: 15,
        textAlign: 'center',
        color: '#1F1F1F',
        fontFamily: '$REGULAR_FONT',
    },
    hdrTitle: {
        fontSize: 18,
        textAlign: 'center',
        color: '#040404',
        fontFamily: '$BOLD_FONT'
    },
    hdrIconView: {
        padding: 16,
        borderRadius: 100,
        alignSelf: 'center',
        borderWidth: 1,
        borderColor: '#070707',
        margin: 16,
    },
});