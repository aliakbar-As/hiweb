import React, { useState, useContext, useEffect, useRef } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    Dimensions, TouchableHighlight,
    TouchableOpacity,
    Modal, TouchableWithoutFeedback, FlatList,
    ToastAndroid,
    Linking,
    AppState,
    Alert
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Button, Header, List, Loading, ModalComponent, SafeView } from '../../../Commons';
import StoreContext from '../../../../Stores';
import { useObserver } from 'mobx-react-lite';
import { Numbers, Storage } from '../../../../Utils';
import Icon from 'react-native-vector-icons/AntDesign';

const { width, height } = Dimensions.get('window');


export default Cart = ({ navigation, route }) => {
    const appState = useRef(AppState.currentState);
    const [appStateVisible, setAppStateVisible] = useState(appState.current);

    const { AuthStore, ShoppingStore, ShoppingProfileStore } = useContext(StoreContext);

    const [loading, setLoading] = useState(false);
    const [loadingList, setLoadingList] = useState(false);
    const [checkoutLoading, setCheckoutLoading] = useState(false);
    const [serviceId, setServiceId] = useState(false);
    const [badgeNumber, setBadgeNumber] = useState(0);

    const [errModal, setErrModal] = useState(0);
    const [errMessage, setErrMessage] = useState('');

    useEffect(() => {
        getShoppingCart();
        setServiceIdFunc();

        AppState.addEventListener("change", _handleAppStateChange);

        return () => {
            AppState.removeEventListener("change", _handleAppStateChange);
        };
    }, []);

    const getShoppingCart = () => {
        setLoadingList(true);
        ShoppingStore.getShoppingCart(true).then(() => {
            if (ShoppingStore.badgeNumber === 0) {
                ShoppingStore.resetCartList();
                ShoppingStore.setBadge(0);
            };

            setLoadingList(false);
        });
    }
    const setServiceIdFunc = async () => {
        let serviceId = await Storage.GetItem('@serviceId');
        setServiceId(serviceId);
    };

    const _handleAppStateChange = (nextAppState) => {
        if (
            appState.current.match(/inactive|background/) &&
            nextAppState === "active"
        ) {
            console.log("App has come to the foreground!");
            ShoppingStore.getShoppingCart(true);
        } else {
            console.log("App has come to the background!");

        };

        appState.current = nextAppState;
        setAppStateVisible(appState.current);
        // console.log("AppState", appState.current);

    };

    const showToast = () => {
        ToastAndroid.show("سبد شما با موفقیت حذف شد !", ToastAndroid.SHORT);
    };

    const getCheckout = (data) => {
        setLoading(true);
        ShoppingStore.getPaymentGateWay(data.id).then(res => {
            Linking.openURL(res);
            setLoading(false);
        });
    };

    const deleteCartOnclick = (cartId) => {
        setLoadingList(true);
        ShoppingStore.deleteCart(cartId).then(res => {
            ShoppingStore.getShoppingCart(true).then(() => {
                setLoadingList(false);
                if (res) showToast();
            })
        });
    };


    const deleteCartProduct = (cartId, item, items) => {
        setLoadingList(true);
        ShoppingStore.postCart(item.promotionProductId, 'remove', 0).then(res => {
            if (!res) {
                setErrModal(-1);
                setErrMessage(ShoppingStore.errMessage !== null ? ShoppingStore.errMessage : 'خطایی رخ داده است!');
                setLoadingList(false);
                return;
            };
            ShoppingStore.getShoppingCart(true).then(res => {
                if (res) {
                    ShoppingStore.getShoppingCart(true);
                    if (ShoppingStore.badgeNumber === 0) {
                        ShoppingStore.resetCartList();
                        ShoppingStore.setBadge(0);
                    };
                    ToastAndroid.show("محصول مورد نظر از سبد شما پاک شد !", ToastAndroid.SHORT);
                    setLoadingList(false);
                };

            });
        });
    };

    const minusOnclick = (id) => {
        // setLoadingList(true);
        setLoading(true);
        ShoppingStore.cartData[0].items.filter(item => {
            if (item.productId === id) {
                let count = item.quantity < 0 ? 0 : item.quantity - 1;
                ShoppingStore.postCart(item.promotionProductId, 'add', count).then(() => {
                    ShoppingStore.getShoppingCart(true).then(() => {
                        // setLoadingList(false);
                        setLoading(false);
                    });
                });
            };

        })
    };
    const plusOnclick = (id) => {
        setLoading(true);
        ShoppingStore.cartData[0].items.filter(item => {
            if (item.productId === id) {
                let count = item.quantity + 1;
                ShoppingStore.postCart(item.promotionProductId, 'add', count).then(() => {
                    ShoppingStore.getShoppingCart(true).then(() => {
                        setLoading(false);
                    });
                });
            };

        })
    };

    return useObserver(() => (
        <SafeView>
            <Header
                title={"سبد خرید"}
                backOnClick={() => navigation.pop()}
            />

            <List
                ListEmptyComponent={() => {
                    if (ShoppingStore.badgeNumber === 0 || !loadingList) {
                        return (
                            <Text style={styles.emptyDataTitle}>
                                سبد شما خالی است!
                            </Text>
                        )
                    } return null;
                }}
                onRefresh={() => getShoppingCart()}
                showLoading={loadingList}
                data={ShoppingStore.cartData}
                renderItem={(data) => {
                    if (ShoppingStore.cartData[0].items.length !== 0) {
                        return (
                            <View style={styles.cartsMainContainer}>

                                <View style={styles.hdrContainer}>
                                    <Text style={styles.headerTitles}>
                                        شما در حال خرید برای سرویس <Text style={{ color: 'red', fontFamily: EStyleSheet.value('$BOLD_FONT'), fontSize: 16 }}>{data.phoneNumber !== null ? data.phoneNumber : data.postalCode}</Text> هستید!
                                    </Text>
                                </View>

                                <List
                                    data={data.items}
                                    renderItem={(item, index) => {
                                        return (
                                            <View style={styles.cartView}>
                                                <View style={styles.hdrCartView}>
                                                    {item.promotionProductId !== '00000000-0000-0000-0000-000000000000' ?
                                                        <TouchableHighlight
                                                            style={styles.onclicks}
                                                            onPress={() => deleteCartProduct(data.id, item, data.items)}
                                                            underlayColor={'transparent'}>
                                                            <Icon
                                                                name={'close'}
                                                                color={'gray'}
                                                                size={20}
                                                            />
                                                        </TouchableHighlight> : <View />}

                                                    <View style={{ flexDirection: 'row' }}>
                                                        <View style={styles.views}>
                                                            <Icon
                                                                name={'minus'}
                                                                color={'#2d2d2d'}
                                                                disabled={item.quantity === 1}
                                                                size={15}
                                                                onPress={() => {
                                                                    if (item.quantity < 0) {
                                                                        return;
                                                                    } else {
                                                                        minusOnclick(item.productId);
                                                                    }
                                                                }}
                                                            />
                                                        </View>

                                                        <View style={styles.views}>
                                                            <Text style={styles.desTitle}>{item.quantity < 0 ? 0 : item.quantity}</Text>
                                                        </View>

                                                        <View style={styles.views}>
                                                            <Icon
                                                                name={'plus'}
                                                                color={'#2d2d2d'}
                                                                size={15}
                                                                onPress={() => {
                                                                    if (item.quantity < item.orderMax) {
                                                                        plusOnclick(item.productId);
                                                                    } else {
                                                                        setErrModal(-2);
                                                                        setErrMessage('تعداد خرید شما بیشتر از تعداد مجاز است')
                                                                    }
                                                                }}
                                                            />
                                                        </View>
                                                    </View>
                                                </View>
                                                <View style={styles.tableView}>
                                                    <Text style={styles.hdrTitle}>{item.productName}</Text>
                                                    <Text style={styles.desTitle}>محصول:    </Text>
                                                </View>

                                                <View style={styles.tableView}>
                                                    <Text style={styles.hdrTitle}>{item.quantity}</Text>
                                                    <Text style={styles.desTitle}>تعداد:    </Text>
                                                </View>

                                                <View style={styles.tableView}>
                                                    <Text style={styles.hdrTitle}>{Numbers.putCommas(item.extendedAmount)} ریال</Text>
                                                    <Text style={styles.desTitle}>قیمت:    </Text>
                                                </View>
                                            </View>
                                        )
                                    }}
                                />

                                {data.items.length !== 0 ?
                                    <Button
                                        extraStyles={{ width: '95%', }}
                                        onPress={() => getCheckout(data)}
                                        showLoading={checkoutLoading}
                                        title={'نهایی کردن سفارش'}
                                    /> : null}

                            </View>
                        )
                    } else return <Text style={styles.emptyDataTitle}>
                        سبد شما خالی است!
                    </Text>;
                }}
            />

            {loading ? <Loading /> : null}

            <ModalComponent
                modalVisible={errModal !== 0}
                okModal
                title={errModal === 1000 ? 'اطلاعیه' : 'خطا!'}
                description={errMessage}
                extraTitle={errModal === 1000 ? 'مخالفم' : undefined}
                okText={errModal === 1000 ? 'موافقم' : 'باشه'}
                onPress={() => errModal === 1000 ? activeBundle() : setErrModal(0)}
                extraButtonOnPress={() => setErrModal(0)}
                onRequestClose={() => setErrModal(0)}
            />


        </SafeView>
    ));
};


const styles = EStyleSheet.create({
    hdrCartView: {
        flexDirection: 'row', alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10,
    },
    views: {
        width: 30,
        height: 30,
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#ccc',
        justifyContent: 'center',
        alignSelf: 'center',
    },
    hdrContainer: {
        // paddingHorizontal: 16,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerTitles: {
        fontSize: 16,
        color: '#2d2d2d',
        textAlign: 'center',
        fontFamily: '$REGULAR_FONT',
    },
    emptyDataTitle: {
        color: '#2d2d2d',
        textAlign: 'center',
        fontSize: 16,
        fontFamily: '$BOLD_FONT',
        marginTop: '50%',
        marginHorizontal: 32,
    },
    deleteCartTitle: {
        fontSize: 14,
        color: '#36618F',
        textAlign: 'center',
        fontFamily: '$REGULAR_FONT',
    },
    cartsMainContainer: {
        width: '95%',
        backgroundColor: '#fff',
        alignSelf: 'center',
        marginTop: 16,
        paddingVertical: 10,
        borderRadius: 10,


        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,

        elevation: 5,
        shadowRadius: 10,
    },
    tableView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    desTitle: {
        fontSize: 16,
        color: '#2D2D2D',
        textAlign: 'right',
        fontFamily: '$REGULAR_FONT',
    },
    hdrTitle: {
        fontSize: 16,
        color: '#2D2D2D',
        textAlign: 'right',
        fontFamily: '$BOLD_FONT',
        width: '55%'
    },
    onclicks: {
        padding: 16,
        margin: -16
    },
    cartView: {
        width: '95%',
        padding: 10,
        borderWidth: 1,
        borderColor: '#fff',
        backgroundColor: '#fff',
        alignSelf: 'center',
        marginTop: 10,
        borderRadius: 10,

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,

        elevation: 5,
        shadowRadius: 10,
    },
});